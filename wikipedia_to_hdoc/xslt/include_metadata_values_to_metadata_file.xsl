<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:param name="Link" required="yes" as="xs:string"/>
    
   <xsl:template match="html">
       
       <link>
           <url>
               <xsl:value-of select="$Link"/>
           </url>
           <creator>
               <xsl:value-of select="//span[@id='creator']//a"/>
           </creator>
           <date>
               <xsl:value-of select="//td[@id='fileinfotpl_date']/../td[last()]"/>
           </date>
           <licence>
               <xsl:value-of select="//span[@class='licensetpl_long']"/>
           </licence>
       </link>
       
    </xsl:template>
</xsl:stylesheet>