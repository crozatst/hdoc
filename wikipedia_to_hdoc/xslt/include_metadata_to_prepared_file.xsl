<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:output method="xml"/>
    <xsl:param name="Link" required="yes" as="xs:string"/>
    <xsl:param name="Length_string_to_delete" as="xs:integer">
        <xsl:value-of select="string-length('https://fr.wikipedia.org')+1"/>
    </xsl:param>
    <xsl:param name="Path" required="yes" as="xs:string"/>
    <xsl:param name="Link_to_test">
        <xsl:value-of select="substring($Link,$Length_string_to_delete)"/>
    </xsl:param>
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="a[img][./@href=$Link_to_test]">
               <xsl:copy>                             
                   <xsl:copy-of select="@*"/> 
                   <xsl:copy-of select="./*"/>
                   <span style="display:none" class="img_metadata">
                       <xsl:text>Date : </xsl:text><xsl:value-of select="document($Path)//date"/>
                       <xsl:text> ; Auteur : </xsl:text><xsl:value-of select="document($Path)//creator"/>
                       <xsl:text>; licence : </xsl:text><xsl:value-of select="document($Path)//licence"/>
                   </span>
            </xsl:copy>
            
   
    </xsl:template>
</xsl:stylesheet>