<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">

    <xsl:output method="xml"/>

    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match ="/html/body/div/div/div/div[@class='thumb tleft' or @class='thumb tright']" priority="1">
        <div class="{@class}"> 
            <xsl:apply-templates select=".//img" />
            <xsl:apply-templates select=".//div[@class='thumbcaption']" />            
        </div>
    </xsl:template>

    <xsl:template match="img">
            <img src="http:{@src}" alt="{@alt}"/>
    </xsl:template>
    <xsl:template match="div[@class='thumbcaption']">
            <p class="thumbcaption"> 
                <xsl:apply-templates select="./text() | .//a/text()" mode="thumbcaption"/>
            </p>
    </xsl:template>

    <xsl:template match="text()" mode="thumbcaption">
          <xsl:value-of select="."/>
    </xsl:template>




    <!-- Scripts are not useful to us -->
    <xsl:template match="script"/>

    <!-- Ignoring divs that are not useful and that might interfere with the true xslt transformation -->
    <xsl:template match="/html/body/div/div/div/div[@class!='mw-highlight mw-content-ltr' and not(descendant::table)]"/>
</xsl:stylesheet>