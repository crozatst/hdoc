<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:output encoding="UTF-8"/>

    <xsl:template match="html">
        <images>
            <xsl:apply-templates select=".//a[img]"/>
        </images>
    </xsl:template>
    <xsl:template match="a[img]">
        <xsl:param name="test">
            <xsl:value-of select="@href"/>
        </xsl:param>
             <xsl:if test="not(contains($test,'External') or contains($test,'Portail')) and contains($test,'Fichier')">
           <image>
            <link>
                <xsl:text>https://fr.wikipedia.org</xsl:text><xsl:value-of select="@href"/>             
            </link>
            <ref>
                <xsl:value-of select="./img/@src"/>
            </ref>
        </image>
             </xsl:if>
    </xsl:template>
    
  
</xsl:stylesheet>