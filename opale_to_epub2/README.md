Opale to Epub2
------------

License
-------

http://www.gnu.org/licenses/gpl-3.0.txt

Credits
-------

### Autumn 2016

* Biagianti Marie

* Couturier Valentin

Presentation
------------

"Opale to Epub2" is an opale converter to epub version 2.0 files. It's a set of ANT scripts and XSL files

Dependencies
------------

There's no particular dependencies needed to run the converter.

User Documentation
------------------

### Scenario

La personne travaille sur son contenu opale. Il voudrait avoir un rendu sur sa liseuse lui permettant à lui et ses étudiants de relire plus facilement son cours. Il décide d'utiliser hdoc pour faire une conversion d'opale vers epub. Il se dirige vers le site hdoc et télécharge le zip. Il extrait le zip et se rend vers le dossier opale_to_epub2. Il lit le README.md et suit les directives pour obtenir son format epub. Il doit exporter son contenu opale et mettre son extension .scar dans le dossier input. Ensuite il doit utiliser l'exécuteur run.bat ou run.sh en fonction du système d'exploitation qu'il utilise. Enfin, il aura son format epub disponible dans le dossier output.

Unsupported
-----------

### EPUB2

ODG : non supporté en EPUB => "Élément non supporté en EPUB : <xsl:value-of select="@data"/>"

CSS3

HTML5

SVG

(voir https://mpsltd.wordpress.com/2011/05/25/epub-3-vs-epub-2-0-2/)

### Conversion

Ce qui n'est pas implémenté dans la conversion opale_to_hdoc c'est-à-dire :
* Module
	* Titre court
	* Références générales
	* Activité d'auto évaluation
	* Exercice rédactionnel
	* Exercice QCM
	* Exercice QCM graphique
	* Exercice QCU
	* Exercice QCU graphique
	* Exercice catégorisation
	* Exercice ordonnancement
	* Exercice texte à trous
	* Exercice question à réponse courte
	* Liste d'exercice

* Activité d'apprentissage
	* Titre court
	* Exercice rédactionnel
	* Exercice QCU
	* Exercice QCU graphique
	* Exercice QCM
	* Exercice QCM graphique
	* Exercice catégorisation
	* Exercice ordonnancement
	* Exercice texte à trous
	* Exercice question à réponse courte
	* Liste d'exercice

* Grain de contenu
	* Titre court

* Blocs
	* All bloc types are supported

* Types de texte
	* Ressources (ressources de type animation non supporté)
	* Fichier en téléchargement

* Éléments présents dans un texte
	* Formule mathématique
	* Abréviation
	* Glossaire
	* Références
	* Lien vers un document
	* Renvoi vers une ressource
	* Renvoi vers un grain de contenu
	* Animation

Known bugs
----------

bug conversion opale_to_hdoc => data-hdoc-type="references" : liste à puce inversé (<li><ul></ul><ul></ul></li>)

Todo list
---------

Faire les conversions nécessaires lorsque la conversion opale_to_hdoc évoluera dans le dossier hdoc_to_epub2.

Technical Notes
---------------

Il est déconseillé de justifier le texte pour des raisons de lisibilité sur les liseuses.
