const fs = require('fs');
var maki = require('maki-sushi');
const config = JSON.parse(fs.readFileSync('config.json'));
const testFolder = '../output/';


fs.readdir(testFolder, (err, files) => {
  files.forEach(file => {
  	let q = fs.readFileSync("../output/" + file).toString().replace(/[\n\t\r]/g,'').replace(/\s{4,}/g,'').split(';');
  	q.pop();
  	insert(q);
  });
})


maki.config.setId(config.db,config.pwd);
maki.config.setEndpoint(config.host, config.path);
maki.config.setPort(config.port);


function insert(queries){
	for(let i = 0; i < queries.length; ++i){
		let q = queries[i];
		console.log(q + ";");
		if(q.indexOf('MATCH') !== -1){
			setTimeout(function(){
				maki.query.single(q + ';', function(status, response){
					console.log("on insere un lien " + response);
				});
			}, 2000);
		}
		else{
			maki.query.single(q, function(status, response){
				console.log("on insere un noeud " + response);
			});
		}
	}
}

