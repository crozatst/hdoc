<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xpath-default-namespace="http://www.utc.fr/ics/hdoc/xhtml"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:output method="text" indent="no"/>
   
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="head"/>
    
    <xsl:template match="body">
        MERGE (i:module {title:"<xsl:value-of select="concat(../head/title,' (',string-length(.), ')')"/>"});
        <xsl:apply-templates select="./section"/>
    </xsl:template>
    
    <xsl:template match="body/section">
        MERGE (i:<xsl:value-of select="./@data-hdoc-type"/> {title:"<xsl:value-of select="concat(./header/h1,' (',string-length(.), ')')"/>"});
        MATCH (l1 {title:"<xsl:value-of select="concat(./header/h1,' (',string-length(.), ')')"/>"}), (l2 {title:"<xsl:value-of select="concat(ancestor::html/head/title,' (',string-length(ancestor::html/body), ')')"/>"}) MERGE (l2)-[:LIEN]->(l1);
        <xsl:apply-templates select="./section"/>
    </xsl:template>
    
    <xsl:template match="body/section/section">
        MERGE (i:<xsl:value-of select="./@data-hdoc-type"/> {title:"<xsl:value-of select="concat(./header/h1,' (',string-length(.), ')')"/>"});
        MATCH (l1 {title:"<xsl:value-of select="concat(./header/h1,' (',string-length(.), ')')"/>"}), (l2 {title:"<xsl:value-of select="concat(parent::section/header/h1,' (',string-length(parent::section), ')')"/>"}) MERGE (l2)-[:LIEN]->(l1);
        <xsl:apply-templates select="./section"/>
    </xsl:template>
    
    <!-- Traitement spécial pour les objectifs, ils ont parfois la même longueur -->
    <xsl:template match="body/section/section/section[@data-hdoc-type='aims' or @data-hdoc-type='synthesis']">
        MERGE (i:<xsl:value-of select="./@data-hdoc-type"/> {title:"<xsl:value-of select="concat(./header/h1,' - ', parent::section/header/h1, ' (',string-length(.), ')')"/>"});
        MATCH (l1 {title:"<xsl:value-of select="concat(./header/h1,' - ', parent::section/header/h1, ' (',string-length(.), ')')"/>"}), (l2 {title:"<xsl:value-of select="concat(parent::section/header/h1,' (',string-length(parent::section), ')')"/>"}) MERGE (l2)-[:LIEN]->(l1);
    </xsl:template>
    
    <xsl:template match="body/section/section/section[not(@data-hdoc-type='aims' or @data-hdoc-type='synthesis')]">
        MERGE (i:<xsl:value-of select="./@data-hdoc-type"/> {title:"<xsl:value-of select="concat(./header/h1,' (',string-length(.), ')')"/>"});
        MATCH (l1 {title:"<xsl:value-of select="concat(./header/h1,' (',string-length(.), ')')"/>"}), (l2 {title:"<xsl:value-of select="concat(parent::section/header/h1,' (',string-length(parent::section), ')')"/>"}) MERGE (l2)-[:LIEN]->(l1);
    </xsl:template>
     
</xsl:stylesheet>