(: This script will return documents by searching its author :)
(: We can assign a Regular Expression to the variable $author :)
(: For example, $name := '^Baptiste Montangé$', to search for an exact name:)
(: For example, $name := 'Montangé', to search for documents whose author named Coutant:)
(: Remark : case and accents in author's name have been taken care of :)

module namespace myNs = "https://gitlab.utc.fr/crozatst/hdoc/tree/master/hdoc_to_basex";

declare function myNs:searchDocByAuthor($name as xs:string, $docs as node()*) as node()*
{
  let $name_noAcc := translate($name, 'áàâäéèêëíìîïóòôöúùûüABCDEFGHIJKLMNOPQRSTUVWXYZ','aaaaeeeeiiiioooouuuuabcdefghijklmnopqrstuvwxyz')
  for $doc in $docs
  return
    for $author in $doc/authors/author
    let $title := $doc/title
    let $author_noAcc := translate($author, 'áàâäéèêëíìîïóòôöúùûüABCDEFGHIJKLMNOPQRSTUVWXYZ','aaaaeeeeiiiioooouuuuabcdefghijklmnopqrstuvwxyz')
    where matches($author_noAcc, $name_noAcc)
    group by $title (: Avoid duplications of documents by their titre:)
    return $doc
};