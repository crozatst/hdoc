<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:h="http://www.utc.fr/ics/hdoc/xhtml"
    exclude-result-prefixes="xs" version="2.0">

    <xsl:template match="h:html">
        <document>
        <xsl:apply-templates mode="title"/>
    </document>
    </xsl:template>

    <xsl:template match="h:head" mode="title">
        <title>
            <xsl:value-of select="./h:title"/>
        </title>
            <xsl:apply-templates select="./h:meta"/>
            
    </xsl:template>
    
    <xsl:template match="h:meta[@name='author']">
        <authors> 
           
                <xsl:for-each select="tokenize(@content,', \d')">
                    <xsl:if test="position() = 1">
                        <xsl:for-each select="tokenize(.,', ')">
                        <author>
                            <xsl:value-of select="."/>
                            
                        </author>
                        </xsl:for-each>
                    </xsl:if>
                </xsl:for-each>
            
        </authors>
    </xsl:template>

    <xsl:template match="h:body" mode="title">
        <sections>
           
                <xsl:apply-templates select="./h:section"/>
            
        </sections>
    </xsl:template>
    
    <xsl:template match="h:section">
        <section>
            <typesection>
                <xsl:value-of select="@data-hdoc-type"/>
            </typesection>
            <titlesection>
                <xsl:value-of select="./h:header/h:h1"/>
            </titlesection>
            <content>
                <paragraphs>
                    <xsl:apply-templates select="h:div"/>
                </paragraphs>
                <xsl:apply-templates select="h:section" mode="soussect"/>
            </content>
        </section>
    </xsl:template>
 

    <xsl:template match="h:section" mode="soussect">
        <soussection></soussection>
    </xsl:template>
    

    <xsl:template match="h:div" >
            <xsl:if test="h:p">
                <paragraph>
                <xsl:value-of select="."/>
                </paragraph>
            </xsl:if>
            <xsl:if test="h:ul">
                <xsl:apply-templates select="h:ul"></xsl:apply-templates>
            </xsl:if>
        
    </xsl:template>
    <xsl:template match="h:ul">
        <xsl:for-each select="h:li">
            <paragraph>
                <xsl:value-of select="h:p"/>
            </paragraph>
        </xsl:for-each>
        
    </xsl:template>
    


</xsl:stylesheet>
