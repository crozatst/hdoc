﻿Converter hdoc_to_basex
-----------------------

The purpose of this converter is to obtain an XML data file suitable for importation into basex for futher XQuery requests from a HDOC file

License GPL3.0
--------------

http://www.gnu.org/licenses/gpl-3.0.txt


Credits
-------

* Simei YIN
* Baptiste MONTANGE


Dependance
----------

This project can be used alone if you want to import an HDOC file into basex.


User stories
------------------
	- Among a group of courses, user can search by title, author or keywords of the course.
	- By searching a certain keyword, user can obtain the sections that contain it with their hierarchy levels in the course.
	- In a certain section, by searching a key word, user can obtain paragraphs that contain it.
	- User can get definitions related to a keyword
	- User can get examples whose titles contain a keyword

Step by step :

	[`Step 1 : File transformation`]
		- Put the files .hdoc you want to deal with in the folder [`input`](https://gitlab.utc.fr/crozatst/hdoc/tree/master/hdoc_to_basex/input)
		- Run the transformation progam (Win : double click run.bat, Linux : execute run.sh)
		- This transformation includes :
			* Title, authors, keywords
			* First Level section : title, type, paragraphs
	
	[`Step 2 : Create data base in basex`]
		- Download and install [BaseX](http://basex.org/products/download/all-downloads/)
		- Run BasexGui
		- In the Text Editor of BaseX, open the command script "createbd.bxs" in folder [`command`](https://gitlab.utc.fr/crozatst/hdoc/tree/master/hdoc_to_basex/basex/command).
		  Follow the instructions in the script, and then execute it.
	
	[`Step 3 : Make XQuery request`]
		- In the Text Editor of BaseX, you can open and execute xquery script "main.xq" in folder [`xquery`](https://gitlab.utc.fr/crozatst/hdoc/tree/master/hdoc_to_basex/basex/xquery).
		  It's in this main module that we call predefined functions and execute script
		  
		- .xqm files are library modules where we have defined functions, we can go to these files for more detailed using instructions
		  Available library modules :
			* searchDocByAuthor.xqm
			* searchDocByTitle.xqm
			* searchSectionByTitle.xqm
	
		P.S. the symbols "(: :)" are used for adding comments in xQuery files

TODO List
------------------
	- xlst Transformation :
		* Remove text formats
		* Sub sections
		* Exercices
		* Resources : images, ...
	
	- XQuery requests :
		* Search keywords in text 
		* Search sub sections by keywords in their titles
		* Search exercices by keywords in their titles