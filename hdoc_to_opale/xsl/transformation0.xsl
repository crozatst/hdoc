﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:c="urn:utc.fr:ics:hdoc:container"
    >
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

    <xsl:template match="c:container">
        <project name="getContentFileAndTransformIt" basedir="." default="start">
            <property file="global.properties"/>
            <property name="filename" location="${filename}"/>
            <property name="lib" location="${lib}"/>
            <taskdef name="jing" classname="com.thaiopensource.relaxng.util.JingTask">
                <classpath>
                    <pathelement location="../${lib}/jing.jar"/>
                </classpath>
            </taskdef>

            <xsl:apply-templates select="./c:rootfiles"/>
        </project>
    </xsl:template>

    <xsl:template match="c:rootfiles">
        <target name="start">
          <trycatch property="foo" reference="bar">
            <try>
              <jing file="${{tmp}}/${{filename}}/decompressedHdoc/{c:rootfile/@full-path}" rngfile="${{schema}}/hdoc1-xhtml.rng"></jing>
            </try>
            <catch>
              <echo>Validation failed</echo>
            </catch>
          </trycatch>
            <xslt
                in="${{tmp}}/${{filename}}/decompressedHdoc/{c:rootfile/@full-path}"
                out="${{tmp}}/${{filename}}/moveRessourceFiles.xml"
                style="${{xsl}}/moveRessourceFiles.xsl"
                processor="org.apache.tools.ant.taskdefs.optional.TraXLiaison"
            >
              <param name="filename" expression="${{tmp}}\${{filename}}"/>
            </xslt>
            <chmod file="${{tmp}}/${{filename}}/moveRessourceFiles.xml" perm="777"/>
            <xslt
                in="${{tmp}}/${{filename}}/decompressedHdoc/{c:rootfile/@full-path}"
                out="${{tmp}}/${{filename}}/decompressedOpale/main.xml"
                style="${{xsl}}/transformation2.xsl"
                processor="org.apache.tools.ant.taskdefs.optional.TraXLiaison"
                >
                <param name="filename" expression="${{tmp}}/${{filename}}"/>
            </xslt>

            <chmod file="${{tmp}}/${{filename}}/decompressedOpale/main.xml" perm="777"/>

            <!-- Finding references and converting them -->
            <xslt
                in="${{tmp}}/${{filename}}/decompressedHdoc/{c:rootfile/@full-path}"
                out="${{tmp}}/${{filename}}/convertReferences.xml"
                style="${{xsl}}/prepareReferencesConversions.xsl"
                processor="org.apache.tools.ant.taskdefs.optional.TraXLiaison"
            >
              <param name="filename" expression="${{tmp}}/${{filename}}"/>
            </xslt>
            <ant antfile="${{tmp}}/${{filename}}/convertReferences.xml"/>
        </target>
    </xsl:template>
</xsl:stylesheet>
