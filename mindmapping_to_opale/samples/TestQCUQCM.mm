<map version="1.0.1">
    <node ID="ID_1" TEXT=" NF29">
        <edge COLOR="#9900ff"/>
        <node ID="ID_2" POSITION="right" STYLE="fork" TEXT="#1 Noeud1">
            <node ID="ID_11" POSITION="right" STYLE="fork" TEXT="#def D&#233;finition">
                <node COLOR="#00ff00" ID="ID_12" POSITION="right" STYLE="fork" TEXT="#p Contenu 1"/>
                <node ID="ID_13" POSITION="right" STYLE="fork" TEXT="#p Contenu 2">
                    <font BOLD="true" ITALIC="true" SIZE="12"/>
                </node>
                <node ID="ID_14" POSITION="right" STYLE="fork" TEXT="#p Contenu 3">
                    <arrowlink DESTINATION="ID_8" STARTARROW="Default"/>
                </node>
            </node>
            <node ID="ID_5" POSITION="right" STYLE="fork" TEXT="#rmk Remarque"/>
            <node ID="ID_8" POSITION="right" STYLE="fork" TEXT="#compl Compl&#233;ment"/>
            <node ID="ID_32" POSITION="right" STYLE="fork" TEXT="#singlechoicequestion Ceci est l'&#233;nonc&#233; de la question pour un QCU">
                <node ID="ID_36" POSITION="right" STYLE="fork" TEXT="#choice-qcu Ceci est une r&#233;ponse"/>
                <node ID="ID_37" POSITION="right" STYLE="fork" TEXT="#choice-qcu Ceci est une autre r&#233;ponse"/>
                <node ID="ID_38" POSITION="right" STYLE="fork" TEXT="#solution 2"/>
                <node ID="ID_35" POSITION="right" STYLE="fork" TEXT="#explanation Ceci est l'explication de la r&#233;ponse"/>
            </node>
            <node ID="ID_9" POSITION="right" STYLE="fork" TEXT="#wrng Attention"/>
            <node ID="ID_10" POSITION="right" STYLE="fork" TEXT="#ex Exemple"/>
            <node ID="ID_7" POSITION="right" STYLE="fork" TEXT="#emph Emphase"/>
            <node ID="ID_25" POSITION="right" STYLE="fork" TEXT="#question Ceci est l'&#233;nonc&#233; de la question pour un QCM">
                <node ID="ID_26" POSITION="right" STYLE="fork" TEXT="#choice-correct Ceci est une bonne r&#233;ponse">
                    <node ID="ID_29" POSITION="right" STYLE="fork" TEXT="#explanation-choice-correct Ceci est l'explication locale de la bonne r&#233;ponse"/>
                </node>
                <node ID="ID_27" POSITION="right" STYLE="fork" TEXT="#choice-incorrect Ceci est une mauvaise r&#233;ponse">
                    <node ID="ID_30" POSITION="right" STYLE="fork" TEXT="#explanation-choice-incorrect Ceci est l'explication locale de la mauvaise r&#233;ponse"/>
                </node>
                <node ID="ID_28" POSITION="right" STYLE="fork" TEXT="#explanation Ceci est l'explication de la r&#233;ponse"/>
            </node>
        </node>
        <node ID="ID_16" POSITION="left" STYLE="fork" TEXT="Lambda">
            <node ID="ID_17" POSITION="left" STYLE="fork" TEXT="Fils 1">
                <node ID="ID_19" POSITION="left" STYLE="fork" TEXT="Petit-fils 1">
                    <node ID="ID_20" POSITION="left" STYLE="fork" TEXT="#p Paragraphe"/>
                </node>
            </node>
            <node ID="ID_18" POSITION="left" STYLE="fork" TEXT="Fils 2">
                <node ID="ID_21" POSITION="left" STYLE="fork" TEXT="Paragraphe"/>
            </node>
        </node>
        <node ID="ID_31" POSITION="right" STYLE="fork">
            <edge COLOR="#9900ff"/>
        </node>
        <node ID="ID_3" POSITION="right" STYLE="fork" TEXT="#2 Noeud2"/>
        <node ID="ID_4" POSITION="right" STYLE="fork" TEXT="#intro Intro"/>
        <node ID="ID_15" POSITION="right" STYLE="fork" TEXT="#conclu Conclusion">
            <node ID="ID_6" POSITION="right" STYLE="fork" TEXT="#adv Conseil"/>
        </node>
    </node>
</map>