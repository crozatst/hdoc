<map version="1.0.1">
    <node ID="ID_1" TEXT="Test NF29 1">
        <edge COLOR="#9900ff"/>
        <node ID="ID_2" POSITION="right" STYLE="fork" TEXT="#1 Noeud1">
            <node ID="ID_11" POSITION="right" STYLE="fork" TEXT="#def D&#233;finition">
                <node COLOR="#00ff00" ID="ID_12" POSITION="right" STYLE="fork" TEXT="#p Contenu 1"/>
                <node ID="ID_13" POSITION="right" STYLE="fork" TEXT="#p Contenu 2">
                    <font BOLD="true" ITALIC="true" SIZE="12"/>
                </node>
                <node ID="ID_14" POSITION="right" STYLE="fork" TEXT="#p Contenu 3">
                    <arrowlink DESTINATION="ID_8" ENDARROW="Default"/>
                </node>
            </node>
            <node ID="ID_5" POSITION="right" STYLE="fork" TEXT="#rmk Remarque"/>
            <node ID="ID_8" POSITION="right" STYLE="fork" TEXT="#compl Compl&#233;ment"/>
            <node ID="ID_9" POSITION="right" STYLE="fork" TEXT="#wrng Attention"/>
            <node ID="ID_10" POSITION="right" STYLE="fork" TEXT="#ex Exemple"/>
            <node ID="ID_7" POSITION="right" STYLE="fork" TEXT="#emph Emphase"/>
            <node ID="ID_6" POSITION="right" STYLE="fork" TEXT="#adv Conseil"/>
        </node>
        <node ID="ID_16" POSITION="left" STYLE="fork" TEXT="Lambda">
            <node ID="ID_17" POSITION="left" STYLE="fork" TEXT="Fils 1">
                <node ID="ID_19" POSITION="left" STYLE="fork" TEXT="Petit-fils 1">
                    <node ID="ID_20" POSITION="left" STYLE="fork" TEXT="#p Paragraphe"/>
                </node>
            </node>
            <node ID="ID_18" POSITION="left" STYLE="fork" TEXT="Fils 2">
                <node ID="ID_21" POSITION="left" STYLE="fork" TEXT="Paragraphe"/>
            </node>
        </node>
        <node ID="ID_3" POSITION="right" STYLE="fork" TEXT="#2 Noeud2"/>
        <node ID="ID_4" POSITION="right" STYLE="fork" TEXT="#intro Intro"/>
        <node ID="ID_15" POSITION="right" STYLE="fork" TEXT="#conclu Conclusion"/>
    </node>
</map>