Mindmapping to Opale
=================

Conversion of Freemind file(s) (.mm) to Opale (.scar)

## Licence
-------------------------------
http://www.gnu.org/licenses/gpl-3.0.txt

## Credits
-------------------------------
This section has been written by 

* Thibault BROCHETON
* Bastien FREMONDIERE
* Amélie PERDRIAUD
* Quentin KEUNEBROEK
* Alexandre GUTH

It is based on the work of Guillaume GOMEZ in 2014.

## Warning
-------------------------------

L'état courant du projet permet de faire une démonstration des fonctionnalités mais l'implémentation actuelle des balises n'est pas très judicieuse. En effet, les balises prennent largement le pas sur les comportements par défauts et l'implémentation en xsl empêche d'utiliser des fonctions natives de la mindmap. Nous conseillons ainsi aux suivants de reprendre l'ensemble du xsl et de fonctionner d'abord avec les fonctionnalités natives de la mindmap. Pour les balises, elles ne devraient apporter qu'un supplément d'informations et non pas restreindre les comportements disponibles. 

## Presentation
-------------------------------

The project aims at converting Freemind files in Opale files. If you use another tool to create mindmapping files, you can probably export it to a .mm file and use it in this converter.

## Dependence
-------------------------------
In dependence with mindmapping_to_hdoc and hdoc_to_opale

##Installation
-------------------------------
* To get the tool, go to the page https://gitlab.utc.fr/crozatst/hdoc/tree/master4 and click the download icon on the right of the screen : you can get a .zip version of the project. For the git users, the clone link is available on the same site.

## User documentation
-------------------------------

* Use a terminal and go to the root of the folder (mindmapping_to_opale).
* Please refer to the part *Rules to follow* to understand the rules that can help you to personalize the output
* Put your files in a folder 'mindmapping_to_opale/input'
* Enter the command line corresponding to your OS :
	* On Linux : 'sh run.sh'
	* On Windows : 'run.bat'
If you want to convert one specific file that is in the input folder, use the parameters ' -DinputPath input/<yourFilename>' (ex : 'sh run.sh -DinputPath input/<yourFilename>')

If the input folder happens to be missing, you should create your own input folder in the root of the mindmapping_to_opale folder, and add your own samples inside. 

*You will find the result of the conversion in the folder mindmapping_to_opale/output*
*You can find a sample file il the folder /samples.*

### Rules to follow

You can use special rules in Freemind to personnalize your Scenari Module :

* By default, a "Division" is created with the name of the main node of your MindMap
* Then :
	* A node without children is transformed in **grain content**
	* A node with children is transformed in **division**
* You can add introduction and conclusion using the hashtag **#intro** and **#conclu** on all the node that has no child
* You can use **#p** to create a paragraph, and **#a** to create a link
* You can use some hashtags to create specific contents :
	* **#rmk** : a remark
	* **#adv** : an advice
	* **#emph** : an emphasis
	* **#compl** : a complement
	* **#wrng** : a warning
	* **#ex** : an example
	* **#def** : a definition
	* **#question** : a MCQ
	* **#singlechoicequestion** : a QCU
	* **#choice-qcu** : an answer within a QCU
	* **#list** : a list inside a paragraph, first node being the title and children being the content. 
	* **#solution** : a solution associated to a single choice within a QCU (matches an integer)
	* **#choice-correct** : a correct answer within a MCQ
	* **#choice-incorrect** : an incorrect answer within a MCQ
	* **#explanation** : an explanation of the answers within a MCQ
	* *Then you can add some **#p** inside these node to create the content*
- You can arrange the order of the node usine the hashtag **#1, #2, #3** etc.

## Unsupported

* Convert colors
* Convert icon into summary elements
* HTML node

## Known bugs

* Currently, the xsl is lacking : there is no support for childs of nodes who are not named "nodes" themselves. The bug has been investigated and is therefore non trivial, and should be solved in order to handle functionnalities outside of the special rules formatting. 

## TODO

* Develop new hashtags to create other type of Opale contents (such as new exercises types, self-evaluation etc.)
* Explore native freemind functionalities to allow a more user friendly formatting : using the bold and italic formatting was tried but failed due to a xsl malfunction.

## Technicals notes

### Preamble
This subsection will explain precisely how the conversion process works. If you are reading this, we assume you are familiar with XML technologies seen in NF29 (i.e. ANT scripts, XSL-XSLT transformations and Hdoc format). We also recommend you to open sources files and read the comments.
Besides, the converter have been tested on recent ANT versions (> 1.7.0) only. It depends on hdoc_to_opale module.

### How does the converter works
This converter is using standard NF29 conversion project structure : a main ANT file (mindmapping_to_opale.ant), which handles routine tasks (zipping archives, copying files, order tasks), XSL-XSLT transformation scripts calls. This main ANT file is composed of several targets, and supports both Windows or Linux platforms.
This ant file is called by a script (run.sh or run.bat), with ou without parameters.

### Hdoc files
During the conversion process, the converter is using the converter mindmapping_to_hodc. The result of this conversion is also available in the folder output.

### What does the main ANT file do ?

* Delete and create folder temp, output, output/hdoc and output/scar
* Check if the OS is Windows or Mac, and call the appropriate target
* If there is no input path in parameters,  it converts all the files present in the input folder
* It calls the converter mindmapping_to_hdoc to convert .mm files in .hdoc files.
* Copy the results of the conversion in the folder /output/hdoc
* It calls the converter hdoc_to_opale to convert .hdoc files in .scar files
* Copy the results of the conversion in the folder /output/scar
* Finally, clean temporary files

## Capitalization

* Concerning the xsl malfunction, we know that the issue comes from a mispriority between nodes without childs and nodes with childs who aren't named "nodes". There is a confusion between the two because previous implementation didn't take the seconds into account. We didnt manage to find a way to force the xsl to recognize those nodes, but this is where you should look if you happen to need to fix the issue. A simple but time consuming fix would be to rewrite the xsl with the functionnality in mind, but we didn't took that option as we lacked time. The problematic functions are at the root of the xsl transformation, and therefore a simple fix hasn't been found yet.
* There's post on the Scenari forum to explain what has been achieved so far with the mindmapping_to_opale project. Here is the link to the post : https://forums.scenari.org/t/transformation-dune-framindmap-en-module-opale/1079

