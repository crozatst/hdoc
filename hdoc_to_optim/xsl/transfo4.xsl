﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
    xmlns:of="scpf.org:office"
    xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
    xmlns:h="http://www.utc.fr/ics/hdoc/xhtml">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="h:html">
        <sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
            <of:page xmlns:of="scpf.org:office" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
                <xsl:apply-templates/>
            </of:page>
        </sc:item>
    </xsl:template>
    <xsl:strip-space elements="*"/>
	
    <!--Metadata of the head tag-->
    <xsl:template match="h:head">
        <of:pageM>
            <xsl:apply-templates select="h:title"/>
            <!-- <sp:commonMeta>
                <of:commonM>
                    <xsl:if test="h:meta[@name='author'] or h:meta[@name='rights']">
                        <sp:copyright>
                            <xsl:value-of select="h:meta[@name='author']/@content"/>
                            <xsl:if test="h:meta[@name='author'] and h:meta[@name='rights']"> - </xsl:if>
                            <xsl:value-of select="h:meta[@name='rights']/@content"/>
                        </sp:copyright>
                    </xsl:if>
                    <xsl:if test="h:meta[@name='date']">
                        <sp:version>
                            <xsl:value-of select="h:meta[@name='date']/@content"/>
                        </sp:version>
                    </xsl:if>
                </of:commonM> 
            </sp:commonMeta>-->
        </of:pageM>
        <!--in optim there is no tag for description and keywords, they are added in a preliminary part-->
        <!--<xsl:if test="h:meta[@name='description'] or h:meta[@name='keywords']">
            <sp:pre>
                <of:section>
                    <of:sectionM>
                        <sp:title>Metadata</sp:title>
                    </of:sectionM>
                    <sp:content>
                        <of:fragment>
                            <xsl:apply-templates select="h:meta[@name='description']"/>
                            <xsl:apply-templates select="h:meta[@name='keywords']"/>
                        </of:fragment>
                    </sp:content>
                </of:section>
            </sp:pre>
        </xsl:if>-->
    </xsl:template>
    
    <!--<xsl:template match="h:meta[@name='description'] | h:meta[@name='keywords']">
        <sp:info>
            <of:block>
                <of:blockM>
                    <sp:title>
                        <xsl:if test="@name='description'">Description</xsl:if>
                        <xsl:if test="@name='keywords'">Keywords</xsl:if>
                    </sp:title>         
                </of:blockM>
                <sp:co>
                    <of:flow>
                        <sp:txt>
                            <of:txt>
                                <sc:para xml:space="preserve">
                                <xsl:value-of select="@content"/>
                            </sc:para>
                            </of:txt>
                        </sp:txt>
                    </of:flow>
                </sp:co>
            </of:block>
        </sp:info>
    </xsl:template>-->
    
    <xsl:template match="h:title | h:h1 | h:h6">
        <sp:title><xsl:value-of select="."/></sp:title>
    </xsl:template>
    
    <xsl:template match="h:body">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="h:body/h:section[1]" priority="2">
        <sp:main>
            <xsl:call-template name="sec-body"/>
        </sp:main>
    </xsl:template>
    
    <!--hdoc sections can become chapters or sections in Optim, we must differentiate the two-->
    <xsl:template match="h:body/h:section">
		<xsl:variable name="chapi" select="(.//text())[1]"/>
		<xsl:message><xsl:value-of select="$chapi"/></xsl:message>
		<xsl:variable name="chap-norm">
		   <xsl:choose>
			 <xsl:when test="contains($chapi,':')">
				<xsl:message>Yescontiiii!</xsl:message>
				<xsl:value-of select="normalize-space(replace($chapi,':','-'))"/>
			 </xsl:when>
			 <xsl:otherwise><xsl:value-of select="normalize-space($chapi)"/></xsl:otherwise>
		   </xsl:choose>
		</xsl:variable>
		<xsl:message><xsl:value-of select="$chap-norm"/></xsl:message>
		<xsl:message>test</xsl:message>
        
        <xsl:variable name="secNum">
            <xsl:number count="h:body/h:section" format="1" level="any"/>
        </xsl:variable>
        <xsl:variable name="chap-num">
            <xsl:value-of select="concat($secNum,'-',$chap-norm)"/>
        </xsl:variable>
        
        <sp:subPage sc:refUri="pages/{$chap-num}.section">
			<of:filter/>
            <xsl:call-template name="sec-body1">
                <xsl:with-param name="chap" select = "$chap-num" />
			</xsl:call-template>
        </sp:subPage>
    </xsl:template>
    
    <xsl:template match="h:section">
        <sp:sec>
            <xsl:call-template name="sec-body"/>
        </sp:sec>
    </xsl:template>
	
	<xsl:template name="sec-body1">
		<xsl:param name = "chap" />
		<xsl:message><xsl:value-of select="$chap"/></xsl:message>
		<xsl:result-document encoding="UTF-8" href="pages/{$chap}.section" >
			<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
				<of:section>
					<xsl:if test="@data-hdoc-type='introduction'">
						<xsl:comment>Introduction</xsl:comment>
					</xsl:if>
					<xsl:if test="@data-hdoc-type='conclusion'">
						<xsl:comment>Conclusion</xsl:comment>
					</xsl:if>
					<xsl:apply-templates select="h:header"/>
					<xsl:if test="./h:div">
						<sp:content>
							<of:fragment>
								<xsl:apply-templates select="h:div"/>
							</of:fragment>
						</sp:content>
					</xsl:if>
					<xsl:apply-templates select="h:section"/>
				</of:section>
			</sc:item>
		</xsl:result-document>
        <xsl:apply-templates select="h:footer"/>
    </xsl:template>
    
    <xsl:template name="sec-body">
        <of:section>
            <xsl:if test="@data-hdoc-type='introduction'">
                <xsl:comment>Introduction</xsl:comment>
            </xsl:if>
            <xsl:if test="@data-hdoc-type='conclusion'">
                <xsl:comment>Conclusion</xsl:comment>
            </xsl:if>
            <xsl:apply-templates select="h:header"/>
            <xsl:if test="./h:div">
                <sp:content>
                    <of:fragment>
                        <xsl:apply-templates select="h:div"/>
                    </of:fragment>
                </sp:content>
            </xsl:if>
            <xsl:apply-templates select="h:section"/>
        </of:section>
        <xsl:apply-templates select="h:footer"/>
    </xsl:template>
    
    <xsl:template match="h:header">
        <of:sectionM>
            <xsl:apply-templates/>
        </of:sectionM>
    </xsl:template>
    
    <xsl:template match="h:footer">
        <xsl:apply-templates/>
    </xsl:template>
    
    <!--there are no tags for metadata in Optim sections, so we add this information in the comments-->
    <xsl:template match="h:div[@data-hdoc-type]">
        <xsl:comment>
            <xsl:if test="@data-hdoc-type='author'">Author : <xsl:value-of select="."/></xsl:if>
            <xsl:if test="@data-hdoc-type='date'">Date : <xsl:value-of select="."/></xsl:if>
            <xsl:if test="@data-hdoc-type='rights'">Rights : <xsl:value-of select="."/></xsl:if>
            <xsl:if test="@data-hdoc-type='tags'">Tags : <xsl:for-each select="h:span"><xsl:value-of select="."/>, </xsl:for-each></xsl:if>
            <xsl:if test="@data-hdoc-type='categories'">Categories : <xsl:for-each select="h:span"><xsl:value-of select="."/>, </xsl:for-each></xsl:if>
        </xsl:comment>
    </xsl:template>
    
    <xsl:template match="h:div">
        <sp:info>
            <xsl:call-template name="div-body"/>
        </sp:info>
    </xsl:template>
    
    <xsl:template match="h:div[@data-hdoc-type='emphasis'] | h:div[@data-hdoc-type='definition'] | h:div[@data-hdoc-type='warning']">
        <sp:emphasis>
            <xsl:if test="@data-hdoc-type='definition'">
                <xsl:comment>Definition</xsl:comment>
            </xsl:if>
            <xsl:if test="@data-hdoc-type='warning'">
                <xsl:comment>Warning</xsl:comment>
            </xsl:if>
            <xsl:if test="@data-hdoc-type='date'">Date : <xsl:value-of select="."/></xsl:if>
            <xsl:call-template name="div-body"/>
        </sp:emphasis>
    </xsl:template>
    
    <xsl:template match="h:div[@data-hdoc-type='complement'] | h:div[@data-hdoc-type='example'] | h:div[@data-hdoc-type='remark'] | h:div[@data-hdoc-type='advice']">
        <sp:extra>
            <xsl:if test="@data-hdoc-type='example'">
                <xsl:comment>Example</xsl:comment>
            </xsl:if>
            <xsl:if test="@data-hdoc-type='remark'">
                <xsl:comment>Remark</xsl:comment>
            </xsl:if>
            <xsl:if test="@data-hdoc-type='advice'">
                <xsl:comment>Advice</xsl:comment>
            </xsl:if>
            <xsl:call-template name="div-body"/>
        </sp:extra>
    </xsl:template>
    
    <xsl:template name="div-body">
        <of:block>
            <of:blockM>
                <xsl:apply-templates select="h:h6"/>
            </of:blockM>
            <sp:co>
                <of:flow>
                    <sp:txt>
                        <of:txt>
                            <xsl:apply-templates select="*[not(self::h:h6)]"/>
                        </of:txt>
                    </sp:txt>
                </of:flow>
            </sp:co>
        </of:block>
    </xsl:template>
    
    <xsl:template match="h:span">
        <xsl:apply-templates/>
    </xsl:template>
    
    <!--Text-->
    <!--paragraph-->   
    <xsl:template match="h:p">
        <sc:para xml:space="preserve">
        <xsl:apply-templates/>
    </sc:para>
    </xsl:template>
    
    <!--list-->
    <xsl:template match="h:ul">
        <sc:itemizedList>
            <xsl:apply-templates/>
        </sc:itemizedList>
    </xsl:template>
    
    <xsl:template match="h:ol">
        <sc:orderedList>
            <xsl:apply-templates/>
        </sc:orderedList>
    </xsl:template>
    
    <xsl:template match="h:li">
        <sc:listItem>
            <xsl:apply-templates/>
        </sc:listItem>
    </xsl:template>
    
    <!--url-->
    <xsl:template match="h:a">
        <sc:uLink role="url" url="{@href}">
            <xsl:apply-templates/>
        </sc:uLink>
    </xsl:template>
    
    <xsl:template match="h:em">
        <sc:inlineStyle role="emphasis">
            <xsl:apply-templates/>
        </sc:inlineStyle>
    </xsl:template>
    
    <xsl:template match="h:i">
        <sc:phrase role="special">
            <xsl:apply-templates/>
        </sc:phrase>
    </xsl:template>
    
    <xsl:template match="h:q">
        <sc:phrase role="quote">
            <xsl:apply-templates/>
        </sc:phrase>
    </xsl:template>
    
    <!--Table-->
    <xsl:template match="h:table">
        <sc:table role="">
            <xsl:apply-templates select="h:caption"/>
            <xsl:for-each select="h:tr[1]/h:td">
                <sc:column width="20"/>
            </xsl:for-each>
            <xsl:apply-templates select="h:tr"/>
        </sc:table>
    </xsl:template>
    
    <xsl:template match="h:caption">
        <sc:caption xml:space="preserve">
        <xsl:value-of select="."/>
    </sc:caption>
    </xsl:template>
    
    <xsl:template match="h:tr">
        <sc:row>
            <xsl:apply-templates/>
        </sc:row>
    </xsl:template>
    
    <xsl:template match="h:td">
        <sc:cell>
            <xsl:apply-templates/>
        </sc:cell>
    </xsl:template>
    
    <!--Object-->
    <xsl:template match="h:object">
        <xsl:variable name="data" select="@data"/>
        <sc:extBlock role="img" sc:refUri="../{$data}"/>
    </xsl:template>
    
    <!--Image-->
    <xsl:template match="h:img">
        <xsl:variable name="src" select="@src"/>
        <sc:extBlock role="img" sc:refUri="../{$src}"/>
    </xsl:template>
    
    <!--Audio-->
    <xsl:template match="h:audio">
        <xsl:variable name="src" select="@src"/>
        <sc:extBlock role="anim" sc:refUri="../{$src}"/>
    </xsl:template>
	
	<!--Video-->
    <xsl:template match="h:video">
        <xsl:variable name="src" select="@src"/>
        <sc:extBlock role="anim" sc:refUri="../{$src}"/>
    </xsl:template>
    
</xsl:stylesheet>