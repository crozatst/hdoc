<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0"
    xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
    xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
    xmlns:op="utc.fr:ics/opale3">
    
    <xsl:variable name="ProjectFileName" select="'opale_to_hdoc'"/>
    
    <xsl:template match="sc:item">
        <project name='Opale2Hdoc' default="main">
            <target name='main'>
                <xsl:apply-templates select="//sp:res[@sc:refUri]"/>
            </target>
        </project>
    </xsl:template>
    
    <xsl:template match="sp:res[@sc:refUri]">
        <xsl:variable name="link" select="@sc:refUri"></xsl:variable>
        <xsl:variable name="name" select="tokenize($link, '/')[last()]"></xsl:variable>
        <xsl:variable name="link2" select="tokenize($link, concat($ProjectFileName, '/'))[last()]"></xsl:variable>
        <copy file="{$link2}/{$name}" todir="output/hdoc/re"/>
    </xsl:template>
    
    <xsl:template match="sp:res[contains(@sc:refUri,'.xml')]"/>
    
    <xsl:template match="*"></xsl:template>
    
</xsl:stylesheet>