﻿Converter opale_to_hdoc
-----------------------

The purpose of this converter is to obtain an HDOC file from an Opale document. 


License GPL3.0
--------------

http://www.gnu.org/licenses/gpl-3.0.txt


Credits
-------

* Thibault Drain

* Christophe Virot

* Pierre Lemaire


Dependance
----------


This project can be used alone if you only want to convert an Opale .scar into an HDOC file.


User documentation
------------------

You have to respect the following steps :

1. First, if you want to transform your  Opale publication to a Hdoc format, 
you have to export a module file as a scar. 

	Extract a module file from your Opale workspace. You can't export directly a web, paper or slide show support. You must extract a scar module. Moreover, when you extract a module, keep the default configuration :
	
	* "Include descendant network" input : checked
	* "keep workspaces" : unchecked

2. Then you just have to put your scar in the input directory inside the project opale_to_hdoc. Place only one file in that folder !!

3. Finally, you can either launch the bash, the bat or start the ant by
typing the following instruction : 
	
		ant -buildfile opale_to_hdoc


Unsupported 
-----------

* Module
	* Titre court
	* Références générales
	* Activité d'auto évaluation
	* Exercice rédactionnel
	* Exercice QCM
	* Exercice QCM graphique
	* Exercice QCU
	* Exercice QCU graphique
	* Exercice catégorisation
	* Exercice ordonnancement
	* Exercice texte à trous
	* Exercice question à réponse courte
	* Liste d'exercice

* Activité d'apprentissage
	* Titre court
	* Exercice rédactionnel
	* Exercice QCU
	* Exercice QCU graphique
	* Exercice QCM
	* Exercice QCM graphique
	* Exercice catégorisation
	* Exercice ordonnancement
	* Exercice texte à trous
	* Exercice question à réponse courte
	* Liste d'exercice

* Grain de contenu
	* Titre court

* Blocs
	* All bloc types are supported

* Types de texte
	* Ressources (ressources de type animation non supporté)
	* Fichier en téléchargement

* Éléments présents dans un texte
	* Formule mathématique
	* Abréviation
	* Glossaire
	* Références
	* Lien vers un document
	* Renvoi vers une ressource
	* Renvoi vers un grain de contenu
	* Animation


Known bugs
----------

All the ressources are not managed yet. Thus, sometimes the markup sp:res is not visited by the program and it ends up with blank divs inside the result.hdoc which prevents it from being valid. Moreover, we're not 100% sure that there always will be a markup sp:index to  define the type of the ressource. However, now the index is used to determine wether the target hdoc markup has to be a img or object (video, audio remain in development).


Todo
---- 

* Listings are simply put inside p markups and should be externalized as pictures (ressources) in order to maintain the indent.

* Externalized opale UC and UA files are full of relative path that point multiple kinds of references. Our ant program begins by merging all these files into the root opale file. Thus the relative paths become wrong. Next group could look at what has been done with ressources (sp:res) in order to handle references the same way.

* Presently, you cannot select one scar file inside the input repository. It could be useful to define an InputPath parameter that would either have the value of the only file set in input or would have the value of a submissed relative path.

* The next group should manage the version of hdoc.


Technical notes
---------------

The zip file includes 7 files :

* opale_to_hdoc.ant : which executes instructions (unzips compressed files, calls xsl, etc.)

* opale_to_hdoc.properties : linked to the ANT file (may contain ant properties)

* opale_to_hdoc_regle1.xsl : which creates an only XML source file

* opale_to_hdoc_regle2.xsl : which converts the XML source file to a hdoc file

* container.xsl : which changes the container's schemas to hdoc schemas

* find_ressources.xsl : which creates an ant file to move all resource files from the original scar directory to the output hdoc directory

* saxon9he.jar : Stored in the lib folder, this file allows the project to use the same xsl processor oxygen uses.

The sample is a Opale scar (an exported module). You can use it to test  the converter. It contains several elements which are currently supported by the converter.


Capitalisation
--------------

During this semester, several converters have been realized. The goal is to transform different formats (mainly scenari formats) into others.

The opale_to_hdoc converter transcribes an Opale publication to a hdoc format. Opale has many elements and all are not supported by the converter yet. Currently, it can handle the majority of "Modules", "Activités d'apprentissage" and "Grains de contenu".

The source code converter is available and can be completed in order to 
improve it and manage more Opale elements.


Modified by Benoit Villain (12/12/2016)
------------------
Les divisions et les parties d'un grain n'étaient pas traitées... Contrairement à ce que mentionne le README...
J'ai donc apporté les modifications nécessaires pour que les divisions et les parties (récursives) soient bel et bien transformées en "section" hdoc.


