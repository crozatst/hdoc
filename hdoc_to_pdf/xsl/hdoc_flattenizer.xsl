<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
  xpath-default-namespace="http://www.w3.org/1999/xhtml"
  xmlns="http://www.w3.org/1999/xhtml"
  >

  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

  <!-- Ajout de l'attribut lang -->
  <xsl:template match="html">
    <html lang="fr" xml:lang="fr">
      <xsl:apply-templates/>
    </html>
  </xsl:template>

  <!-- Ajout de la référence au CSS -->
  <xsl:template match="head">
    <head>
      <xsl:apply-templates select="node()|@*"/>
      <link>
        <xsl:attribute name="rel">stylesheet</xsl:attribute>
        <xsl:attribute name="type">text/css</xsl:attribute>
        <xsl:attribute name="href">main.css</xsl:attribute>
        <xsl:attribute name="media">print</xsl:attribute>
      </link>
    </head>
  </xsl:template>

  <!-- "Aplatissement" des sections en div avec numérotation correcte des hn -->
  <xsl:template match="section">
    <xsl:variable name="level" select="count(ancestor::section) + 2"/>
    <div class="section">
      <xsl:element name="h{$level}">
        <xsl:attribute name="id">
          <xsl:value-of select="concat('h', $level, '-')"/>
          <xsl:number level="any"/>
        </xsl:attribute>
        <xsl:value-of select="header/h1"/>
      </xsl:element>
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:template match="header"/>

  <xsl:template match="h6">
    <xsl:variable name="level" select="count(ancestor::section) + 2"/>
    <xsl:element name="h{$level}">
      <xsl:attribute name="id">
        <xsl:value-of select="concat('h', $level, '-')"/>
        <xsl:number level="any"/>
      </xsl:attribute>
      <xsl:value-of select="."/>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>
