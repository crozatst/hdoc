<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
  xpath-default-namespace="http://www.w3.org/1999/xhtml"
  xmlns="http://www.w3.org/1999/xhtml">

  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

  <!-- Traitement du titre et des auteurs du document -->
  <xsl:template match="body">
    <body>

      <xsl:apply-templates select="node()|@*"/>

      <xsl:call-template name="toc">
        <xsl:with-param name="toclevel" select="2"/>
      </xsl:call-template>
    </body>
  </xsl:template>

  <!-- Gestion de la table des matières -->

  <xsl:template name="toc">

       <xsl:param name="toclevel"/>
       <xsl:variable name="hnlevel" select="concat('h', $toclevel)"/>

       <xsl:choose>

         <xsl:when test="$toclevel = 2">
           <xsl:if test="count(//*[local-name()=$hnlevel]) &gt; 0">
             <h2 class="nocount toc-title">Table des matières</h2>

             <ul class="toc level2">
               <xsl:for-each select="//*[local-name()=$hnlevel]">
                 <li>
                   <xsl:call-template name="toc-a"/>

                   <xsl:call-template name="toc">
                     <xsl:with-param name="toclevel" select="3"/>
                   </xsl:call-template>

                 </li>
               </xsl:for-each>
             </ul>
           </xsl:if>
         </xsl:when>

         <xsl:otherwise>
           <xsl:if test="count(..//*[local-name()=$hnlevel]) &gt; 0">

             <ul class="toc level{$toclevel}">
               <xsl:for-each select="..//*[local-name()=$hnlevel]">
                 <li>
                   <xsl:call-template name="toc-a"/>

                   <xsl:if test="$toclevel &lt; 4">
                     <xsl:call-template name="toc">
                       <xsl:with-param name="toclevel" select="$toclevel + 1"/>
                     </xsl:call-template>
                   </xsl:if>
                 </li>
               </xsl:for-each>
             </ul>
           </xsl:if>
         </xsl:otherwise>

       </xsl:choose>

   </xsl:template>

  <xsl:template name="toc-a">
    <a>
      <xsl:attribute name="data-type">toc</xsl:attribute>
      <xsl:attribute name="href">
        <xsl:text>#</xsl:text>
        <xsl:value-of select="./@id"/>
      </xsl:attribute>
      <xsl:value-of select="."/>
    </a>
    <span>
      <xsl:attribute name="href">
        <xsl:text>#</xsl:text>
        <xsl:value-of select="./@id"/>
      </xsl:attribute>
    </span>
  </xsl:template>



</xsl:stylesheet>
