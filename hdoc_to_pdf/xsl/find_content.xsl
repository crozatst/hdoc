<?xml version="1.0" encoding="UTF-8"?>
<!--find_content.xsl creates a ANT file prepare_hdoc.ant-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
    xmlns:h="urn:utc.fr:ics:hdoc:container"
    xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xsl:param name="BaseDirectory" required="yes" as="xs:string"/>
  <xsl:param name="TempDirectory" required="yes" as="xs:string"/>

    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="h:container">
        <project name="hdoc_to_pdf" basedir=".." default="main">
            <property name="lib" location="lib"/>
            <property name="Schema" location="schemas"/>
            <taskdef name="jing" classname="com.thaiopensource.relaxng.util.JingTask">
                <classpath>
                    <pathelement location="lib/jing.jar"/>
                </classpath>
            </taskdef>
            <target name="main">
                <xsl:apply-templates/>
            </target>
        </project>
    </xsl:template>

    <xsl:template match="h:rootfiles">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="h:rootfile">
        <xsl:variable name="path" select="@full-path"/>

        <xsl:analyze-string select="$path" regex="\.?[-_0-9a-zA-Z]+(\.[-_0-9a-zA-Z]+)?$">
            <xsl:matching-substring>
            <echo message="MATCHING TEST  :"/>
            <echo>
                <xsl:attribute name="message"><xsl:value-of select="."/></xsl:attribute>
            </echo>
            <xsl:variable name="filename">
                <xsl:value-of select="."/>
            </xsl:variable>

            <xsl:variable name="dirpath" select="concat($TempDirectory, '/', substring($path, 1, string-length($path) - string-length($filename) - 1))"/>

            <echo message="Path : {$path}"/>
            <echo message="Filename : {$filename}"/>
            <echo message="Dirpath : {$dirpath}"/>

            <trycatch property="foo" reference="bar">
                <try>
                    <jing file="{$TempDirectory}/{$path}" rngfile="${{Schema}}/xhtml/hdoc1-xhtml.rng"></jing>
                    <echo>content.xml is valid</echo>
                </try>
                <catch>
                    <echo>Warning : Validation for content.xml failed</echo>
                </catch>
            </trycatch>

            <copy file="{$BaseDirectory}/css/main.css" tofile="{$dirpath}/main.css"/>
            <copy file="{$BaseDirectory}/font/cmunrm.ttf" tofile="{$dirpath}/cmunrm.ttf"/>
            <copy file="{$BaseDirectory}/font/cmunrb.ttf" tofile="{$dirpath}/cmunrb.ttf"/>
            <copy file="{$BaseDirectory}/font/cmunti.ttf" tofile="{$dirpath}/cmunti.ttf"/>
            <copy file="{$BaseDirectory}/font/cmuntt.ttf" tofile="{$dirpath}/cmuntt.ttf"/>

            <xslt in="{$TempDirectory}/{$path}" out="{$dirpath}/hdoc_nsless.xhtml" style="xsl/ns_remover.xsl" classpath="./lib/saxon9he.jar"/>
            <xslt in="{$dirpath}/hdoc_nsless.xhtml" out="{$dirpath}/flat_hdoc.xhtml" style="xsl/hdoc_flattenizer.xsl" classpath="./lib/saxon9he.jar"/>
            <xslt in="{$dirpath}/flat_hdoc.xhtml" out="{$dirpath}/post_toc.xhtml" style="xsl/toc_generator.xsl" classpath="./lib/saxon9he.jar"/>
            <xslt in="{$dirpath}/post_toc.xhtml" out="{$dirpath}/pre_final.xhtml" style="xsl/microformat_transformer.xsl" classpath="./lib/saxon9he.jar"/>
            <xslt in="{$dirpath}/pre_final.xhtml" out="{$dirpath}/final.xhtml" style="xsl/last_transformer.xsl" classpath="./lib/saxon9he.jar"/>

            <propertyfile file="{$TempDirectory}/my.properties">
                <entry key="dirpath" value="{$dirpath}"/>
            </propertyfile>

            </xsl:matching-substring>
        </xsl:analyze-string>

    </xsl:template>
</xsl:stylesheet>
