<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
  xpath-default-namespace="http://www.w3.org/1999/xhtml"
  xmlns="http://www.w3.org/1999/xhtml">

  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

  <!-- Traitement des micro-formats -->
  <xsl:template match="node()[local-name()=('h2', 'h3', 'h4', 'h5', 'h6') and parent::div[@data-hdoc-type=('definition', 'example', 'remark', 'advice', 'warning', 'complement', 'emphasis')]]">

    <xsl:variable name="prename" select="./text()"/>
    <xsl:variable name="type" select="parent::div/@data-hdoc-type"/>
    <xsl:variable name="prefix">
      <xsl:choose>
        <xsl:when test="$type = 'definition' and not(lower-case(./text()) = ('definition', 'définition'))">
          <xsl:text>Définition – </xsl:text>
        </xsl:when>
        <xsl:when test="$type = 'example' and not(lower-case(./text()) = ('exemple'))">
          <xsl:text>Exemple – </xsl:text>
        </xsl:when>
        <xsl:when test="$type = 'remark' and not(lower-case(./text()) = ('remarque'))">
          <xsl:text>Remarque – </xsl:text>
        </xsl:when>
        <xsl:when test="$type = 'advice' and not(lower-case(./text()) = ('conseil'))">
          <xsl:text>Conseil – </xsl:text>
        </xsl:when>
        <xsl:when test="$type = 'warning' and not(lower-case(./text()) = ('attention'))">
          <xsl:text>Attention – </xsl:text>
        </xsl:when>
        <xsl:when test="$type = 'complement' and not(lower-case(./text()) = ('complement', 'complément'))">
          <xsl:text>Complément – </xsl:text>
        </xsl:when>
        <xsl:when test="$type = 'emphasis' and not(lower-case(./text()) = ('fondamental'))">
          <xsl:text>Fondamental – </xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:value-of select="concat($prefix,  $prename)"/>
    </xsl:copy>

  </xsl:template>

</xsl:stylesheet>
