<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet
  version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xpath-default-namespace="http://www.utc.fr/ics/hdoc/xhtml"
  xmlns="http://www.w3.org/1999/xhtml"
  >

  <xsl:output method="xml" indent="yes"/>

  <!-- Identity transformation -->
  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

  <!-- Namespace substitution for hdoc elements -->
  <xsl:template match="*" priority="1">
    <xsl:element name="{local-name()}">
      <xsl:apply-templates select="node()|@*"/>
    </xsl:element>
  </xsl:template>

  <!-- Suppress processing-instructions -->
  <xsl:template match="processing-instruction()" priority="1"/>

</xsl:stylesheet>
