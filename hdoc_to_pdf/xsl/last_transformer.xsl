<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
  xpath-default-namespace="http://www.w3.org/1999/xhtml"
  xmlns="http://www.w3.org/1999/xhtml">

  <xsl:output method="xml" indent="no"/>

  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

  <!-- Traitement du titre et des auteurs du document -->
  <xsl:template match="body">
    <body>
      <h1><xsl:value-of select="/html/head/title"/></h1>
      <p class="authors"><xsl:value-of select="/html/head/meta[@name='author']/@content"/></p>

      <xsl:apply-templates select="node()|@*"/>
    </body>
  </xsl:template>

  <!-- Suppression des div de section -->
  <xsl:template match="div[@class='section']">
    <xsl:apply-templates select="node()"/>
  </xsl:template>

  <!-- Suppression des div de micro-formats -->
  <xsl:template match="div[@data-hdoc-type=('definition', 'example', 'remark', 'advice', 'warning', 'complement', 'emphasis')]">
    <xsl:apply-templates select="node()"/>
  </xsl:template>

  <!-- Suppression des div génériques -->
  <xsl:template match="div">
    <xsl:apply-templates select="node()"/>
  </xsl:template>

  <!-- Traitement des liens hypertextes pour autoriser le saut à la ligne-->
  <xsl:template match="a[not(@data-type = 'toc')]">
    <a>
      <xsl:copy-of select="@*" />
      <xsl:call-template name="addWordBreak">
        <xsl:with-param name="input" select="."/>
      </xsl:call-template>
    </a>
  </xsl:template>

  <xsl:template name="addWordBreak">
    <xsl:param name="input"/>
    <xsl:value-of select="substring($input,1,1)"/><wbr />
    <xsl:if test="substring($input,2)">
      <xsl:call-template name="addWordBreak">
        <xsl:with-param name="input" select="substring($input,2)"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <!-- Cleaning des footers vides -->
  <xsl:template match="footer[not(node())]"/>

  <!-- Cleaning des tags -->
  <xsl:template match="div[@data-hdoc-type='tags']"/>

</xsl:stylesheet>
