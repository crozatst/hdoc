import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.xhtmlrenderer.pdf.ITextRenderer;

import com.lowagie.text.DocumentException;


public class MyPDFGenerator {
    public static void main(String[] args) throws IOException, DocumentException {
        
		//Paramètrage du fichier à convertir
		//String baseFolder = System.getProperty("user.dir") + "/"; // correspond à la racine du projet Java

		
		String entree = args[0];
		String sortie = args[1];
    	
		File f = new File(entree);
		if(f.exists() && !f.isDirectory()) { 
			OutputStream os = new FileOutputStream(sortie); 

	        //Génération du PDF avec Flying Saucer
	        XHTMLToPDF(entree, os);
	        
	        //fermeture de l'outputStream
	        os.close();
		}
		else{
			System.err.println("Input file doesn't exist");
		}
    }
    
    public static void XHTMLToPDF(String entree, OutputStream os) throws IOException, DocumentException {
    	//Génération du PDF avec Flying Saucer
    	   	
        ITextRenderer renderer = new ITextRenderer();    	    	
        renderer.setDocument(new File(entree));      
        renderer.layout();
        renderer.createPDF(os);
    }
}
