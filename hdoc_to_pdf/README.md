﻿Converter hdoc_to_pdf
-----------------------

The purpose of this converter is to obtain a PDF file from a hdoc document.


License GPL3.0
--------------

http://www.gnu.org/licenses/gpl-3.0.txt


Credits
-------

*   2016
    - Raphaël Debray
    - Baptiste Perraud


Dependance
----------


This project can be used alone if you only want to convert a hdoc file into a PDF file.


User documentation
------------------

There are two different ways to use the converter hdoc_to_pdf: by running a script run.bat/run.sh or by command line using a terminal (allows the user to specify some parameters).
The folder samples contains a hdoc file which may be used for some tests.

#### Running the script run.bat/run.sh:

Use this method if you do not want to use a terminal.

1. Download hdoc_converter.zip and unzip it.
2. Add your source file into the input folder. It must be a .hdoc file.
3. Place _only one file_ in that folder.
4. On Linux or Mac, run the script run.sh. On Windows, run the script run.bat.
5. Your file has been converted, the result is in the output folder.  

#### Terminal:

By using the terminal you can specify one parameter to the conversion at the moment: the source file.

1. Download hdoc_converter.zip and unzip it.
2. Open your terminal and go into the folder hdoc_to_pdf.
3. Run the following command:

    "ant -buildfile hdoc_to_pdf.ant"

    You can specify the source file by adding parameters.
    Use -DInputFile to specify the source file.
    Exemple:

    "ant -buildfile hdoc_to_pdf.ant -DInputFile=sample.hdoc"


This parameter is optional. Your file has been converted, the result is in the output folder.



Flying Saucer limitations
-------------------------

* Nested ul in ol are sometimes converted to ol... [only noticed once, to be verified]
* It seems that FS doesn't support the max-width or max-height for img tags, which makes proper scaling harder... For now, as a temporary solution, we scale all images at a width of 80mm.
* ToC lines rendering is sometimes ugly if the title label is too long: dotted leader or even page number may appear on the following line, sometimes colliding between themselves.
* Inline elements like em cause bad paragraphs justification if they are rendered at the beginning of a new line.
* FS doesn't support the CSS widows/orphans properties, which makes their handling harder.


Known bugs
----------

* Sometimes, they are still unwanted page breaks before a heading + list (e.g. h4 then ol).
* A schema validation is executed by jing during the hdoc_to_pdf conversion. Normally, if the validation fails, the process should abort because we are not treating a valid hdoc file. However, at the moment, the script only warns the user of the error and goes on, because the schemas and the opale_to_hdoc converter are not synchronized at the moment (it needs to be corrected).

Generic Todo
------------

* Rework the hdoc_to_pdf.ant and find_content.xsl scripts to allow multifiles handling.
* Handle as fully as possible of widows and girl orphans; trying to match Prince's layout and implementing the suitable CSS rules (which shall not be interpreted by FS).
* Allow the user to override some specific CSS rules, according to the main layout logical rules.
* Provide the user with a full set of options/parameters to customise the output: bound/unbound, odd/even margins, report/article LaTeX format (first page formating), etc.
* Bonus: find out a HTML editor to manually add line breaks to a hdoc file in order to resolve widows and girl orphans problems after the PDF file's generation.

Specific Todo list
------------------

* Ajouter le paramètre de reliure ("bound") au script ant
* Intégrer les styles CSS selon le paramètre "bound" dans un xsl
* Ajouter le paramètre de recto-verso au script ant
* Intégrer les styles CSS selon le paramètre recto-verso dans un xsl
* Ajout le support des marges pour documents oneside reliés
* Ajout le support des marges pour documents twoside reliés
* Identifier les règles CSS principales de traitement des tableaux
* Gérer les espacements veuves/o. pour les paragraphes
* Gérer les espacements veuves/o. pour les listes
* Gérer les espacements veuves/o. pour les tableaux
* Gérer les espacements veuves/o. pour les images
* Support des objets : ajouter une consigne dans le README de convertir tout objet graphique (odg, etc.) en image avant l'exécution
* Support des objets : ajouter des règles xsl de transformation des <object> en <img>
* Permettre à l'utilisateur de surcharger les règles CSS selon les règles logiques de la mise en page par défaut



Technical notes
---------------

* This converter works with _only one_ hdoc file in the input folder at the moment, please ensure to clean the folder before proceeding with the hdoc you want to convert to PDF. When the multifiles ability is set within the hdoc_to_pdf converter, the opale_to_pdf one shall naturally work because it already implements the opale_to_hdoc multifiles handling (the copy of all the hdoc results into the input directory of the hdoc_to_pdf converter).
* The java classes we use for the project are located in the "lib/MyPDFGenerator Sources" folder, please modify these if needed before compiling and adding the new jar file to the lib folder. In Eclipse, when the class is modified and ready to be exported, please choose the "Runnable jar file" export option.

User Story
----------

* Cas d'un fichier hdoc à convertir :
  * L'utilisateur dispose d'un fichier hdoc en entrée, il veut obtenir un fichier pdf paginé en sortie.
  * Il accède au convertisseur (dossier dédié) hdoc_to_pdf.
  * Il place le fichier hdoc dans le dossier input.
  * Il lance le script run.bat/run.sh ou exécute directement le script ant hdoc_to_pdf.ant.
  * Il récupère le fichier pdf dans le dossier output.


Capitalisation
--------------

* A16 : during this semester, we have built a hdoc_to_pdf converter from scratch, which aims to be integrated in the global hdoc project. We use the java library Flying Saucer (FS) for the purpose, but this tool has some limitations, the ones we have already noticed are listed above.
At the moment, the converter is functional and deals with main PDF layout properties: title and authors, pages numbering, headings ranks, ToC generationk, basic inline formating (+ fonts) and nested lists for instance. Some elements still need to be worked on, especially the widows/orphans behaviours for the lists. Other elements need to be handled, like the tabulars or specific objects (e.g. odg resources).
The main objective has been to keep whenever it is possible the right formating and typographic rules (often in comparison to the LateX ones), and thus deliver a readable printed document at the end.
