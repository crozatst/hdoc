# Hdoc to PDF font selection

The font used for PDF generation through FS is the **Computer Modern** font, the default LaTeX's one. Here are the main families to use :
- `cmunrm.ttf` for default text ;
- `cmunrd.ttf` for **bold** text ;
- `cmunti.ttf` for *italic/emphasis* text ;
- `cmuntt.ttf` for `code syntax`.
