# Opale to Mongo (`opale_to_mongo`)

## License
License GPL3.0
http://www.gnu.org/licenses/gpl-3.0.txt

## Credits
Alexandre Thouvenin
Kapilraj Thangeswaran

## Presentation
This module is able to extract data from a file in Opale format and insert them into MongoDB.

## Dependencies
In order to work properly this module needs :
- [`opale_to_hdoc`](https://gitlab.utc.fr/crozatst/hdoc/tree/master/opale_to_hdoc) (Opale to Hdoc conversion)
- [`hdoc_to_mongo`](https://gitlab.utc.fr/crozatst/hdoc/tree/master/hdoc_to_mongo) (Hdoc to Mongo conversion)
- to download and install Node.js
    - For windows :
        - from the [Node.js download page](https://nodejs.org/en/)
    - For linux (instruction for debian 8, may vary depending on your distrbution) execute the followings commands :
        - `su`
        - `apt install nodejs`
        - `apt install node`
        - `apt install npm`
	    - `npm link mongodb`
		
- to download and install MongoDB 
    - For Windows :
        - from the [MongoDB download page](https://www.mongodb.com/download-center#community)
    - For linux : 
	    - `# apt install mongodb`
	    - `# echo 'jsonp=true' >> /etc/mongodb.conf`
	    - `# echo 'rest=true' >> /etc/mongodb.conf`
	    - on a virtual machine or a small system ONLY : `# echo 'smallfiles=true' >> /etc/mongodb.conf`
	    - `# service mongodb restart`

## Instructions
1. Install dependencies
2. Add all your hdoc documents in an "input" folder
3. Add or edit "config.xml" file in "input" folder (for more details, please check "Input configuration")
4. Edit "config.json" file from "mongo" folder in "hdoc_to_mongo" module (for more details, please check "MongoDB configuration")
5. Make sure that MongoDB is running (`mongod.exe --rest --jsonp` command from "MongoDB/Server/3.2/bin" folder)
6. Execute run.bat or run.sh

## Input configuration
You can add or edit "config.xml" in "input" folder to provide more information about your documents.

Supported information
- link

### Schema
```
<?xml version="1.0" encoding="UTF-8"?>
<grammar ns="" xmlns="http://relaxng.org/ns/structure/1.0" datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes">
  <start>
    <element name="config">
      <element name="file">
        <attribute name="name"/>
        <element name="link">
          <data type="anyURI"/>
        </element>
      </element>
    </element>
  </start>
</grammar>
```

### Example
```
<?xml version="1.0" encoding="UTF-8"?>
<config>
	<file name='sample.scar'>
		<link>https://stph.scenari-community.org/nf17/co/nf17.html</link>
	</file>
</config>
```

## MongoDB configuration
### In "mongo" folder from "hdoc_to_mongo" module
Use "config.json" in the "mongo" folder to specify
- url : mongodb's url
- database : the database you are using
- collection : the collection you are using
- request : the request you want to perform (for allowed requests, please check "Supported requests")

#### Example
```
{
	"url" : "mongodb://localhost:27017/",
	"database" : "database",
	"collection" : "collection",
	"request" : "insert"
}
```

## Supported requests
- insert
- update (using the title as filter)
- remove (using the title as filter)

## User stories
- En tant qu’utilisateur rédigeant des documents sous opale, je veux récupérer les exercices corrigés d’un sujet donné dans le but de les réutiliser.
- En tant qu’utilisateur rédigeant des documents sous opale, je veux récupérer les cours d’un auteur précis dans le but d’utiliser ses cours comme références.
- En tant qu’utilisateur universitaire, je souhaite mettre à disposition mes exercices sur internet de manière structuré dans le but de permettre à d’autre utilisateurs universitaires de les trouver, par thème, auteur ou contenu et de les réutiliser.