@echo off
set lib=lib
set ant=opale_to_mongo.ant
set antparam=-Dprogram.param=%1

set scJarList=%lib%\*

npm link mongodb & java.exe -classpath "%scJarList%" -Xmx150m org.apache.tools.ant.Main -buildfile %ant% %antparam% & rmdir /Q /S node_modules & pause

REM start /MIN java.exe -classpath "%scJarList%" -Xmx150m org.apache.tools.ant.Main -buildfile %ant% %antparam%
