# Converter hdoc_to_lexique

License GPL3.0
http://www.gnu.org/licenses/gpl-3.0.txt

## Crédits
Antoine Aufrechter, Estelle de Magondeaux, Marouane Hammi 

## Install
In order to use this converter, follow those steps :
1. Copy your hdoc file (.scar or .zip) into the input directory. Do not put more than one file into the input directory.
2. Execute the run that correspond to your OS.
3. You will find the result into the output directory.

## User documentation
For each definition in the content.xml a .term is generated and for each source that is referenced in a definition the image associated (with its meta and props files) is copied into the output file with the definition.

## Known bugs
>- Cannot put multiple input files

## TODO
>- Change the name of the file (.term) to have a name without special character
>- Manage multiple input files (e.g. see lexique_to_opale) 
>- list of element to convert :
>>- src url
>>- image
>>- pronounciation
>>- type (female, male,...) and (abbreviation, expanded,...)
>>- array
>>- multimedia
>-Do not generate the content.term or use it to generate an index instead (currently content.term is generated but useless)

## Technical notes
>- The converter have to respect the same schema as the lexique_to_hdoc converter for the xslt

