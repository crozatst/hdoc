<!--find_ressources.xsl creates a ANT file get_ressources-->
<!--the ANT copies all audio files, images and objects used in the content to convert-->
<xsl:stylesheet
	version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:h="http://www.utc.fr/ics/hdoc/xhtml"
	xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xsl:output method="xml" indent="yes"/>
	
    <xsl:template match="h:html">
    	<project name="hdoc_to_lexique" default="main">
			<target name="main">
				<xsl:apply-templates select="h:body"/>
			</target>
		</project>	
	</xsl:template>
	
	<xsl:template match="h:body">
		<xsl:apply-templates select="h:section"/>
	</xsl:template>
	
	<xsl:template match="h:section">
		<xsl:apply-templates select="h:div"/>
		<xsl:apply-templates select="h:section"/>
		<xsl:apply-templates select="h:p"/>
	</xsl:template>
		
	<xsl:template match="h:div">
		<xsl:apply-templates select="h:section"/>
		<xsl:apply-templates select="h:p"/>
		<xsl:apply-templates select="h:img"/>
		<xsl:apply-templates select="h:object"/>
		<xsl:apply-templates select="h:audio"/>
	</xsl:template>
	
	<xsl:template match="h:p">
		<xsl:apply-templates select="h:p"/>
		<xsl:apply-templates select="h:img"/>
		<xsl:apply-templates select="h:object"/>
		<xsl:apply-templates select="h:audio"/>
	</xsl:template>
	
	<xsl:template match="h:img">	
		<xsl:variable name="src" select="@src"/>
		<copy file="${{srcdir}}/${{nameInputsId}}/{$src}" tofile="${{outdir}}/{$src}"/>
		<copy file="${{srcdir}}/${{nameInputsId}}/{$src}/../meta.xml" tofile="${{outdir}}/{$src}/../meta.xml"/>
		<copy file="${{srcdir}}/${{nameInputsId}}/{$src}/../props.xml" tofile="${{outdir}}/{$src}/../props.xml"/>
	</xsl:template>
	
	<xsl:template match="h:object">	
		<xsl:variable name="data" select="@data"/>
		<copy file="${{srcdir}}/${{nameInputsId}}/{$data}" tofile="${{outdir}}/{$data}"/>
	</xsl:template>
	
	<xsl:template match="h:audio">	
		<xsl:variable name="src" select="@src"/>
		<copy file="${{srcdir}}/${{nameInputsId}}/{$src}" tofile="${{outdir}}/{$src}"/>
	</xsl:template>
</xsl:stylesheet>



		
				
	
		

	