<?xml version="1.0" encoding="UTF-8"?>
<!--find_content.xsl creates a ANT file get_content-->
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
    xmlns:h="urn:utc.fr:ics:hdoc:container"
    xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <!--<xsl:param name="destfile" required="yes" as="xs:string"/>-->
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="h:container">
        <project name="hdoc_to_lexique" default="main">
            
            <property file="build.properties"/>
            
            <target name="main">
                <!--<copy file="output/.wspmeta" todir="result"/>-->
                <xsl:apply-templates/>
            </target>
        </project>
    </xsl:template>
    
    <xsl:template match="h:rootfiles">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="h:rootfile">
        <xsl:variable name="path" select="@full-path"/>
        
        <!--the ANT runs the XSLT get_ressources then the ANT created by it-->
        <xslt in="${{srcdir}}/${{nameInputsId}}/{$path}" out="get_ressources.ant" style="${{xsldir}}/find_ressources.xsl"/>
        <chmod file="get_ressources.ant" perm="777"/>
        <ant antfile="get_ressources.ant"/>
        
        <!--the ANT runs the main XSLT transfo-->
        <!--TODO change this in order to perform transfo and get multiple out file as .term each one for a specific definition-->
        <xslt in="${{srcdir}}/${{nameInputsId}}/{$path}" out="${{outdir}}/content.term" style="${{xsldir}}/transfo.xsl"/>
    </xsl:template>
</xsl:stylesheet>
