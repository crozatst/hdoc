﻿Converter opale_to_pdf
-----------------------

* WARNING : This module is not functionnal yet !

The purpose of this converter is to obtain a PDF file from an Opale document.

The Hdoc pivot format is used to convert one format to another, losing the least amount of information. It is based on HTML.


License GPL3.0
--------------

http://www.gnu.org/licenses/gpl-3.0.txt


Credits
-------

* Thibault DRAIN
* Christophe VIROT
* Pierre Lemaire
* 2016 :
  * Baptiste PERRAUD
  * Raphaël DEBRAY


Dependance
----------

This converter is based on two converters :
* Opale to Hdoc
* Hdoc to Pdf


User documentation
------------------

The folder samples contains a scar file which may be used for some tests.
Follow the steps above to get a pdf file from a scar one:
1. Download hdoc_converter.zip and unzip it.
2. Add your source file into the input folder. It must be a .scar file.
3. Place _only one file_ in that folder.
4. On Linux or Mac, run the script run.sh. On Windows, run the script run.bat.
5. Your file has been converted, the result is in the output folder.  

**Warning: on Windows systems, the script may fail due to the lack of writing rights for some folders, try to re-execute it once are twice and it should work (until this bug is solved).**

Unsupported
-----------

Refer to the unsupported elements in Opale to Hdoc and in Hdoc to Pdf.


Known bugs
----------

- The windows execution of the run.bat script sometimes fails (problems when we try to remove some folders).

Refer to the known bugs in Opale to Hdoc and in Hdoc to Pdf.


Todo
----


Technical notes
---------------

* This converter works with _only one_ hdoc file in the input folder at the moment, please ensure to clean the folder before proceeding with the hdoc you want to convert to PDF. When the multifiles ability is set within the hdoc_to_pdf converter, the opale_to_pdf one shall naturally work because it already implements the opale_to_hdoc multifiles handling (the copy of all the hdoc results into the input directory of the hdoc_to_pdf converter).

The converter contains 1 file:

* opale_to_pdf.ant

    It checks whether the user specified or not the parameters and performs the following :
    * Copy the input files in the opale_to_hdoc directory.
    * Perform the opale_to_hdoc transformation.
    * Copy the results in the hdoc_to_pdf directory.
    * Perform the hdoc_to_pdf transformation.
    * Copy the results in the opale_to_pdf directory.


Capitalisation
--------------

N/A
