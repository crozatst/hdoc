(: This script will return a section by searching keyword in its title :)
(: We can assign a Regular Expression to the variable $keyword :)
(: For example, $keyword := '^Objectifs$', to search for sections whose titles are exactly "Objectifs":)
(: For example, $keyword := 'Syntax', to search for sections whose titles containing "Syntax" :)
(: Remark : case and accents in the keyword have been taken care of  :)

module namespace myNs = "https://gitlab.utc.fr/crozatst/hdoc/tree/master/hdoc_to_basex";

declare function myNs:searchSectionByTitle($keyword as xs:string, $docs as node()*) as node()*
{
  let $keyword_noAcc := translate($keyword, 'áàâäéèêëíìîïóòôöúùûüABCDEFGHIJKLMNOPQRSTUVWXYZ','aaaaeeeeiiiioooouuuuabcdefghijklmnopqrstuvwxyz')
  for $doc in $docs
  return
    for $section in $doc/sections/section
    let $title_noAcc := translate($section/titlesection, 'áàâäéèêëíìîïóòôöúùûüABCDEFGHIJKLMNOPQRSTUVWXYZ','aaaaeeeeiiiioooouuuuabcdefghijklmnopqrstuvwxyz')
    where matches($title_noAcc, $keyword_noAcc)
    return $section
};