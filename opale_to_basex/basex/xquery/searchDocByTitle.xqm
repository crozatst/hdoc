(: This script will return documents by searching one keyword in their title :)
(: We can assign a Regular Expression to the variable $name :)
(: For example, $name := '^NF29_HdocEtherpad$', to search for an exact name:)
(: For example, $name := 'NF29', to search for documents whose name contains 'NF29':)

module namespace myNs = "https://gitlab.utc.fr/crozatst/hdoc/tree/master/hdoc_to_basex";

declare function myNs:searchDocByTitle($name as xs:string, $docs as node()*) as node()*
{
  for $doc in $docs
  where matches($doc/title, $name, "i")
  return $doc
};