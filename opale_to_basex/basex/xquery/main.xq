(: this script with .xq as suffix is the main module :)
(: all functions defined in .xqm files (library modules) can be called here :)

import module namespace myNs = "https://gitlab.utc.fr/crozatst/hdoc/tree/master/hdoc_to_basex" at "searchSectionByTitle.xqm", "searchDocByTitle.xqm", "searchDocByAuthor.xqm";

(: myNs:searchDocByTitle('^NF29_HdocEtherpad$', //document ) :)
(: myNs:searchSectionByTitle('contenu', //document) :)

myNs:searchDocByAuthor('Antoine', //document)/title