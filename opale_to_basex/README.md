﻿Converter opale_to_basex
-----------------------

The purpose of this converter is to obtain an HDOC file from an Opale document. 


License GPL3.0
--------------

http://www.gnu.org/licenses/gpl-3.0.txt


Credits
-------

* Simei YIN
* Baptiste MONTANGE


Dependance
----------

In order to work properly, this module needs

1. [`opale_to_hdoc`](https://gitlab.utc.fr/crozatst/hdoc/tree/master/opale_to_hdoc) (Opale to Hdoc conversion)
2. [`hdoc_to_basex`](https://gitlab.utc.fr/crozatst/hdoc/tree/master/hdoc_to_basex) (Hdoc to Basex conversion)


User Stories
----------

Please consult the section "User Stories" of README.md file in [`hdoc_to_basex`] (https://gitlab.utc.fr/crozatst/hdoc/tree/master/hdoc_to_basex/README.md)

Step by step :

	[`Step 1 : File transformation`]
		- Put the files .scar you want to deal with in the folder [`input`](https://gitlab.utc.fr/crozatst/hdoc/tree/master/opale_to_basex/input)
		- Run the transformation progam (Win : double click run.bat, Linux : execute run.sh)
		- This transformation includes :
			* Title, authors, keywords
			* First Level section : title, type
	
	[`Step 2 : Create data base in basex`]
		- Download and install [`BaseX`](http://basex.org/products/download/all-downloads/)
		- Run BasexGui
		- In the Text Editor of BaseX, open the command script "createbd.bxs" in folder [`command`](https://gitlab.utc.fr/crozatst/hdoc/tree/master/opale_to_basex/basex/command).
		  Follow the instructions in the script, and then execute it.
	
	[`Step 3 : Make XQuery request`]
		- In the Text Editor of BaseX, you can open and execute xquery script "main.xq" in folder [`xquery`](https://gitlab.utc.fr/crozatst/hdoc/tree/master/opale_to_basex/basex/xquery).
		  It's in this main module that we call predefined functions and execute script
		  
		- .xqm files are library modules where we have defined functions, we can go to these files for more detailed using instructions
		  Available library modules :
			* searchDocByAuthor.xqm
			* searchDocByTitle.xqm
			* searchSectionByTitle.xqm
		
		P.S. the symbols "(: :)" are used for adding comments in xQuery files