<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xpath-default-namespace="http://www.utc.fr/ics/hdoc/xhtml"
    xmlns:ns="http://www.utc.fr/ics/hdoc/xhtml"
    > 
    
    <xsl:output method="xml" indent="yes"/>
    
    <!-- Identity transformation -->
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>        
    </xsl:template>    
    
    <!-- section -->    
    
    <xsl:template match="ns:section" priority="1">
        <xsl:choose>
            <xsl:when test="ns:section">
                <xsl:element name="section">
                    <xsl:apply-templates select="node()|@*"/>
                </xsl:element>
            </xsl:when>
            
            <xsl:otherwise>
                <xsl:element name="section">
                    <xsl:attribute name="data-hdoc-type">opale-expUc</xsl:attribute>
                    <xsl:apply-templates select="node()|@*"/>
                </xsl:element>
            </xsl:otherwise>            
        </xsl:choose>
    </xsl:template> 
    
    
</xsl:stylesheet>
