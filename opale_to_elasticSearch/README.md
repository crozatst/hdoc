﻿# Opale to ElasticSearch

## License
-------------
License GPL3.0 http://www.gnu.org/licenses/gpl-3.0.txt

## Credits
-------------
DELAUNAY Grégory
KELLER Vincent

## Presentation
-------------
Opale to ElasticSearch module extract data from an Opale file to use it with ElasticSearch


## Dependence
-------------

-   Opale To Hdoc Converter
-   Hdoc to ElasticSearch Converter

## User Story
-------------
"Vous disposez d'un ensemble de contenus Opale à votre disposition et vous aimeriez pouvoir l'analyser selon les différents types d'éléments possibles : cours, exercices, notions tout en permettant de trier par rapport aux différents sujets de ces dit contenus.
Pour se faire, vous mettrez les contenus que vous voulez analyser en input de opale_to_elasticSearch et vous lancerez le script lançant le fichier ANT.
Vous pourrez ensuite accéder à des graphiques Kibana à une adresse donnée et paramétrer vos graphiques afin d'obtenir les informations qui vous intéresse.

A savoir que les contenus opale de plusieurs machines/utilisateurs peuvent être capitalisés car l'ensemble des instances de base elasticSearch installées forment un cluster accessible via l'interface Kibana"

##Utilisation
-------------
L'utilisation complète d'opale_to_elasticSearch nécessite l'utilisation de la stack ELK (ElasticSearch, Logstash, Kibana).
Notez qu'il vous faut java8 pour lancer la stack ELK: 
- lancez java --version
- Si ce n'est pas le cas : sudo apt-get install default-jdk

Installation : 
- Télécharger ElasticSearch :  https://www.elastic.co/fr/downloads/elasticsearch et extraire l'archive
- Télécharger Logstash : https://www.elastic.co/fr/downloads/logstash et extraire l'archive
- Télécharger Kibana : https://www.elastic.co/fr/downloads/kibana et extraire l'archive
- Mettre le fichier esconf.conf situé dans opale_to_elasticSearch/conf/logstash/ dans %{dossier_installation_logstash}/
- Editer le fichier esconf.conf : ligne 11, remplacez "path => ["/elasticSearch/logstash/input/*.json"]" par "path => ["%{votreCheminAbsolu}/opale_to_elasticSearch/logstash/input/*.json"]"
- Sauvegarder les modifications.

Etapes : 
- aller dans votre dossier d'installation d'elasticsearch et lancer bin/elasticsearch
- aller dans votre dossier d'installation de kibana et lancer bin/kibana
- aller dans votre dossier d'installation de logstash et lancer bin/logstash -f esconf.conf
- Attendre les messages de logstash qui indiquent le lancement sans problème.
- lancer la transformation opale_to_elasticsearch en mettant d'abord les .scar dans opale_to_elasticsearch/input
- Le dossier de sortie n'est pas le classique opale_to_elasticsearch/output mais opale_to_elasticsearch/logstash/input afin de faire directement le lien avec Logstash
- Normalement les log de logstash indique l'insertion des sorties de la transformation, il arrive pour le moment qu'il ne le fasse qu'au moment où logstash s'arrête, l'arrêter alors.
- aller sur localhost:5601 
- Vous allez arriver sur la page de création d'index. Par défaut vous n'en avez pas et il vous propose de créer logstash-*. Créez-le en appuyant sur "Créer".
- Allez dans Management => Saved object.
- Cliquez sur "import" et importer le fichier JSON opale_to_elasticSearch/conf/kibana/dashboard.json
- Vous devriez voir un dashboard et des objets dans "vizualisation" s'ajouter à votre liste de saved objects
- Allez dans Dashboard, cliquez sur "Open" tout en haut à droite et sélectionnez le dashboard ajouté (Normalement "NF29_DATA_DASHBOARD")
- Vous pouvez cliquez sur les différents éléments des graphiques pour filtrer les informations, les graphiques s'actualiseront en fonction.

Pour vérifier que vos données sont bien insérées dans kibana, vous pouvez aller sur l'onglet "Dev tools" et lancer la requête suivante :
``` 
GET logstash-*/_search/
{
   "query": {
       "match_all": {}
   }
}
```
##Bugs connus
-------------
- Logstash ne prend pas toujours en compte tout de suite le fichier une fois ajouté au dossier, il faut parfois l'éteindre/le relancer
- Kibana ne reconnait pas les champs JSON si une/des tabulations se retrouvent devant le nom du champ.

##Idée d'amélioration
-------------
- Maintenant que hdoc_to_elasticSearch permet la gestion des exercices / questions, ajouter des graphiques utiles à l'analyse de ceux-ci
- Former un dossier de requête REST pré-formulées pour pouvoir récupérer facilement les informations qui nous intéressent après filtrage des données grâce aux graphiques Kibana
