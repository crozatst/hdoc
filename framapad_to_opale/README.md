﻿# framapad2opale -- HDOC CONVERTER PROJECT
## License
[GPL 3.0](http://www.gnu.org/licenses/gpl-3.0.txt)
## Credits
- Boucaud Fabien
- Chognard Etienne
- Rit Gabrielle
- Vintache Jean
- Douteau Jean-Come
- Fecherolle Cecile (2014)

## Presentation
How to transform a framapad document in opale document.

Filepaths in this document are relative to this readme file.

## Dependence
- framapad2Hdoc
- Hdoc2Opale

## User Documentation
1. Create a framapad document then export it in html format (Import/Export Button) and put it in the `/input` directory (if the directory does not exists, you have to create it).
2. Execute the file `/run.bat` or `/run.sh` depending on the OS. A `.scar` file is created in the directory `/output`
*If the `/input` directory contains multiple files, they will be all treated.
3. Open the document with Opale
	1. Open Scenari, and choose "UTC-etu_opale" as distant depot.
	2. Go in the sandbox.
	3. Import your `.scar` file in the directory.
	4. Open the file Main.xml created.


## Technical notes
### Description of framapad_to_hdoc.ant

#### Prelude
- Importation of necessary classes (antlib, htmlcleaner, jing)
- Creation of directories architecture tree

#### Transformations
- Use of htmlcleaner to transform the input file from html to xhtml. For more info, see http://htmlcleaner.sourceforge.net/index.php.
- Apply html2xhtml.xsl : this xsl extracts the content into <body> tags
- Apply html2xhtmlv1.xsl : this xsl is used as a fix and adds br tag at the end of lists (ul and ol)
- Apply html2xhtmlv2.xsl : this xsl surround text line with p tags and transforms non-hdoc tags into hdoc tags as s, u, strong tags.
- Apply html2xhtml3.xsl : this xsl is used as a fix, it deletes p tags when its child is ul or ol
- Apply html2hdocstruct1 to 6 : those xsl files are dedicated to building the hdoc structure based on the titles h1 to h6
- Apply html2hdocstructdivsection: this xsl completes the sections created in the previous xsl with
around the actual content of each level
- Apply xhtml2hdoc.xsl : this xsl transforms the content into hdoc structure

#### Post-transformations actions
- Build hdoc structure
- Jing checks if the output file is validated with the right rng schema
- Zip the directory into hdoc archive

###TO DO
- Régler le problème qui fait que pour certains fichiers html2xhtml.xsl ne copie pas correctement l'ensemble du document
- Implémenter la gestion du code
- Gérer parfaitement les indentations
- Possibilité d'utiliser des tags pour typer les sections

### Supported tags
- html tags -> hdoc tags
- u, s, em, strong, color -> em
- sub -> sub
- sup -> sup
- li -> li
- ol -> ol
- br -> p

###Titles
- If it's h1 -> new section
	- If following title is not h1 -> new section in the section
		- If following title is at the same level -> new section at the same level
		- If following title is at a lower level -> new section in the previous section
- If there are any misplaced titles, they will become a new section at their current level of section if there is any. For example, if the first title of the doc is a h2 it will produce a section at the highest level as if it was a h1.

## Capitalisation
Using regular expression with xsl is a good way to parse a non xml file.

We can note about the creation of the hdoc's structure with the titles h1 to h6 that XSLT is probably not the best tool for these specific transformations. Something like SAX, which actually explore the XML files sequentially, would probably be more efficient and easier to use for these kinds of transformations.