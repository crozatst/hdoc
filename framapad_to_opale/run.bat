@echo off
set lib=lib
set ant=framapad_to_opale.ant
set antparam=-Dprogram.param=%1

set scJarList=%lib%\*

java.exe -classpath "%scJarList%" -Xmx150m org.apache.tools.ant.Main -buildfile %ant% %antparam%
pause

REM start /MIN java.exe -classpath "%scJarList%" -Xmx150m org.apache.tools.ant.Main -buildfile %ant% %antparam%
