# Converter lexique_to_opale

Converter lexique_to_opale

License GPL3.0
http://www.gnu.org/licenses/gpl-3.0.txt

Crédits
Florian Arend
Estelle de Magondeaux
Marouane Hammi
Antoine Aufrechter

Install
In order to use this converter, follow those steps :
1. Copy your Lexique file(s) (.scar or .zip) into the input directory.
2. Execute the run that correspond to your OS.
3. You will find the result into the output directory. The name of the output files depends on the hour it was processed.

Dependance

User documentation

Unsupported
Type of term
    Equivalent term
    Pronunciation
    In the definition(s)
        Text content
            Links to other terms
            Spreadsheets
            Images (informative and decorative)
            Animations
            Inline images


Known bugs

TODO
Work to handle the above unsupported items. 

capitalisation
