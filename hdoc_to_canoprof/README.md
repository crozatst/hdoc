﻿Hdoc to Canoprof
===

License
-------
This project is under [GPL 3.0 license](http://www.gnu.org/licenses/gpl-3.0.txt).

Credits
-------
### Autumn 2016

* Villain Benoit
* Luszcz Charlotte

Presentation
---
"Hdoc to Canoprof" is an hdoc converter to Canoprof files. It's a set of ANT scripts and XSL files.

Dependencies
---
There's no particular dependencies needed to run the converter.

Conditions particulières
---
Pour le moment, il ne faut pas utiliser les "sous-Division" de Opale. Les divisions à la racine sont autorisées.

User Story
---
### Running the script

* Put the `.hdoc` files in the input folder
* Run `run.bat` or `run.sh` according to your operating system
* The output files are in the output folder


DONE
---
Opale											Hdoc								Canoprof
-----------------------------------------------------------------------------------------------------------------------------------------------------------------
Module											fichier hdoc							Programme
Objectif / Introduction / Conclusion du module						Section contenant une div avec un header et du texte		Séance contenant une activité de type description courte
Division ou activité									Section								Séance
Grain opale										Section contenant une section					Activité Texte et multimédia
Parties et sous-partie d’un grain							section/section/section...					Activité Texte et multimédia

Définition										div avec un attribut data-hdoc-type = definiton			Définition
Exemple											div avec un attribut data-hdoc-type = example			Exemple
Remarque										div avec un attribut data-hdoc-type = remark			Hypothèse (information?)
Conseil											div avec un attribut data-hdoc-type = advice			Conseil
Attention										div avec un attribut data-hdoc-type = warning			Attention
Complément										div avec un attribut data-hdoc-type = complement		Complément
Fondamental										div avec un attribut data-hdoc-type = emphasis			Méthode
Information										div avec un attribut data-hdoc-type = information		Information
Méthode											div avec un attribut data-hdoc-type = method			Méthode
Syntaxe											div avec un attribut data-hdoc-type = complement		Complément
Texte légal										div avec un attribut data-hdoc-type = complement		Complément



Remarque : le texte, les listes, les tableaux fonctionnent.

TODO
---
Points particuliers de Canoprof non traités:
Questions avec corrigé
Consigne élève
Remarque prof

Blocs : 
Rappel		Pas traité	(à faire dans Opale_to_Hdoc et Hdoc_to_canoprof)
Simulation	Pas traité	(à faire dans Opale_to_Hdoc et Hdoc_to_canoprof)

Permettre l'utilisation de vidéos de Opale, notamment les vidéos venues du web dans des modules web distants (à faire dans Opale_to_Hdoc et Hdoc_to_canoprof)