<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:h="http://www.utc.fr/ics/hdoc/xhtml"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
    xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:cp="canope.fr:canoprof"
    xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
    xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs">
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    
    <xsl:param name="filename"/>
    
    <xsl:strip-space elements="*"/>
    
    <!-- This template matches the root. One hdoc file = one Canoprof's Programme" -->
    <xsl:template match="h:html">
        <sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
            <cp:program xmlns:cp="canope.fr:canoprof"
                xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
                <xsl:apply-templates select="./*"/>
            </cp:program>
        </sc:item>
    </xsl:template>
    
    <!-- Head related templates. -->
    <xsl:template match="h:head">
        <cp:programM>
            <!-- Title of Programme. -->
            <sp:title>
                <xsl:value-of select="./h:title"/>
            </sp:title>
            
            <!-- Hdoc's "date" = Programme's period. -->
            <xsl:apply-templates select="./h:meta[@name='date']"/>
            
            <!-- Hdoc's "description" = Programme's resume. -->
            <xsl:apply-templates select="./h:meta[@name='description']"/>
            
        </cp:programM>
    </xsl:template>
    
    <xsl:template match="h:head/h:meta[@name='date']">
        <sp:period>
            <xsl:value-of select="./@content"/>
        </sp:period>
    </xsl:template>
    
    <xsl:template match="h:head/h:meta[@name='description']">
        <sp:abstract>
            <cp:txtDesc>
                <sc:para xml:space="preserve">
                    <xsl:value-of select="./@content"/>
                </sc:para>
            </cp:txtDesc>
        </sp:abstract>
    </xsl:template>
    
    
    <!-- Body related templates. -->
    <xsl:template match="h:body">
        <xsl:if test="./*">
            <xsl:apply-templates select="./*"/>
        </xsl:if>
    </xsl:template>
    
    <!-- Section related templates -->
    <!-- CanoProf's Seance = hdoc's  body/Section = Opale's Division -->
    <!-- CanoProf's Activite TetM = hdoc's  body/Section/Section = Opale's Grain -->
    <!-- CanoProf's Activite TetM (section) = hdoc's  body/Section/Section/(Section...) = Opale's Grain (partie) -->
    
    <!-- CanoProf's Seance = hdoc's  body/Section = Opale's Division or Activite-->
    <xsl:template match="h:body/h:section">
        <xsl:if test="./*">
            <sp:session>
                <cp:session>
                    <cp:sessionM>
                        <sp:title>
                            <xsl:if test="not(./h:header/h:h1/text())"> Untitled </xsl:if>
                            <xsl:value-of select="./h:header/h:h1"/>
                        </sp:title>
                        
                        <!-- If a SOLO section have a div; this div become a Canoprof's resume of the current seance.
                        <xsl:if test="./h:div/*">
                            <sp:abstract>
                                <cp:txtDesc>
                                    <xsl:apply-templates select="./h:div/*" mode="resume"/>
                                </cp:txtDesc>
                            </sp:abstract>
                        </xsl:if>
                        -->
                        
                    </cp:sessionM>
                    <xsl:apply-templates select="./*"/>
                </cp:session>
            </sp:session>
        </xsl:if>
    </xsl:template>
    
    <!-- Résolution du pb ajouté lors d'une modif de opale_to_cano... -->
    <xsl:template match="h:body/h:section[@data-hdoc-type = 'references']"/>
    
    
    <!-- if Section have only a dev (no sub section), we create a short activty to print text into a seance -->
    <xsl:template match="h:body/h:section/h:div">
        <sp:shortActivity>
            <cp:shortActivity>
                <cp:activityM>
                    <sp:title>
                        <xsl:value-of select="../h:header/h:h1"/>
                    </sp:title>
                </cp:activityM>
                <sp:body>
                    <cp:flow>
                        <xsl:apply-templates select="./*"/>
                    </cp:flow>
                </sp:body>
            </cp:shortActivity>
        </sp:shortActivity>
    </xsl:template>
    
    <!-- Toutes les introductions de sections sont transformés en résumé. -->
    <xsl:template match="h:header/h:div[@data-hdoc-type = 'introduction']">
        <sp:abstract>
            <cp:txtDesc>
                <sc:para xml:space="preserve">
                    <xsl:value-of select="./text()"/>
                </sc:para>
            </cp:txtDesc>
        </sp:abstract>
    </xsl:template>
    
    <!-- CanoProf's Activite TetM = hdoc's  body/Section/Section = Opale's Grain -->
    <xsl:template match="h:body/h:section/h:section">
        <xsl:if test="./*">
            <sp:textActivity>
                <cp:textActivity>
                    <cp:activityM>
                        <sp:title>
                            <xsl:if test="not(./h:header/h:h1/text())"> Untitled </xsl:if>
                            <xsl:value-of select="./h:header/h:h1"/>
                        </sp:title>
                        <xsl:if test="(./h:header/h:div[@data-hdoc-type = 'introduction']/text())">
                            <xsl:apply-templates
                                select="./h:header/h:div[@data-hdoc-type = 'introduction']"/>
                        </xsl:if>
                    </cp:activityM>
                    <xsl:if test="./h:div/*">
                        <sp:body>
                            <cp:blocks>
                                <xsl:apply-templates select="./h:div"/>
                            </cp:blocks>
                        </sp:body>
                    </xsl:if>
                    <xsl:if test="./h:section/*">
                        <xsl:apply-templates select="./h:section" mode="CanoprofSection"/>
                    </xsl:if>
                </cp:textActivity>
            </sp:textActivity>
        </xsl:if>
    </xsl:template>
    
    <!-- CanoProf's Activite TetM (section) = hdoc's  body/Section/Section/(Section...) = Opale's Grain (partie) -->
    <xsl:template match="h:section" mode="CanoprofSection">
        <sp:section>
            <cp:textActivity>
                <cp:activityM>
                    <sp:title>
                        <xsl:if test="not(./h:header/h:h1/text())"> Untitled </xsl:if>
                        <xsl:value-of select="./h:header/h:h1"/>
                    </sp:title>
                    <xsl:if test="(./h:header/h:div[@data-hdoc-type = 'introduction']/text())">
                        <xsl:apply-templates
                            select="./h:header/h:div[@data-hdoc-type = 'introduction']"/>
                    </xsl:if>
                </cp:activityM>
                <xsl:if test="./h:div/*">
                    <sp:body>
                        <cp:blocks>
                            <xsl:apply-templates select="./h:div"/>
                        </cp:blocks>
                    </sp:body>
                </xsl:if>
                <xsl:if test="./h:section/*">
                    <xsl:apply-templates select="./h:section" mode="CanoprofSection"/>
                </xsl:if>
            </cp:textActivity>
        </sp:section>
    </xsl:template>
    
    <!-- TODO : intro/conlcusion/obj d'une activité.... 
    <xsl:template match="h:section[@data-hdoc-type = 'introduction'] | h:section[@data-hdoc-type = 'conclusion']">
    </xsl:template>
-->
    
    <!-- Div related templates -->
    <!-- Formatting content based on @data-hdoc-type -->
    <xsl:template match="h:div">
        <xsl:choose>
            <!-- hdoc's explanation = canoprof's information (NOT EXPLICIT...) -->
            <xsl:when test="./@data-hdoc-type = 'explanation'">
                <sp:info>
                    <cp:block>
                        <xsl:if test="string-length(./h:h6/text()) > 0">
                            <cp:blockM>
                                <sp:title>
                                    <xsl:value-of select="./h:h6"/>
                                </sp:title>
                            </cp:blockM>
                        </xsl:if>
                        <sp:body>
                            <cp:flow>
                                <xsl:apply-templates select="./*"/>
                            </cp:flow>
                        </sp:body>
                    </cp:block>
                </sp:info>
            </xsl:when>
            <!-- hdoc's warning = canoprof's warning -->
            <xsl:when test="./@data-hdoc-type = 'warning'">
                <sp:warning>
                    <cp:block>
                        <cp:blockM>
                            <xsl:if test="string-length(./h:h6/text()) > 0">
                                <sp:title>
                                    <xsl:value-of select="./h:h6"/>
                                </sp:title>
                            </xsl:if>
                        </cp:blockM>
                        <sp:body>
                            <cp:flow>
                                <xsl:apply-templates select="./*"/>
                            </cp:flow>
                        </sp:body>
                    </cp:block>
                </sp:warning>
            </xsl:when>
            <!-- hdoc's complement = canoprof's complement (extra) -->
            <xsl:when test="./@data-hdoc-type = 'complement'">
                <sp:extra>
                    <cp:block>
                        <cp:blockM>
                            <xsl:if test="string-length(./h:h6/text()) > 0">
                                <sp:title>
                                    <xsl:value-of select="./h:h6"/>
                                </sp:title>
                            </xsl:if>
                        </cp:blockM>
                        <sp:body>
                            <cp:flow>
                                <xsl:apply-templates select="./*"/>
                            </cp:flow>
                        </sp:body>
                    </cp:block>
                </sp:extra>
            </xsl:when>
            <!-- hdoc's advice = canoprof's advice -->
            <xsl:when test="./@data-hdoc-type = 'advice'">
                <sp:advice>
                    <cp:block>
                        <cp:blockM>
                            <xsl:if test="string-length(./h:h6/text()) > 0">
                                <sp:title>
                                    <xsl:value-of select="./h:h6"/>
                                </sp:title>
                            </xsl:if>
                        </cp:blockM>
                        <sp:body>
                            <cp:flow>
                                <xsl:apply-templates select="./*"/>
                            </cp:flow>
                        </sp:body>
                    </cp:block>
                </sp:advice>
            </xsl:when>
            <!-- hdoc's definition = canoprof's definition -->
            <xsl:when test="./@data-hdoc-type = 'definition'">
                <sp:definition>
                    <cp:block>
                        <cp:blockM>
                            <xsl:if test="string-length(./h:h6/text()) > 0">
                                <sp:title>
                                    <xsl:value-of select="./h:h6"/>
                                </sp:title>
                            </xsl:if>
                        </cp:blockM>
                        <sp:body>
                            <cp:flow>
                                <xsl:apply-templates select="./*"/>
                            </cp:flow>
                        </sp:body>
                    </cp:block>
                </sp:definition>
            </xsl:when>
            <!-- hdoc's example = canoprof's example -->
            <xsl:when test="./@data-hdoc-type = 'example'">
                <sp:example>
                    <cp:block>
                        <cp:blockM>
                            <xsl:if test="string-length(./h:h6/text()) > 0">
                                <sp:title>
                                    <xsl:value-of select="./h:h6"/>
                                </sp:title>
                            </xsl:if>
                        </cp:blockM>
                        <sp:body>
                            <cp:flow>
                                <xsl:apply-templates select="./*"/>
                            </cp:flow>
                        </sp:body>
                    </cp:block>
                </sp:example>
            </xsl:when>
            <!-- hdoc's remark = canoprof's hypothesis (NOT EXPLICIT...) -->
            <xsl:when test="./@data-hdoc-type = 'remark'">
                <sp:hypothesis>
                    <cp:block>
                        <cp:blockM>
                            <xsl:if test="string-length(./h:h6/text()) > 0">
                                <sp:title>
                                    <xsl:value-of select="./h:h6"/>
                                </sp:title>
                            </xsl:if>
                        </cp:blockM>
                        <sp:body>
                            <cp:flow>
                                <xsl:apply-templates select="./*"/>
                            </cp:flow>
                        </sp:body>
                    </cp:block>
                </sp:hypothesis>
            </xsl:when>
            <!-- hdoc's emphasis = canoprof's method (NOT EXPLICIT...) -->
            <xsl:when test="./@data-hdoc-type = 'emphasis'">
                <sp:method>
                    <cp:block>
                        <cp:blockM>
                            <xsl:if test="string-length(./h:h6/text()) > 0">
                                <sp:title>
                                    <xsl:value-of select="./h:h6"/>
                                </sp:title>
                            </xsl:if>
                        </cp:blockM>
                        <sp:body>
                            <cp:flow>
                                <xsl:apply-templates select="./*"/>
                            </cp:flow>
                        </sp:body>
                    </cp:block>
                </sp:method>
            </xsl:when>
            <!-- hdoc's information = canoprof's info  -->
            <xsl:when test="./@data-hdoc-type = 'information'">
                <sp:info>
                    <cp:block>
                        <cp:blockM>
                            <xsl:if test="string-length(./h:h6/text()) > 0">
                                <sp:title>
                                    <xsl:value-of select="./h:h6"/>
                                </sp:title>
                            </xsl:if>
                        </cp:blockM>
                        <sp:body>
                            <cp:flow>
                                <xsl:apply-templates select="./*"/>
                            </cp:flow>
                        </sp:body>
                    </cp:block>
                </sp:info>
            </xsl:when>
            <!-- hdoc's method = canoprof's method  -->
            <xsl:when test="./@data-hdoc-type = 'method'">
                <sp:method>
                    <cp:block>
                        <cp:blockM>
                            <xsl:if test="string-length(./h:h6/text()) > 0">
                                <sp:title>
                                    <xsl:value-of select="./h:h6"/>
                                </sp:title>
                            </xsl:if>
                        </cp:blockM>
                        <sp:body>
                            <cp:flow>
                                <xsl:apply-templates select="./*"/>
                            </cp:flow>
                        </sp:body>
                    </cp:block>
                </sp:method>
            </xsl:when>
            <!-- hdoc's autres or no attribute = canoprof's information-->
            <xsl:otherwise>
                <sp:info>
                    <cp:block>
                        <xsl:if test="string-length(./h:h6/text()) > 0">
                            <cp:blockM>
                                <sp:title>
                                    <xsl:value-of select="./h:h6"/>
                                </sp:title>
                            </cp:blockM>
                        </xsl:if>
                        <sp:body>
                            <cp:flow>
                                <xsl:apply-templates select="./*"/>
                            </cp:flow>
                        </sp:body>
                    </cp:block>
                </sp:info>
                <!--                
                <sp:body>
                    <cp:flow>
                        <xsl:apply-templates select="./*"/>
                    </cp:flow>
                </sp:body>
-->
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- Text related templates -->
    <xsl:template match="h:p | h:ul | h:ol">
        <xsl:if test="not(preceding-sibling::h:p)">
            <xsl:if test="not(preceding-sibling::h:ul)">
                <xsl:if test="not(preceding-sibling::h:ol)">
                    <xsl:choose>
                        
                        <!-- If it is directly included in a div, we have to add Canoprof's text markups -->
                        <xsl:when test="parent::*[name() = 'div']">
                            <sp:txt>
                                <cp:txt>
                                    <xsl:call-template name="blockloop"/>
                                </cp:txt>
                            </sp:txt>
                        </xsl:when>
                        
                        <!-- Otherwise, we can directly display it (this can happen when it is included within a list or a table) -->
                        <xsl:otherwise>
                            <xsl:call-template name="blockloop"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:if>
            </xsl:if>
        </xsl:if>
    </xsl:template>
    
    <!--
    <xsl:template match="h:p | h:ul | h:ol" mode="resume">
        <xsl:if test="not(preceding-sibling::h:p)">
            <xsl:if test="not(preceding-sibling::h:ul)">
                <xsl:if test="not(preceding-sibling::h:ol)">
                    <xsl:call-template name="blockloop"/>
                </xsl:if>
            </xsl:if>
        </xsl:if>
    </xsl:template>
-->
    
    <xsl:template name="blockloop">
        <xsl:for-each select=". | ./following-sibling::*">
            <xsl:choose>
                
                <!-- Paragraph -->
                <xsl:when test="name() = 'p' and (./* | ./text())">
                    <sc:para xml:space="preserve">
                        <xsl:apply-templates select="./* | ./text()"/>
                    </sc:para>
                </xsl:when>
                
                <!-- Unordered list -->
                <xsl:when test="name() = 'ul' and (./h:li/h:p/text())">
                    <sc:itemizedList>
                        <xsl:apply-templates select="./* | ./text()"/>
                    </sc:itemizedList>
                </xsl:when>
                
                <!-- Ordered list -->
                <xsl:when test="name() = 'ol' and (./h:li/h:p/text())">
                    <sc:orderedList>
                        <xsl:apply-templates select="./* | ./text()"/>
                    </sc:orderedList>
                </xsl:when>
                
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="h:li">
        <sc:listItem>
            <xsl:apply-templates select="./* | ./text()"/>
        </sc:listItem>
    </xsl:template>
    
    <xsl:template match="h:i">
        <xsl:if test="./* | ./text()">
            <sc:inlineStyle role="specific">
                <xsl:apply-templates select="./* | ./text()"/>
            </sc:inlineStyle>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="h:em">
        <xsl:if test="./* | ./text()">
            <sc:inlineStyle role="emphasis">
                <xsl:apply-templates select="./* | ./text()"/>
            </sc:inlineStyle>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="h:q">
        <xsl:if test="./* | ./text()">
            <sc:phrase role="quote">
                <xsl:apply-templates select="./* | ./text()"/>
            </sc:phrase>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="h:sub">
        <xsl:if test="./* | ./text()">
            <sc:textLeaf role="ind">
                <xsl:apply-templates select="./* | ./text()"/>
            </sc:textLeaf>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="h:sup">
        <xsl:if test="./* | ./text()">
            <sc:textLeaf role="exp">
                <xsl:apply-templates select="./* | ./text()"/>
            </sc:textLeaf>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="h:a">
        <xsl:if test="./* | ./text()">
            <sc:phrase role="url">
                <cp:link xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
                    xmlns:cp="canope.fr:canoprof"
                    xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
                    <sp:url>
                        <xsl:value-of select="./@href"/>
                    </sp:url>
                </cp:link>
                <xsl:if test="./@title">
                    <sp:title>
                        <xsl:value-of select="./@title"/>
                    </sp:title>
                </xsl:if>
                <xsl:value-of select="."/>
            </sc:phrase>
        </xsl:if>
    </xsl:template>
    
    <!-- Table related templates -->
    <xsl:template match="h:table">
        <xsl:choose>
            <xsl:when test="parent::*[name() = 'div']">
                <!-- If this <table> is a direct child of a <div> then it must be surrounded by Opale's text markups. -->
                <sp:txt>
                    <cp:txt>
                        <sc:table>
                            <xsl:if test="./h:caption">
                                <sc:caption>
                                    <xsl:value-of select="./h:caption"/>
                                </sc:caption>
                            </xsl:if>
                            <xsl:apply-templates select="./h:tr"/>
                        </sc:table>
                    </cp:txt>
                </sp:txt>
            </xsl:when>
            <xsl:otherwise>
                <sc:table>
                    <xsl:if test="./h:caption">
                        <sc:caption>
                            <xsl:value-of select="./h:caption"/>
                        </sc:caption>
                    </xsl:if>
                    <xsl:apply-templates select="./h:tr"/>
                </sc:table>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="h:tr">
        <sc:row>
            <xsl:apply-templates select="./h:td"/>
        </sc:row>
    </xsl:template>
    
    <xsl:template match="h:td">
        <sc:cell>
            <xsl:apply-templates select="./*"/>
        </sc:cell>
    </xsl:template>
	
	<!-- Display images -->
      <xsl:template match="h:img">
		<sp:txt>
			<cp:txt>
				<sc:para xml:space="preserve">
					<sc:inlineImg role="ico" sc:refUri="res/{./@src}"/>
				</sc:para>
			</cp:txt>
		</sp:txt>
      </xsl:template>
	  
	 <!-- Display link for object -->
	<xsl:template match="h:object">
		<sp:txt>
			<cp:txt>
				<sc:para xml:space="preserve">
					<sc:uLink role="media" sc:refUri="res/{./@data}">
						<xsl:value-of select="./@data"/>
					</sc:uLink>
				</sc:para>
			</cp:txt>
		</sp:txt>
	</xsl:template>
    
    <xsl:template match="h:header"/>
    <!-- Its content is already used in <xsl:template match="h:section"> -->
    <xsl:template match="h:h6"/>
    <!-- Its content is already used in <xsl:template match="h:div"> -->
    <xsl:template match="h:h1"/>
    <!-- Its content is already used in <xsl:template match="h:section"> -->
    
</xsl:stylesheet>
