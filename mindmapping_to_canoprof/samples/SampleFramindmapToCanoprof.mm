<map version="1.0.1">
    <node ID="ID_1" TEXT="Framindmap To Canoprof">
        <node ID="ID_5" POSITION="right" STYLE="fork" TEXT="#2 S&#233;ance 2">
            <node ID="ID_6" POSITION="right" STYLE="fork" TEXT="#def D&#233;finition titre">
                <node ID="ID_13" POSITION="right" STYLE="fork" TEXT="#p Definition contenu"/>
            </node>
            <node ID="ID_20" POSITION="right" STYLE="fork" TEXT="#ex Exemple titre">
                <node ID="ID_27" POSITION="right" STYLE="fork" TEXT="#p Exemple contenu : En recherche du moyen de mettre tout les types de contenus similaires &#224; d&#233;finition dans le m&#234;me template"/>
            </node>
        </node>
        <node ID="ID_4" POSITION="left" STYLE="fork" TEXT="#1 S&#233;ance 1">
            <node ID="ID_29" POSITION="left" STYLE="fork" TEXT="#p Ici probl&#232;me, l'activit&#233; cr&#233;&#233;e devrait &#234;tre de type description courte"/>
        </node>
    </node>
</map>