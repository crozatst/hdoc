Mindmapping to Opale
=================

Conversion of Freemind file(s) (.mm) to Opale (.scar)

## Licence
-------------------------------
http://www.gnu.org/licenses/gpl-3.0.txt

## Credits
-------------------------------
This section has been written by 

* Thibault BROCHETON
* Bastien FREMONDIERE
* Amélie PERDRIAUD
* Quentin KEUNEBROEK
* Alexandre GUTH

It is based on the work of Guillaume GOMEZ in 2014.

## Presentation

The project aims at converting Freemind files in Opale files. If you use another tool to create mindmapping files, you can probably export it to a .mm file and use it in this converter.

## Dependence
-------------------------------
In dependence with mindmapping_to_hdoc and hdoc_to_opale

## User documentation
-------------------------------

* Use a terminal and go to the root of the folder (mindmapping_to_opale).
* Please refer to the part *Rules to follow* to understand the rules that can help you to personalize the output
* Put your files in a folder 'mindmapping_to_opale/input'
* Enter the command line corresponding to your OS :
	* On Linux : 'sh run.sh'
	* On Windows : 'run.bat'
If you want to convert one specific file that is in the input folder, use the parameters ' -DinputPath input/<yourFilename>' (ex : 'sh run.sh -DinputPath input/<yourFilename>')

*You will find the result of the conversion in the folder mindmapping_to_opale/output*
*You can find a sample file il the folder /samples.*

### Rules to follow

You can use the native formating in order to complete your Module : 

* By using the "Text Bold" option, you will get an emphasis.
* By using the "Text Italic" option, you will get a quote. 

You can use special rules in Freemind to personnalize your Scenari Module :

* By default, a "Division" is created with the name of the main node of your MindMap
* Then :
	* A node without children is transformed in **grain content**
	* A node with children is transformed in **division**
* You can add introduction and conclusion using the hashtag **#intro** and **#conclu** on all the node that has no child
* You can use **#p** to create a paragraph, and **#a** to create a link
* You can use some hashtags to create specific contents :
	* **#rmk** : a remark
	* **#adv** : an advice
	* **#emph** : an emphasis
	* **#compl** : a complement
	* **#wrng** : a warning
	* **#ex** : an example
	* **#def** : a definition
	* **#question** : a MCQ
	* **#singlechoicequestion** : a QCU
	* **#choice-qcu** : an answer within a QCU
	* **#solution** : a solution associated to a single choice within a QCU (matches an integer)
	* **#choice-correct** : a correct answer within a MCQ
	* **#choice-incorrect** : an incorrect answer within a MCQ
	* **#explanation** : an explanation of the answers within a MCQ
	* *Then you can add some **#p** inside these node to create the content*
- You can arrange the order of the node usine the hashtag **#1, #2, #3** etc.

## Unsupported

* Convert colors
* Convert icon into summary elements
* HTML node

## Known bugs

## TODO

* Develop new hashtags to create other type of Opale contents (such as QCM, self-evaluation etc.)

## Technicals notes

### Preamble
This subsection will explain precisely how the conversion process works. If you are reading this, we assume you are familiar with XML technologies seen in NF29 (i.e. ANT scripts, XSL-XSLT transformations and Hdoc format). We also recommend you to open sources files and read the comments.
Besides, the converter have been tested on recent ANT versions (> 1.7.0) only. It depends on hdoc_to_opale module.

### How does the converter works
This converter is using standard NF29 conversion project structure : a main ANT file (mindmapping_to_opale.ant), which handles routine tasks (zipping archives, copying files, order tasks), XSL-XSLT transformation scripts calls. This main ANT file is composed of several targets, and supports both Windows or Linux platforms.
This ant file is called by a script (run.sh or run.bat), with ou without parameters.

### Hdoc files
During the conversion process, the converter is using the converter mindmapping_to_hodc. The result of this conversion is also available in the folder output.

### What does the main ANT file do ?

* Delete and create folder temp, output, output/hdoc and output/scar
* Check if the OS is Windows or Mac, and call the appropriate target
* If there is no input path in parameters,  it converts all the files present in the input folder
* It calls the converter mindmapping_to_hdoc to convert .mm files in .hdoc files.
* Copy the results of the conversion in the folder /output/hdoc
* It calls the converter hdoc_to_opale to convert .hdoc files in .scar files
* Copy the results of the conversion in the folder /output/scar
* Finally, clean temporary files

## Capitalization