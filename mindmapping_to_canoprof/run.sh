#!/bin/sh
lib="lib"
ant="mindmapping_to_canoprof.ant"
antparam="-Dprogram.param=$1"
inputPath="$2"

#Recherche de java et controle que se soit une version SUN
vJavaCmd="java"
xCheckJava () {
	vInputVarName=\$"$1"
	vInputVarVal=`eval "expr \"$vInputVarName\" "`
	if [ -z "$vInputVarVal" ];then
		eval "$1=false"
		return
	fi
	vSunJavaFound=`$vInputVarVal -version 2>&1 | grep -Eo -m 1 "(HotSpot)|(OpenJDK)"`
	if [ "$vSunJavaFound" != "HotSpot" ] && [ "$vSunJavaFound" != "OpenJDK" ] ; then
		eval "$1=false"
		return
	fi
}
xCheckJava vJavaCmd
if [ "$vJavaCmd" = "false" ]; then
	vJavaCmd="$JAVA_HOME/bin/java"
	xCheckJava vJavaCmd
	if [ "$vJavaCmd" = "false" ]; then
		echo "ERREUR: JRE de SUN introuvable. Veuillez déclarer la variable d'environnement JAVA_HOME."
		exit 1
	fi
fi

#Lancer la commande
scJarList="$lib/*"

if [ -z "$inputPath" ] ; then
	echo "Appel sans paramètre"
	$vJavaCmd -classpath "$scJarList:" -Xmx150m org.apache.tools.ant.Main -buildfile $ant $antparam
else
	echo "Appel avec paramètre"
	$vJavaCmd -classpath "$scJarList:" -Xmx150m org.apache.tools.ant.Main -buildfile $ant $antparam -DinputPath $inputPath
fi