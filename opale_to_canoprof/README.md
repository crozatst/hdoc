Opale to Canoprof
===

License
-------
This project is under [GPL 3.0 license](http://www.gnu.org/licenses/gpl-3.0.txt).

Credits
-------
### Autumn 2016

* Villain Benoit
* Luszcz Charlotte

Presentation
---
"Opale to Canoprof" is an Opale converter to Canoprof files. It's a set of ANT scripts and XSL files.

Dependencies
---
Opale_to_Hdoc et Hdoc_to_canoprof.

Conditions particuli�res
---
Pour le moment, il ne faut pas utiliser les "sous-Division" de Opale. Les divisions � la racine sont autoris�es.

Getting started
---
### Running the script

* Put the Opale`.scar` file in the input folder (You can find an example of Opale .scar in the folder named "Sample")
* Run `run.bat` or `run.sh` according to your operating system
* The output file will be in the output folder


DONE
---
Opale											Hdoc								Canoprof
-----------------------------------------------------------------------------------------------------------------------------------------------------------------
Module											fichier hdoc							Programme
Objectif / Introduction / Conclusion du module						Section contenant une div avec un header et du texte		S�ance contenant une activit� de type description courte
Division ou activit�									Section								S�ance
Grain opale										Section contenant une section					Activit� Texte et multim�dia
Parties et sous-partie d�un grain							section/section/section...					Activit� Texte et multim�dia

D�finition										div avec un attribut data-hdoc-type = definiton			D�finition
Exemple											div avec un attribut data-hdoc-type = example			Exemple
Remarque										div avec un attribut data-hdoc-type = remark			Hypoth�se (information?)
Conseil											div avec un attribut data-hdoc-type = advice			Conseil
Attention										div avec un attribut data-hdoc-type = warning			Attention
Compl�ment										div avec un attribut data-hdoc-type = complement		Compl�ment
Fondamental										div avec un attribut data-hdoc-type = emphasis			M�thode
Information										div avec un attribut data-hdoc-type = information		Information
M�thode											div avec un attribut data-hdoc-type = method			M�thode
Syntaxe											div avec un attribut data-hdoc-type = complement		Compl�ment
Texte l�gal										div avec un attribut data-hdoc-type = complement		Compl�ment



Remarque : le texte, les listes, les tableaux fonctionnent.

TODO
---
Points particuliers de Canoprof non trait�s:
Questions avec corrig�
Consigne �l�ve
Remarque prof

Blocs : 
Rappel		Pas trait�	(� faire dans Opale_to_Hdoc et Hdoc_to_canoprof)
Simulation	Pas trait�	(� faire dans Opale_to_Hdoc et Hdoc_to_canoprof)

Permettre l'utilisation de vid�os de Opale, notamment les vid�os venues du web dans des modules web distants (� faire dans Opale_to_Hdoc et Hdoc_to_canoprof)
