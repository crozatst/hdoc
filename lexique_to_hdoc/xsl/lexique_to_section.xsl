<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
    xmlns:lx="scpf.org:lexicon"
    xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
    xmlns:op="utc.fr:ics/opale3"
    
    exclude-result-prefixes="xs"
    version="2.0">

    <xsl:output method="xml" indent="yes" />
    
    <xsl:template  match="dictionnaire">
		<html xmlns:hdoc="http://www.utc.fr/ics/hdoc/xhtml">
		<head>
			 <title class="$tag">
                          Dictionnaire
                        </title>
                        <meta charset="utf-8"/> 
                    
		</head>
		<body>
			<xsl:for-each select="definition">
				<br/>
				<!-- <xsl:value-of select="document(./@ref)/sc:item/lx:term/lx:termM/sp:name"/> <xsl:value-of select="@ref"/>
					<xsl:value-of select="document('~/hdoc/lexique_to_hdoc/input/achiral.term')/sc:item/lx:term/lx:termM/sp:name"/>
				-->
				<xsl:apply-templates select="document(./@ref)/sc:item"/>
			</xsl:for-each>
		</body>
		</html>
</xsl:template>

    <xsl:template match="sc:item">
       
                <section xmlns:hdoc="http://www.utc.fr/ics/hdoc/xhtml">
                    <header>
     <!--
        GESTION DES TAGS: classe contenant la définition
        -->
                   <xsl:variable name="tag"> <xsl:value-of select="document(@sc:refUri)/sc:item/lx:tag/lx:tagM/sp:title"/></xsl:variable>
                        <h1 class="$tag">
                            <xsl:value-of select="lx:term/lx:termM/sp:name"></xsl:value-of>
                        </h1>
                    
                    </header>
                       <div>
                         <!--  <h6 span="titre_def">
                               <xsl:value-of select="lx:term/lx:termM/sp:name"></xsl:value-of>                        
                           </h6> <p> </p>-->
				
				<!--grammaire et type du mot en italique entre crochets 
<xsl:if test="lx:term/lx:termM/sp:grammar">
				</xsl:if>
				<xsl:if test="lx:term/lx:termM/sp:type">
				</xsl:if>
-->
				<h6>				
                               [<xsl:apply-templates select="lx:term/lx:termM/sp:grammar"></xsl:apply-templates>]
                               [<xsl:apply-templates select="lx:term/lx:termM/sp:type"></xsl:apply-templates>]
				</h6>
                           <p span="def_content">
                               <xsl:apply-templates select="lx:term"></xsl:apply-templates>
                           </p>
                       </div>
                </section>
    </xsl:template>
    
    <!--HEAD
        RECUPERATION DU TERME : avant definition  -->
    <xsl:template match="lx:termM/sp:grammar">
        <xsl:choose>
            <xsl:when test="text() = 'female'">nf.</xsl:when>
            <xsl:when test="text() = 'male'">nm.</xsl:when>
            <xsl:when test="text() = 'adj'">adj.</xsl:when>
            <xsl:when test="text() = 'verb'">verb.</xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="lx:termM/sp:type">
        <xsl:choose>
            <xsl:when test="text() = 'abbreviation'">abbreviation</xsl:when>
            <xsl:when test="text() = 'expanded'">forme développée d'une abréviation</xsl:when>
            <xsl:when test="text() = 'usualTerm'">terme usuel</xsl:when>
            <xsl:when test="text() = 'scientific'">terme scientifique</xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="lx:term">

        <!-- Definition(s)  dans le span="def_content-->

        <xsl:choose>
            <xsl:when test="count(sp:def) > 1">
                <ol>
                    <xsl:apply-templates select="sp:def/lx:definition" mode="inList"></xsl:apply-templates>
                </ol>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="sp:def/lx:definition"></xsl:apply-templates>
            </xsl:otherwise>
        </xsl:choose>
        <!-- Tags 
        <xsl:apply-templates select="/sc:item/lx:term/sp:index/lx:index"></xsl:apply-templates>
        DEJA FAIT -->
    </xsl:template>
    
    <!--
        GESTION DES DEFINITIONS
        -->
    
    <xsl:template match="lx:definition" mode="inList">
       <li>
           <xsl:apply-templates select="sp:desc/lx:defTxt"></xsl:apply-templates>
           <xsl:apply-templates select="sp:source"></xsl:apply-templates>
           <xsl:apply-templates select="sp:moreInfo"></xsl:apply-templates>
	<xsl:apply-templates select="sp:desc/sc:extBlock"></xsl:apply-templates>
       </li>       
    </xsl:template>
    
    <xsl:template match="lx:definition">
	   <xsl:apply-templates select="sp:desc/lx:defTxt"></xsl:apply-templates>
           <xsl:apply-templates select="sp:desc/lx:defTxt/sc:table"></xsl:apply-templates>
           <xsl:apply-templates select="sp:desc/lx:defTxt/sc:extBlock"></xsl:apply-templates>
           <xsl:apply-templates select="sp:desc/lx:defTxt/lx_defTxt_blocs"></xsl:apply-templates>
           <xsl:apply-templates select="sp:source"></xsl:apply-templates>
           <xsl:apply-templates select="sp:moreInfo"></xsl:apply-templates>
	<xsl:apply-templates select="sp:desc/sc:extBlock"></xsl:apply-templates>

    </xsl:template>
    
    <!-- Information Complémentaire -->
    <xsl:template match="sp:moreInfo">
	<p span="moreinfo">
	<i> complément : </i> <br/>
        <xsl:apply-templates select="lx:moreInfoM/sp:title"></xsl:apply-templates>
        <ul>
                <xsl:apply-templates select="lx:txt/*"></xsl:apply-templates>
        </ul>
	</p>
    </xsl:template>
    
    <xsl:template match="lx:moreInfoM/sp:title">
        <p class="more_info_title">
           <xsl:value-of select="text()"></xsl:value-of>
            </p>
    </xsl:template>
    
 <!-- Copie du contenu de la balise lx:defTxt -->
	<xsl:template match="lx_defTxt_blocs/sc:para"  >
	<p class="def_text">
        <i>definition : </i>
	<xsl:value-of select="."/>
	</p>
    </xsl:template>
<xsl:template match="lx:defTxt/lx_defTxt_blocs/sc:para"  >
	<p class="def_text">
        <i>definition : </i>
	<xsl:value-of select="."/>
	</p>
    </xsl:template>

    <!-- Copie du contenu de la balise lx:defTxt et moreInfo
    <xsl:template match="lx:defTxt//*|lx:defTxt//@*|sp:moreInfo/lx:txt//*|sp:moreInfo/lx:txt//@*" >
         <li> 
	<p>
            <xsl:copy>
                <xsl:apply-templates select="node()|@*"/>
            </xsl:copy>
       </li>
      </p>
    </xsl:template>
 -->
    
    <!--
        FILTRAGE DES CONTENUS DE LA DEFINITION
        -->
    
    <!-- Tableaux -->
	
    <xsl:template match="sc:table">
	<br/>
        <table border="1">
            <caption> <xsl:value-of select="sc:caption"/></caption>
            <xsl:apply-templates select="sc:row"/>
        </table>
    </xsl:template>
    
    <xsl:template match="sc:row">
        <tr>
            <xsl:for-each select="sc:cell">
                <td>
                <xsl:value-of select="sc:para"/>
                </td>
            </xsl:for-each>
        </tr>
    </xsl:template>

	<!--images -->
	<xsl:template match="sc:extBlock[@role='img']">
		
	<xsl:element name="img">
		<xsl:attribute name="src">
			<xsl:value-of select="@sc:refUri"/>
		</xsl:attribute>
		<xsl:attribute name="alt">
			<xsl:value-of select="@sc:refUri"/>
		</xsl:attribute>
	</xsl:element>
	</xsl:template>
    <!-- 
<xsl:variable name="img_uri" select="@sc:refUri" ></xsl:variable>
<img src="$img_uri" alt="$img_uri">
	</img>
	<img src="./@sc:refUri" alt="./@sc:refUri">
	</img>
	<img src="./toronto.jpg" alt="./@sc:refUri">
	</img>
<xsl:element name="img">
<xsl:attribute name="src" value="$img_uri"/>
<xsl:attribute name="alt" value="$img_uri"/>
</xsl:element>
<xsl:element name="img">
<xsl:attribute name="src" value="toronto.jpg"/>
<xsl:attribute name="alt" value="toronto.jpg"/>
</xsl:element>Les liens vers d'autres termes,les imagettes ne sont pas reprise mais leur contenu oui 
    <xsl:template match="sc:inlineImg|sc:uLink" priority="2">
        <xsl:apply-templates select="*|text()"></xsl:apply-templates>
    </xsl:template>
    
     reprend le contenu des citations Lexique et les met entre guillemets 
  
    <xsl:template match="sc:phrase[@role='quote']" priority="2">
        &#171;&#160;<xsl:apply-templates select="*|text()"></xsl:apply-templates>&#160;&#187;
    </xsl:template>
    -->
    <!-- transforme les termes spécifiques de Lexique en termes étrangers de Opale
    <xsl:template match="sc:phrase[@role='special']" priority="2">
        <sc:inlineStyle role="spec"><xsl:apply-templates select="*|text()"></xsl:apply-templates></sc:inlineStyle>
    </xsl:template>
     -->
    <!-- remplace 'emphasis' par 'emp'
    <xsl:template match="@role[.='emphasis']" priority="2">
        <xsl:attribute name="role">emp</xsl:attribute>
    </xsl:template>
     -->
    
    <!-- Gestion des URLs 
    <xsl:template match="lx:urlM" priority="2">
        <op:urlM>
            <xsl:apply-templates select="*"></xsl:apply-templates>
        </op:urlM>
    </xsl:template>
    -->
    
    <!--
        GESTION DES SOURCES
        -->
       <!-- 
    <xsl:template match="lx:definition/sp:source[@sc:refUri]">
	<br/>
   <xsl:variable name="path" select="resolve-uri(replace(@sc:refUri,'^/',''),concat('file:/',replace($srcdir,'\\','/')))"></xsl:variable>
        <xsl:apply-templates select="document(@sc:refUri)/sc:item/lx:source/lx:sourceM"></xsl:apply-templates>
    </xsl:template>
    -->
    <xsl:template match="lx:definition/sp:source[not(@sc:refUri)]">
        <xsl:apply-templates select="lx:source/lx:sourceM"></xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="lx:source/lx:sourceM[sp:url]">

	<p span="source">
        <xsl:text>Source : </xsl:text>
        <xsl:variable name="url">  <xsl:value-of select="sp:url/text()"></xsl:value-of></xsl:variable>
        <a href="$url">
            <xsl:value-of select="sp:title"></xsl:value-of>
        </a>        
	</p>
    </xsl:template>
    
    <xsl:template match="lx:source/lx:sourceM[not(sp:url)]">
                <xsl:text>Source : </xsl:text>
                <xsl:value-of select="sp:title"></xsl:value-of>
    </xsl:template>
    
    <!--
        GESTION DES TAGS
    
    <xsl:template match="sp:index/lx:index">
        <sc:para>
            <xsl:text>Concerne : </xsl:text>
            <xsl:apply-templates select="sp:tag"></xsl:apply-templates>
        </sc:para>
    </xsl:template>
        -->
    <!--
    <xsl:template match="lx:index/sp:tag">
         <xsl:variable name="path" select="resolve-uri(replace(@sc:refUri,'^/',''),concat('file:/',replace($srcdir,'\\','/')))"></xsl:variable>
            <xsl:if test="position() != 1">
                <xsl:text>, </xsl:text>
            </xsl:if>
        <xsl:value-of select="document(@sc:refUri)/sc:item/lx:tag/lx:tagM/sp:title"></xsl:value-of>
    </xsl:template>
    -->
</xsl:stylesheet>
