README.md
# Converter lexique_to_hdoc

#License GPL.3.0
http://www.gnu.org/licenses/gpl-3.0.txt

#Crédits
Estelle de Magondeaux
Antoine Aufrechter
Marouane Hammi

#Install
In order to use this converter, follow those steps :
1. Create a dictionnary "dictionnaire.xml" which referenciate every definition you want to put in your hdoc. This dictionnary must respect the "dictionnaire.rng" schema
2. Copy the referenciated definitions in the same folder than the dictionnary (input)
3. Your input folder must contain the dictionnary (dictionnaire.xml) and the lexique definitions (**.term).
4.  Execute the run that corresponds to your OS. (run.sh for Linux, run.bat for Windows) 
5.  You will find the result into the output directory: it is named "content.xml".

#Dependances
none.

#User documentation
User documentation
You will find a "help" directory that contains: 
- A sample of dictionnary that you need to write and call "dictionnaire.xml".
- the lexique definition that you can choose to put into your hdoc (help/available_definition folder).
The generated folder is a .xml . If you desire to see the output in you browser, you need to rename it as a .html.
If you want to create more definitions with images and tables, you will need to put this ressources in the folder named "&". It contains all the ressources which are copy-pasted in the output folder so they can e seen in the hdoc.

#Unsupported
Audio and video transformation

#Known bugs
none

#TODO
create a better transformation for the ressources. At the moment the user needs to copy the ressources in a folder named "&". The best would be to match the ressources in every definitions, and to call an Ant which automatically copy the ressources needed in this folder.
Create a transformation which allows every audio resources to be seen in the hdoc.
create the Ant transformation which copies the sources and tags folders so that the source link is not empty.

#Technical notes
If folders' names are changed, you will need to change the tasks of the ANT file.


#Capitalisation
This transformation was created in order to handle hdoc to opale and hdoc to optim transformation for termeRiche. 

