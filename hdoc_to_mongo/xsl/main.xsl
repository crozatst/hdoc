<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xpath-default-namespace="http://www.utc.fr/ics/hdoc/xhtml"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:import href="subroutines/title.xsl"/>
    <xsl:import href="subroutines/author.xsl"/>
    <xsl:import href="subroutines/keyword.xsl"/>
    <xsl:import href="subroutines/right.xsl"/>
	<xsl:import href="subroutines/introduction.xsl"/>
	<xsl:import href="subroutines/exercise.xsl"/>
	<xsl:import href="subroutines/config.xsl"/>
    
    <xsl:output method="text" encoding="UTF-8" omit-xml-declaration="yes"/>
    
	<xsl:param name="fileName" required="yes" as="xs:string"/>
	<xsl:param name="inputPath" required="yes" as="xs:string"/>
	
    <xsl:template match="/">
		{
			<xsl:call-template name="config-main">
				<xsl:with-param name="fileName"><xsl:value-of select="$fileName"/></xsl:with-param>
				<xsl:with-param name="inputPath"><xsl:value-of select="$inputPath"/></xsl:with-param>
			</xsl:call-template>
			"title" : <xsl:call-template name="title-main"/>,
			"authors" : <xsl:call-template name="author-main"/>,
			"keywords" : <xsl:call-template name="keyword-main"/>,
			"rights" : <xsl:call-template name="right-main"/>,
			"introduction" : <xsl:call-template name="introduction-main"/>,
			"exercises" : <xsl:call-template name="exercise-main"/>
		}
    </xsl:template>
	
	<xsl:template match="text()"/>
</xsl:stylesheet>