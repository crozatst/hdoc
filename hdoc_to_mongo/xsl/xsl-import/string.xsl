<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:h2m="http://www.utc.fr/hdoc/hdoc_to_mongo">
    <xsl:function name="h2m:escape-string">
        <xsl:param name="value"/>
        "<xsl:value-of select="translate(replace($value, '&quot;', '\\&quot;'), '&#13;&#10;&#x9;', '   ')"/>"
    </xsl:function>
</xsl:stylesheet> 