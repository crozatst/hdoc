<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:h2m="http://www.utc.fr/hdoc/hdoc_to_mongo">
    <xsl:import href="../xsl-import/string.xsl"/>
    <xsl:function name="h2m:array">
        <xsl:param name="nodeset"/>
        [
        <xsl:for-each select="$nodeset[not(position() = last())]">
            <xsl:value-of select="h2m:escape-string(.)"/>,
        </xsl:for-each>
        <xsl:value-of select="h2m:escape-string($nodeset[last()])"/>
        ]
    </xsl:function>
</xsl:stylesheet> 