<?xml version="1.0" encoding="UTF-8"?>
<?target hdoc/content.xml ?>
<xsl:stylesheet version="2.0" xpath-default-namespace="http://www.utc.fr/ics/hdoc/xhtml"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:h2m="http://www.utc.fr/hdoc/hdoc_to_mongo">
    <xsl:import href="../xsl-import/string.xsl"/>
    <xsl:template name="title-main">
        <xsl:choose>
            <xsl:when test="name() = 'section'">
                <xsl:call-template name="title-section"></xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="title-rootDocument"></xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="title-section">
        <xsl:value-of select="h2m:escape-string(header/h1/text())"/>
    </xsl:template>
    <xsl:template name="title-rootDocument">
        <xsl:value-of select="h2m:escape-string(/html/head/title/text())"/>
    </xsl:template>
</xsl:stylesheet> 