<?xml version="1.0" encoding="UTF-8"?>
<?target hdoc/content.xml ?>
<xsl:stylesheet version="2.0" xpath-default-namespace="http://www.utc.fr/ics/hdoc/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:h2m="http://www.utc.fr/hdoc/hdoc_to_mongo">
	<xsl:import href="../xsl-import/string.xsl"/>
	<xsl:template name="exercise-main">
		[ 
			<xsl:apply-templates select="(//section[@data-hdoc-type='exercise'])[position() != last()]" mode="default"/>
			<xsl:apply-templates select="(//section[@data-hdoc-type='exercise'])[last()]" mode="last"/>
		]
	</xsl:template>
	
	<xsl:template match="//section[@data-hdoc-type='exercise']" mode="default">
		<xsl:call-template name="exercise-object"/>,
	</xsl:template>
	
	<xsl:template match="//section[@data-hdoc-type='exercise']" mode="last">
		<xsl:call-template name="exercise-object"/>
	</xsl:template>
	
	<xsl:template name="exercise-object">
		{
			"title" : <xsl:value-of select="h2m:escape-string(header/h1)"/>,
			"description" : <xsl:value-of select="h2m:escape-string(normalize-space(string-join(div[@data-hdoc-type='description']/text(), ' ')))"/>,
			"questions" : [
				<xsl:apply-templates select="div[@data-hdoc-type='question']"/>
			]
		}
	</xsl:template>
	
	<xsl:template match="div[@data-hdoc-type='question' and position() != last()]" >
		{
			<xsl:apply-templates select="div[@data-hdoc-type='description']"/>
			<xsl:apply-templates select="div[@data-hdoc-type='solution']"/>
		},
	</xsl:template>
	
	<xsl:template match="div[@data-hdoc-type='question'][last()]" >
		{
			<xsl:apply-templates select="div[@data-hdoc-type='description']"/>
			<xsl:apply-templates select="div[@data-hdoc-type='solution']"/>
		}
	</xsl:template>
	
	<xsl:template match="div[@data-hdoc-type='description']" >
			"description" : <xsl:value-of select="h2m:escape-string(normalize-space(.))"/>
	</xsl:template>
	
	<xsl:template match="div[@data-hdoc-type='solution']" >
			,"solution" : <xsl:value-of select="h2m:escape-string(normalize-space(.))"/>
	</xsl:template>
	
</xsl:stylesheet> 