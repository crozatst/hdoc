<?xml version="1.0" encoding="UTF-8"?>
<?target hdoc/content.xml ?>
<xsl:stylesheet version="2.0" xpath-default-namespace="http://www.utc.fr/ics/hdoc/xhtml"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:h2m="http://www.utc.fr/hdoc/hdoc_to_mongo">
    <xsl:import href="../xsl-import/array.xsl"/>
    <xsl:template name="keyword-main">
    {
		"_" : <xsl:value-of select="h2m:array(/html/head/meta[@name='keywords']/@content)"/>,
		"content" : <xsl:call-template name="keyword-content"/>,
        "topic" : <xsl:call-template name="keyword-topic"/>
    }
    </xsl:template>
    <xsl:template name="keyword-content">
        [
			<xsl:if test="/html/head/meta[@name='generator']/@content = 'HdocConverter/wikipedia'">
				"wiki",
			</xsl:if>
			<xsl:if test="//@data-hdoc-type = 'exercice'">
				"exercice",
			</xsl:if>
			"text" <!-- TODO: faire un switch pour tester si on index pas une image par exemple -->
        ]
    </xsl:template>
    <xsl:template name="keyword-topic">
        [
			<xsl:if test="/html/head/meta[@name='keywords']/@content[contains('XML', .)]">
				"XML"
			</xsl:if>
			<!-- TODO: faire une taxonomie des sujets, récupérer les keywords présents dedans -->
        ]
    </xsl:template>
</xsl:stylesheet> 