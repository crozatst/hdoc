<?xml version="1.0" encoding="UTF-8"?>
<?target hdoc/content.xml ?>
<xsl:stylesheet version="2.0" xpath-default-namespace="http://www.utc.fr/ics/hdoc/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:h2m="http://www.utc.fr/hdoc/hdoc_to_mongo">
	<xsl:import href="../xsl-import/string.xsl"/>
	<xsl:template name="introduction-main">
		<xsl:value-of select="h2m:escape-string(string-join(/html/body/section[@data-hdoc-type='introduction']/div/descendant::*/text(), ' '))"/>
	</xsl:template>
	
</xsl:stylesheet> 