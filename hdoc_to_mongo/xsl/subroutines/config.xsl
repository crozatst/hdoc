<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:h2m="http://www.utc.fr/hdoc/hdoc_to_mongo">
	<xsl:import href="../xsl-import/string.xsl"/>
	<xsl:template name="config-main">
		<xsl:param name="fileName"/>
		<xsl:param name="inputPath"/>
		<xsl:if test="document('../../input/config.xml')/config/file[@name = $fileName]/link">
			"link" : <xsl:value-of select="h2m:escape-string(document('../../input/config.xml')/config/file[@name = $fileName]/link)" />,
		</xsl:if>
	</xsl:template>
	
</xsl:stylesheet> 