const fs = require('fs');
const config = JSON.parse(fs.readFileSync('config.json'));
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const ObjectId = require('mongodb').ObjectID;
var url = config.url;
if(url.slice(-1) == "/") {
	url += config.database;
} else {
	url += "/" + config.database;
}
const outputFolder = "../output";

var insertDocument = function(db, collection, json) {
	db.collection(collection).insertOne(json, function(err, result) {
		assert.equal(err, null);
		console.log(json.title + " inserted into " + collection + " collection.");
	});
};

var updateDocument = function(db, collection, json) {
	db.collection(collection).updateOne({"title" : json.title}, json, function(err, result) {
		assert.equal(err, null);
		if(result.result.n > 0) {
			console.log(json.title + " updated.");
		} else {
			console.log(json.title + " not found.");
		}
	});
};

var removeDocument = function(db, collection, json) {
	db.collection(collection).remove({"title" : json.title}, function(err, result) {
		assert.equal(err, null);
		if(result.result.n > 0) {
			console.log(json.title + " removed.");
		} else {
			console.log(json.title + " not found.");
		}
	});
};

MongoClient.connect(url, function(err, db) {
	assert.equal(null, err);
	fs.readdir(outputFolder, function(err, files) {
		files.forEach(function(file) {
			var json = JSON.parse(fs.readFileSync(outputFolder + "/" + file));
			if(config.request === 'insert') {
				insertDocument(db, config.collection, json);
			} else if(config.request === 'update') {
				updateDocument(db, config.collection, json);
			} else if(config.request === 'remove') {
				removeDocument(db, config.collection, json);
			}
		});
		db.close();
	});
});