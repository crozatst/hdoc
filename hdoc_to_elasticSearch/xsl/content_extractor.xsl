<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"     
xmlns:xs="http://www.w3.org/2001/XMLSchema"    
xpath-default-namespace="http://www.utc.fr/ics/hdoc/xhtml"     
exclude-result-prefixes="xs"   
version="2.0">

<xsl:output omit-xml-declaration="yes"
encoding="UTF-8">
<!-- Encodage UTF-8 pour caractères spéciaux (accents...) -->
</xsl:output> 

<!-- On remplace les quotes dans le texte pour éviter les problèmes de JSON -->
<xsl:param name="pPattern">"</xsl:param>
<xsl:param name="pReplacement">\\"</xsl:param>

<!-- -->
<xsl:template match="/" >
{
<xsl:apply-templates select="html"/>
}
</xsl:template>

<!-- Extraction des informations du head -->
<xsl:template match="head">
"title" : "<xsl:value-of select="normalize-space(title/text())"/>",

"keywords" : [
<xsl:for-each select="meta[@name='keywords']">
"<xsl:value-of select="@content"/>"<xsl:if test="position() != last()">,</xsl:if>
</xsl:for-each>
],
<xsl:apply-templates select="meta"/>
</xsl:template>

<!-- Extraction des informations du body -->
<xsl:template match="body">
<!-- Extraction des sections  -->
<xsl:apply-templates select="*/section[@data-hdoc-type='exercise']"/>
</xsl:template>	

<xsl:template match="meta[@name='author']">
"author" : "<xsl:value-of select="normalize-space(@content)"/>",
</xsl:template>

<xsl:template match="meta[@name='date']">
"date" : "<xsl:value-of select="normalize-space(@content)"/>",
</xsl:template>

<xsl:template match="meta[@name='rights']">
"rights" : "<xsl:value-of select="normalize-space(@content)"/>",
</xsl:template>


<!-- Traitement des sections  -->
<xsl:template match="section[not(@data-hdoc-type='exercise')]"/>



<xsl:template match="section[@data-hdoc-type='exercise']">
"exercice_<xsl:value-of select="generate-id()"/>" : {
"titre" : "<xsl:value-of select="normalize-space(replace(header/h1/text(),$pPattern,$pReplacement))"/>",
<xsl:apply-templates select="div[@data-hdoc-type='description']"/> 	
} <xsl:if test=". != (//section[@data-hdoc-type='exercise'])[last()] or div[@data-hdoc-type='question']">,</xsl:if> 
<!-- On test si c'est le dernier exercice et qu'il n'a pas de question -->
<xsl:apply-templates select="div[@data-hdoc-type='question']">
<xsl:with-param name="prev_id" select="generate-id()"/>
<xsl:with-param name="is_not_last" select=". != (//section[@data-hdoc-type='exercise'])[last()]"/>
</xsl:apply-templates>
</xsl:template>


<!-- Traitement des question -->
<xsl:template match="div[@data-hdoc-type='question']">
<xsl:param name="prev_id"/>
<xsl:param name="is_not_last"/>
"question_<xsl:value-of select="generate-id()"/>" : {
"parent" : "exercice_<xsl:value-of select="$prev_id"/>",
<xsl:apply-templates select="div[@data-hdoc-type='description']"/>
<xsl:apply-templates select="div[@data-hdoc-type='solution']"/>
}<xsl:if test="((position() != last()) or $is_not_last)">,</xsl:if>
<!-- On test si c'est le dernier exercice et la dernière question -->
</xsl:template>


<!-- Traitement de l'élément description -->
<xsl:template match="div[@data-hdoc-type='description']">
"description" : "<xsl:value-of select="normalize-space(replace(./text(),$pPattern,$pReplacement))"/>"
</xsl:template>

<!-- Traitement de l'élément solution -->
<xsl:template match="div[@data-hdoc-type='solution']">
,"solution" : "<xsl:value-of select="normalize-space(replace(./text(),$pPattern,$pReplacement))"/>"
</xsl:template>

</xsl:stylesheet>