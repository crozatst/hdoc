# HDOC to ElasticSearch

## License
-------------
License GPL3.0 http://www.gnu.org/licenses/gpl-3.0.txt

## Credits
-------------
DELAUNAY Gr�gory
KELLER Vincent

## Presentation
-------------
Hdoc to ElasticSearch module extract data from an Hdoc file to use it with ElasticSearch


## Dependence
-------------
No depedencies

##Utilisation
-------------
Pour utiliser ce convertisseur, veuillez suivre les �tapes suivantes : 
1 - Placer le(s) fichier(s) hdoc que vous souhaitez convertir dans le dossier input (des exemples sont fournis dans le dossier sample).
2 - Lancer l'un des executable run.bat ou run.sh en fonction du syst�me d'exploitation de votre ordinateur.
3 - R�cup�rer le(s) r�sultat(s) au format .json dans le dossier output.

##Conversion
------------
Ce module permet : 
	- L'extraction des donn�es du header
		- Titre
		- Auteurs
		- Mots cl�s
		- Droits
	- L'extraction des exercices conform�ment aux attentes de Kibana : 
		- R�cup�ration des exercices et affectation d'un ID unique.
		- Association des questions aux exercices gr�ce aux ID g�n�r�s pr�c�demment.
		

##TODO
------

- Prendre en compte les futures modification de la conversion opale_to_hdoc pour les exercices.
- Ajouter la gestion des sections sachant que Kibana ne consid�re pas les tableaux d'objets JSON. Pour cela, la solution que nous avons retenu :
	- G�n�rer des ID via la function generate-id() pour produire une cl� de la forme section_ID.
	- Ajouter une r�f�rences vers l'id de la section m�re dans chacun des fils. (Ex : "parent" : "section_ID")


##Technical notes
-----------------
Les tabulations lors de la conversion en JSON sont d�conseill�s car cela pose probl�me avec Kibana. 