<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="html/body/div">
        <div>
            <xsl:apply-templates select="node()[not(preceding::h1)] | h1"/>
        </div>
    </xsl:template>
    
    <xsl:template match="h1">
        <xsl:variable name="numberh1" select="count(preceding-sibling::h1) + 1"/>
        <section>
            <header>
                <h1><xsl:value-of select="."/></h1>
            </header>
                <xsl:apply-templates select="following-sibling::node()[count(preceding-sibling::h1) = $numberh1 and not(self::h1)]"/>
        </section>
    </xsl:template>
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>