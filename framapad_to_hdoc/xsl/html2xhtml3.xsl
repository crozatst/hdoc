<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="//li[parent::p]" priority="1">
          <xsl:apply-templates select="@*|node()"/>
    </xsl:template>
    
    <xsl:template match="p[child::ul or child::ol]" priority="1">
        
      <xsl:apply-templates select="./ul"/>
      <xsl:apply-templates select="./ol"/>
      <p>
          <xsl:apply-templates select="./node()[not(self::ul) and not(self::ol)]"/>
      </p>
      
    </xsl:template>
    
    <xsl:template match="//ul[parent::ul or parent::ol]" priority="1">
        <li>
            <ul>
                <xsl:apply-templates select="*"/>
            </ul>
        </li>
    </xsl:template>
    <xsl:template match="//ol[parent::ul or parent::ol]" priority="1">
        <li>
            <ol>
                <xsl:apply-templates select="*"/>
            </ol>
        </li>
    </xsl:template>
    
    <xsl:template match="p[child::h1 or child::h2 or child::h3 or child::h4 or child::h5 or child::h6]">
        <xsl:apply-templates select="./*"/>
    </xsl:template>
    
    <!--Identity template, 
        provides default behavior that copies all content into the output -->
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
     
</xsl:stylesheet>
