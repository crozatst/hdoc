<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="html/body/div">
        <xsl:if test="count(node()[not(self::section) and not(preceding::section) and not(self::text())]) &gt; 0">
            <div>
                <xsl:apply-templates select="node()[not(self::section) and not(preceding::section)]"/>
            </div>
        </xsl:if>
        <xsl:apply-templates select="section"/>
    </xsl:template>
    
    <xsl:template match="section">
        <section>
            <xsl:apply-templates select="header"/>
            <xsl:if test="count(child::node()[not(self::text()) and not(self::header) and not(self::section)]) &gt; 0">
                <div>
                    <xsl:apply-templates select="child::node()[not(self::text()) and not(self::header) and not(self::section)]"/>
                </div>
            </xsl:if>
            <xsl:apply-templates select="section"/>
        </section>
    </xsl:template>
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>