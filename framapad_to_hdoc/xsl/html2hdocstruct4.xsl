<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="html/body/div">
        <div>
            <xsl:apply-templates select="node()[not(preceding::section) and not(preceding::h4)] | h4 | section"/>
        </div>
    </xsl:template>
    
    <xsl:template match="section">
        <section>
            <xsl:apply-templates select="node()[not(preceding-sibling::h4)] | h4"/>
        </section>
    </xsl:template>
    
    <xsl:template match="h4">
        <xsl:variable name="numberh4" select="count(preceding-sibling::h4) + 1"/>
        <section>
            <header>
                <h1><xsl:value-of select="."/></h1>
            </header>
            <xsl:apply-templates select="following-sibling::node()[count(preceding-sibling::h4) = $numberh4 and not(self::h4)]"/>
        </section>
    </xsl:template>
            
    <xsl:template match="h4[not(parent::section)]" priority="1">
        <xsl:variable name="numberh4out" select="count(preceding-sibling::h4) + 1"/>
        <section>
            <header>
                <h1><xsl:value-of select="."/></h1>
            </header>
            <xsl:apply-templates select="following-sibling::node()[count(preceding-sibling::h4) = $numberh4out and not(self::h4) and not(self::section)]"/>
        </section>
    </xsl:template>
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>