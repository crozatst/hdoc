<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="html/body/div">
        <div>
            <xsl:apply-templates select="node()[not(preceding::section) and not(preceding::h6)] | h6 | section"/>
        </div>
    </xsl:template>
    
    <xsl:template match="section">
        <section>
            <xsl:apply-templates select="node()[not(preceding-sibling::h6)] | h6"/>
        </section>
    </xsl:template>
    
    <xsl:template match="h6">
        <xsl:variable name="numberh6" select="count(preceding-sibling::h6) + 1"/>
        <section>
            <header>
                <h1><xsl:value-of select="."/></h1>
            </header>
            <xsl:apply-templates select="following-sibling::node()[count(preceding-sibling::h6) = $numberh6 and not(self::h6)]"/>
        </section>
    </xsl:template>
    
    <xsl:template match="h6[not(parent::section)]" priority="1">
        <xsl:variable name="numberh6out" select="count(preceding-sibling::h6) + 1"/>
        <section>
            <header>
                <h1><xsl:value-of select="."/></h1>
            </header>
            <xsl:apply-templates select="following-sibling::node()[count(preceding-sibling::h6) = $numberh6out and not(self::h6) and not(self::section)]"/>
        </section>
    </xsl:template>
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>