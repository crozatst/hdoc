<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs"
    xmlns="http://www.utc.fr/ics/hdoc/xhtml" version="2.0">
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="html" priority="3">
        <html>
            <head>
                <title>
                    <xsl:value-of select="/html/head/title"/>
                </title>
                <meta charset="utf-8"/>
            </head>
            <body>
                <section>
                    <header>
                        <h1>Contenu</h1>
                    </header>
                    <xsl:apply-templates select="/html/body/*"/>
                </section>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="li[not(child::ul) and not(child::ol)]" priority = "2">
        <li><p><xsl:apply-templates select="@* | node()"/></p></li>
    </xsl:template>


    <xsl:template match="@* | node()">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>
    <!--
    <xsl:template match="/">
        <xsl:apply-templates select="*"></xsl:apply-templates>
    </xsl:template>
    -->

    <!-- Namespace substitution for hdoc elements -->                        
    <xsl:template match="*" priority="1">
        <xsl:element name="{local-name()}">
            <xsl:apply-templates select="node()|@*"/>
        </xsl:element>
    </xsl:template> 

</xsl:stylesheet>
