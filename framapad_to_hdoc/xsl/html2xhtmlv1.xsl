<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes"/>
    <xsl:key name="bits" match="p/node()[not(self::br)]" use="generate-id((..|preceding-sibling::br[1])[last()])"/>
    
    <xsl:template match="ul[@class='indent']">
        
	<xsl:apply-templates select="*"/>
        
    </xsl:template>
    <xsl:template match="ul">
        <ul>
            <xsl:apply-templates select="*"/>
        </ul>
    </xsl:template>
    <xsl:template match="ol">
        <ol>
            <xsl:apply-templates select="*"/>
        </ol>
    </xsl:template>
    <xsl:template match="code">
        Code (non pris en compte pour l'instant) :<br/>
        <xsl:apply-templates select="@*|node()"/>
        <br/>
    </xsl:template>
    <xsl:template match="code[preceding-sibling::code]">
        <xsl:apply-templates select="@*|node()"/>
        <br/>
    </xsl:template>
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>
