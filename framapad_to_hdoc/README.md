# Converter framapad_to_hdoc

## License
License GPL3.0
http://www.gnu.org/licenses/gpl-3.0.txt

## Credits
- 2016
    - Etienne Chognard
    - Fabien Boucaud
- 2015
    - Jean-Côme Douteau
    - Gabrielle Rit
    - Jean Vintache
- 2014
    - Fecherolle Cécile

## Presentation
This module is able to convert several [framapad](https://framapad.org/) files (exported as html files) to the hdoc format.

## User documentation

## User Story

Vous êtes un utilisateur de framapad et en créez un pour un projet. Après avoir travaillé sur votre pad, vous souhaitez transformer ce document en un autre format que vous pourrez utiliser dans un nouveau contexte de travail. Pour ce faire, vous exportez le framapad au format HTML grâce au bouton « Import/Export ». Vous récupérez ensuite les fichiers nécessaires à la transformation framapad to hdoc sur le répertoire git du projet hdoc (voir http://hdoc.crzt.fr/). Il ne vous reste alors plus qu'à placer le fichier html précédemment récupéré dans le dossier « input » du dossier framapad_to_hdoc et à exécuter le /run.bat si vous êtes sur Windows ou le /run.sh si vous êtes sur Linux/Mac. Cela produira une archive .hdoc dont l'intérêt est de servir de format de passage pour une transformation d'un format à un autre, et ce pour une grande variété de format. Il vous restera ensuite à déterminer le nouveau format dans lequel vous voulez transformer votre hdoc et utiliser le convertisseur approprié s'il existe.

## Running framapad_to_hdoc.ant
1. Create a framapad document and export it as an html file.
2. please place your html files in the `/input` folder
3. run the `run.[bat|sh]` script of your choice depending on your OS
4. and retrieve the hdoc outputs in the `/output` folder

## Product Backlog

Currently (january 2017) available on: https://framemo.org/framapad_to_opale

See also : https://bimestriel.framapad.org/p/nf29_framapad_to_opale for the full documentation of our working process.

## TODO
- Update html2xhtml.xsl which ignores parts of some particular files for a reason probably linked to the regular expression used.
- Code tags
- Markdown
- Tags for typing the structure


## Technical notes
### Description of framapad_to_hdoc.ant

#### Prelude
- Importation of necessary classes (antlib, htmlcleaner, jing)
- Creation of directories architecture tree

#### Transformations
- Use of htmlcleaner to transform the input file from html to xhtml. For more info, see http://htmlcleaner.sourceforge.net/index.php.
- Apply html2xhtml.xsl : this xsl extracts the content into <body> tags
- Apply html2xhtmlv1.xsl : this xsl is used as a fix and adds br tag at the end of lists (ul and ol)
- Apply html2xhtmlv2.xsl : this xsl surround text line with p tags and transforms non-hdoc tags into hdoc tags as s, u, strong tags.
- Apply html2xhtml3.xsl : this xsl is used as a fix, it deletes p tags when its child is ul or ol
- Apply html2hdocstruct1 to 6 : those xsl files are dedicated to building the hdoc structure based on the titles h1 to h6
- Apply html2hdocstructdivsection: this xsl completes the sections created in the previous xsl with <div> around the actual content of each level
- Apply xhtml2hdoc.xsl : this xsl transforms the content into hdoc structure and changes the namespace

#### Post-transformations actions
- Build hdoc structure
- Jing checks if the output file is validated with the right rng schema
- Zip the directory into hdoc archive

### Supported tags
- html tags -> hdoc tags
- u, s, em, strong, color -> em
- sub -> sub
- sup -> sup
- li -> li
- ol -> ol
- br -> p

###Titles
- If it's h1 -> new section
	- If following title is not h1 -> new section in the section
		- If following title is at the same level -> new section at the same level
		- If following title is at a lower level -> new section in the previous section
- If there are any misplaced titles, they will become a new section at their current level of section if there is any. For example, if the first title of the doc is a h2 it will produce a section at the highest level as if it was a h1.

## Capitalisation
Using regular expression with xsl is a good way to parse a non xml file.

We can note about the creation of the hdoc's structure with the titles h1 to h6 that XSLT is probably not the best tool for these specific transformations. Something like SAX, which actually explore the XML files sequentially, would probably be more efficient and easier to use for these kinds of transformations.
