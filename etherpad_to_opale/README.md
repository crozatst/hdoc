﻿# Etherpad2Opale -- HDOC CONVERTER PROJECT
## License
[GPL 3.0](http://www.gnu.org/licenses/gpl-3.0.txt)
## Credits
- Rit Gabrielle
- Vintache Jean
- Douteau Jean-Come
- Fecherolle Cecile (2014)

## Presentation
How to transform an etherpad document in opale document.

Filepaths in this document are relative to this readme file.

## Dependence
- Etherpad2Hdoc
- Hdoc2Opale

## User Documentation
1. Download an etherpad document in html format.
	1. Create or join an etherpad document then export it in html format (Import/Export Button) in the `/input` directory (if the directory does not exists, you have to create it).
2. Execute the file `/run.bat` or `/run.sh` depending on the OS. A `.scar` file is created in the directory `/output`
*If the `/input` directory contains multiple files, they will be all treated.
3. Open the document with Opale
	1. Open Scenari, and choose "UTC-etu_opale" as distant depot.
	2. Go in the directory `sandBox/etherpad-to-opale.`
	3. Import your `.scar` file in the directory.
	4. Open the file Main.xml created.

## Unsupported
- MarkDown
- Timeline and author paternity
- Chat

## Known bugs
Nested lists in lists are not supported.

Here's an example :

`<ul>
	<li>
		<ul>
			<li>
			Never gonna give you up.
			</li>
		</ul>
	</li>
<ul>`

## TODO
- Work with markdown
- Correct nested lists

## Technical notes
### Description of etherpad_to_hdoc.ant

#### Prelude
- Importation of necessary classes (antlib, htmlcleaner, jing)
- Creation of directories architecture tree

#### Transformations
- Use of htmlcleaner to transform the input file from html to xhtml. For more info, see http://htmlcleaner.sourceforge.net/index.php.
- Apply html2xhtml.xsl : this xsl extracts the content into <body> tags
- Apply html2xhtmlv1.xsl : this xsl is used as a fix and adds br tag at the end of lists (ul and ol)
- Apply html2xhtmlv2.xsl : this xsl surround text line with p tags and transforms non-hdoc tags into hdoc tags as s, u, strong tags.
- Apply html2xhtml3.xsl : this xsl is used as a fix, it deletes p tags when its child is ul or ol
- Apply xhtml2hdoc.xsl : this xsl transforms the content into hdoc structure

#### Post-transformations actions
- Build hdoc structure
- Jing checks if the output file is validated with the right rng schema
- Zip the directory into hdoc archive

## Capitalisation
We have to change how we get the input file:
1 Get the first file of the input directory
2 Make the run with the name of the file as parameter
Finally, ant should be changed to manage multiple entries