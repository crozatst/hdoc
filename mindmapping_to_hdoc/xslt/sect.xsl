<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.utc.fr/ics/hdoc/xhtml" xmlns:utc="http://www.utc.fr/ics/hdoc/xhtml"
    xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <!-- This is wrong -> xpath-default-namespace="http://www.utc.fr/ics/hdoc/xhtml"-->

    <xsl:output method="xml" indent="yes"/>
    <!-- Remove spaces/tabs -->
    <xsl:strip-space elements="*"/>

    <!-- Entry point -->
    <xsl:template match="map">
        <xsl:processing-instruction name="oxygen">
            RNGSchema="http://hdoc.crzt.fr/schemas/xhtml/hdoc1-xhtml.rng" type="xml"
        </xsl:processing-instruction>
        <html>
            <head>
                <xsl:apply-templates mode="header"/>
            </head>
            <body>
                <xsl:apply-templates/>
            </body>
        </html>

    </xsl:template>

    <!-- Header in hdoc -->
    <xsl:template match="node[1]" mode="header">
        <title>
            <xsl:value-of select="utc:remove-language(@TEXT)"/>
            <xsl:value-of select="richcontent"/>
        </title>
        <meta charset="utf-8"/>
        <meta name="author" content=""/>
    </xsl:template>

    <!-- Default behavior for node -->
    <xsl:template match="node">
        <xsl:element name="section">
            <header>
                <h1>
                    <xsl:value-of select="utc:remove-language(@TEXT)"/>
                    <xsl:value-of select="richcontent"/>
                </h1>
                <!--Introduction-->
                <xsl:if test="child::*[contains(@TEXT, '#intro')]">
                    <xsl:element name="div">
                        <xsl:attribute name="data-hdoc-type">introduction</xsl:attribute>
                        <xsl:value-of
                            select="utc:remove-language(child::*[contains(@TEXT, '#intro')][1]/@TEXT)"
                        />
                    </xsl:element>
                </xsl:if>

            </header>
            <!-- Process children -->
            <!--<xsl:apply-templates select="node"/>-->
            <xsl:apply-templates/>
            <footer>
                <!--Conclusion-->
                <xsl:if test="child::*[contains(@TEXT, '#conclu')]">
                    <xsl:element name="div">
                        <xsl:attribute name="data-hdoc-type">conclusion</xsl:attribute>
                        <xsl:value-of
                            select="utc:remove-language(child::*[contains(@TEXT, '#conclu')][1]/@TEXT)"
                        />
                    </xsl:element>
                </xsl:if>
            </footer>
        </xsl:element>
    </xsl:template>

    <!-- Behavior when node has no children -->
    <xsl:template
        match="node[not(node)] | node[child::*[contains(@TEXT, '#p')]] | node[child::*[contains(@TEXT, '#a')]]"
        priority="1">
        <xsl:element name="section">
            <xsl:attribute name="data-hdoc-type">unit-of-content</xsl:attribute>
            <xsl:if test="child::attribute[@NAME = 'anchor']">
                <xsl:attribute name="id">
                    <xsl:value-of select="child::attribute[@NAME = 'anchor'][1]/@VALUE"/>
                </xsl:attribute>
            </xsl:if>
            <header>
                <h1>
                    <xsl:value-of select="utc:remove-language(@TEXT)"/>
                    <xsl:value-of select="richcontent"/>
                </h1>
            </header>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

    <!-- Paragraph -->
    <xsl:template match="node[contains(@TEXT, '#p')]" priority="2">
        <xsl:element name="div">
            <xsl:element name="p">
                <xsl:value-of select="utc:remove-language(@TEXT)"/>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="node[contains(@TEXT, '#p')]" priority="3" mode="sub">
            <xsl:element name="p">
                <xsl:value-of select="utc:remove-language(@TEXT)"/>
            </xsl:element>
    </xsl:template>
    
    <xsl:template match="node[contains(@TEXT, '#p')]" priority="3" mode="list">
        <xsl:element name="li">
            <xsl:element name="p">
                <xsl:value-of select="utc:remove-language(@TEXT)"/>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    
    <!-- Liste --> 
    <xsl:template match="node[contains(@TEXT, '#list')]" priority="2">
        <xsl:element name="section">
            <xsl:element name="header">
                <xsl:element name="h1"><xsl:value-of select="utc:remove-language(@TEXT)"/></xsl:element>
            </xsl:element>
            <xsl:element name="div">
                <xsl:element name="ul">
                    <xsl:apply-templates mode="list"></xsl:apply-templates>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:template>

    <!-- Definition -->
    <xsl:template match="node[contains(@TEXT, '#def')]" priority="2">
        <xsl:element name="section">
            <xsl:element name="header">
                <xsl:element name="h1">Definition</xsl:element>
            </xsl:element>
            <xsl:element name="div">
                <xsl:attribute name="data-hdoc-type">definition</xsl:attribute>
                <xsl:element name="h6">
                    <xsl:value-of select="utc:remove-language(@TEXT)"/>
                </xsl:element>
                <xsl:element name="p"/>
                <xsl:apply-templates mode="sub"/>
            </xsl:element>
        </xsl:element>
    </xsl:template>

    <!-- Example -->
    <xsl:template match="node[contains(@TEXT, '#ex')]" priority="2">
        <xsl:element name="section">
            <xsl:element name="header">
                <xsl:element name="h1">Exemple</xsl:element>
            </xsl:element>
            <xsl:element name="div">
                <xsl:attribute name="data-hdoc-type">example</xsl:attribute>
                <xsl:element name="h6">
                    <xsl:value-of select="utc:remove-language(@TEXT)"/>
                </xsl:element>
                <xsl:element name="p"/>
                <xsl:apply-templates mode="sub"/>
            </xsl:element>
        </xsl:element>
    </xsl:template>

    <!-- Remark -->
    <xsl:template match="node[contains(@TEXT, '#rmk')]" priority="2">
        <xsl:element name="section">
            <xsl:element name="header">
                <xsl:element name="h1">Remarque</xsl:element>
            </xsl:element>
            <xsl:element name="div">
                <xsl:attribute name="data-hdoc-type">remark</xsl:attribute>
                <xsl:element name="h6">
                    <xsl:value-of select="utc:remove-language(@TEXT)"/>
                </xsl:element>
                <xsl:element name="p"/>
                <xsl:apply-templates mode="sub"/>
            </xsl:element>
        </xsl:element>
    </xsl:template>

    <!-- Warning -->
    <xsl:template match="node[contains(@TEXT, '#wrng')]" priority="2">
        <xsl:element name="section">
            <xsl:element name="header">
                <xsl:element name="h1">Warning</xsl:element>
            </xsl:element>
            <xsl:element name="div">
                <xsl:attribute name="data-hdoc-type">warning</xsl:attribute>
                <xsl:element name="h6">
                    <xsl:value-of select="utc:remove-language(@TEXT)"/>
                </xsl:element>
                <xsl:element name="p"/>
                <xsl:apply-templates mode="sub"/>
            </xsl:element>
        </xsl:element>
    </xsl:template>

    <!-- Advice -->
    <xsl:template match="node[contains(@TEXT, '#adv')]" priority="2">
        <xsl:element name="section">
            <xsl:element name="header">
                <xsl:element name="h1">Conseil</xsl:element>
            </xsl:element>
            <xsl:element name="div">
                <xsl:attribute name="data-hdoc-type">advice</xsl:attribute>
                <xsl:element name="h6">
                    <xsl:value-of select="utc:remove-language(@TEXT)"/>
                </xsl:element>
                <xsl:element name="p"/>
                <xsl:apply-templates/>
            </xsl:element>
        </xsl:element>
    </xsl:template>

    <!-- Complement -->
    <xsl:template match="node[contains(@TEXT, '#compl')]" priority="2">
        <xsl:element name="section">
            <xsl:element name="header">
                <xsl:element name="h1">Complement</xsl:element>
            </xsl:element>
            <xsl:element name="div">
                <xsl:attribute name="data-hdoc-type">complement</xsl:attribute>
                <xsl:element name="h6">
                    <xsl:value-of select="utc:remove-language(@TEXT)"/>
                </xsl:element>
                <xsl:element name="p"/>
                <xsl:apply-templates mode="sub"/>
            </xsl:element>
        </xsl:element>
    </xsl:template>

    <!-- Emphasis -->
    <xsl:template match="node[contains(@TEXT, '#emph')]" priority="2">
        <xsl:element name="section">
            <xsl:element name="header">
                <xsl:element name="h1">Emphase</xsl:element>
            </xsl:element>
            <xsl:element name="div">
                <xsl:attribute name="data-hdoc-type">emphasis</xsl:attribute>
                <xsl:element name="h6">
                    <xsl:value-of select="utc:remove-language(@TEXT)"/>
                </xsl:element>
                <xsl:element name="p"/>
                <xsl:apply-templates mode="sub"/>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    
    <!-- Emphasis with bold -->
    <xsl:template match="node/font[@BOLD ='true']" priority="3">
        <xsl:comment>passage par bold</xsl:comment>
        <xsl:element name="section">
            <xsl:element name="header">
                <xsl:element name="h1">Emphase</xsl:element>
            </xsl:element>
            <xsl:element name="div">
                <xsl:attribute name="data-hdoc-type">emphasis</xsl:attribute>
                <xsl:element name="h6">
                    <xsl:value-of select="../utc:remove-language(@TEXT)"/>
                </xsl:element>
                <xsl:element name="p"/>
                <xsl:apply-templates mode="sub"/>
            </xsl:element>
        </xsl:element>    
    </xsl:template>

    <!-- Link : need to use anchor attribute ? -->
    <xsl:template match="node[contains(@TEXT, '#a')]" priority="2">
        <xsl:element name="div">
            <xsl:element name="p">
                <xsl:element name="a">
                    <xsl:attribute name="href">
                        <xsl:text>#</xsl:text>
                        <xsl:value-of select="utc:remove-language(@TEXT)"/>
                    </xsl:attribute>
                    <xsl:value-of select="utc:remove-language(@TEXT)"/>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    
    <!-- MCQ -->
    <xsl:template match="node[contains(@TEXT, '#question')]" priority="2">
        <xsl:element name="section">
            <xsl:element name="header">
                <xsl:element name="h1">QCM</xsl:element>
            </xsl:element>
            <xsl:element name="div">
                <xsl:attribute name="data-hdoc-type">question</xsl:attribute>
                <xsl:element name="ul">
                    <xsl:element name="li">
                        <xsl:element name="p">
                            <xsl:value-of select="utc:remove-language(@TEXT)"/>
                        </xsl:element>
                    </xsl:element> 
                </xsl:element>
            </xsl:element>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    
    <!-- QCU -->
    <xsl:template match="node[contains(@TEXT, '#singlechoicequestion')]" priority="2">
        <xsl:element name="section">
            <xsl:element name="header">
                <xsl:element name="h1">QCU</xsl:element>
            </xsl:element>
            <xsl:element name="div">
                <xsl:attribute name="data-hdoc-type">singlechoicequestion</xsl:attribute>
                <xsl:element name="ul">
                    <xsl:element name="li">
                        <xsl:element name="p">
                            <xsl:value-of select="utc:remove-language(@TEXT)"/>
                        </xsl:element>
                    </xsl:element> 
                </xsl:element>
            </xsl:element>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    
    
    
    <!-- Match the global explanation of of a question within a QCM -->
    <xsl:template match="node[contains(@TEXT, '#explanation')]" priority="2">
        <xsl:element name="div">
            <xsl:attribute name="data-hdoc-type">explanation</xsl:attribute>
            <xsl:element name="p">
                <xsl:value-of select="utc:remove-language(@TEXT)"/>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    
    <!-- Match the correct answers of a question within a QCM -->
    <xsl:template match="node[contains(@TEXT, '#choice-correct')]" priority="2">
        <xsl:element name="div">
            <xsl:attribute name="data-hdoc-type">choice-correct</xsl:attribute>
            <xsl:element name="p">
                <xsl:value-of select="utc:remove-language(@TEXT)"/>
            </xsl:element>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    
    <!-- Match the wrong answers of a question within a QCM -->
    <xsl:template match="node[contains(@TEXT, '#choice-incorrect')]" priority="2">
        <xsl:element name="div">
            <xsl:attribute name="data-hdoc-type">choice-incorrect</xsl:attribute>
            <xsl:element name="p">
                <xsl:value-of select="utc:remove-language(@TEXT)"/>
            </xsl:element>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    
    <!-- Match the local explanation of an incorrect answer within a QCM -->
    <xsl:template match="node[contains(@TEXT, '#explanation-choice-incorrect')]"  priority="2">
        <xsl:element name="div">
            <xsl:attribute name="data-hdoc-type">explanation-choice-incorrect</xsl:attribute>
            <xsl:element name="p">
                <xsl:value-of select="utc:remove-language(@TEXT)"/>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    
    <!-- Match the local explanation of a correct answer within a QCM -->
    <xsl:template match="node[contains(@TEXT, '#explanation-choice-correct')]"  priority="2">
        <xsl:element name="div">
            <xsl:attribute name="data-hdoc-type">explanation-choice-correct</xsl:attribute>
            <xsl:element name="p">
                <xsl:value-of select="utc:remove-language(@TEXT)"/>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    
    
    <!-- Match the answers of a question within a QCU -->
    <xsl:template match="node[contains(@TEXT, '#choice-qcu')]" priority="2">
        <xsl:element name="div">
            <xsl:attribute name="data-hdoc-type">choice</xsl:attribute>
            <xsl:element name="p">
                <xsl:value-of select="utc:remove-language(@TEXT)"/>
            </xsl:element>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    
    <!-- Match the solution of a QCU -->
    <xsl:template match="node[contains(@TEXT, '#solution')]" priority="2">
        <xsl:element name="div">
            <xsl:attribute name="data-hdoc-type">solution</xsl:attribute>
            <xsl:element name="p">
                <xsl:value-of select="utc:remove-language(@TEXT)"/>
            </xsl:element>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    
    <!-- Default behavior for intro and conclu nodes : do nothing -->
    <xsl:template match="node[contains(@TEXT, '#intro')] | node[contains(@TEXT, '#conclu')]"
        priority="3"/>

    <xsl:template match="richcontent">
        <xsl:value-of select="utc:remove-language(html/body/p)"/>
    </xsl:template>

    <!-- Utility functions -->
    <xsl:function name="utc:remove-language" as="xs:string">
        <xsl:param name="text" as="xs:string?"/>
        <xsl:variable name="text2" select="string($text)"/>
        <xsl:value-of select="replace($text2, '(#[0-9a-z|-]* )+', '')"/>
    </xsl:function>

</xsl:stylesheet>
