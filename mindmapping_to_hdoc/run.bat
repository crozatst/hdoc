@echo off
set lib=lib
set ant=mindmapping_to_hdoc.ant
set antparam=-Dprogram.param=%1
set inputPath=%2
SETLOCAL ENABLEEXTENSIONS

set scJarList=%lib%\*

::java.exe -classpath "%scJarList%" -Xmx150m org.apache.tools.ant.Main -buildfile %ant% %antparam%

if defined (%inputPath%) (
pause
java.exe -classpath "%scJarList%" -Xmx150m org.apache.tools.ant.Main -buildfile %ant% %antparam% -DinputPath %inputPath% 
pause
)

if not defined (%inputPath%) (
pause
java.exe -classpath "%scJarList%" -Xmx150m org.apache.tools.ant.Main -buildfile %ant% %antparam%  
pause
)

REM start /MIN java.exe -classpath "%scJarList%" -Xmx150m org.apache.tools.ant.Main -buildfile %ant% %antparam%
