<?xml version="1.0" encoding="UTF-8" standalone="no"?><map version="0.8.1">
    <node CREATED="1450696201432" ID="2nngtspni6hi1484pfec44c8lk" MODIFIED="1450696201432" TEXT="MindMapping">
        <node CREATED="1450696201432" ID="00lp6bpfm6jibhcjrpdonrhoqb" MODIFIED="1450696201432" POSITION="right" TEXT="#1 MindMapping to Hdoc">
        <node CREATED="1450696201432" ID="3qm1jgu7dfgs328jthlek5fd4e" MODIFIED="1450696201432" TEXT="Prendre en main le projet existant"/>
            <node CREATED="1450696201432" ID="0u38ectmhqqp4p08p5guvgij71" MODIFIED="1450696201432" TEXT="#wrng Corriger les bugs existants">
                <node CREATED="1450696201432" ID="6cl96umgglk7bg98n0u6v3tena" MODIFIED="1450696201432" TEXT="#p Il subsiste des bugs dans la transformation."/>
            </node>
            <node CREATED="1450696201432" ID="3b1t9o2te6enh59j4iv48f4mdi" MODIFIED="1450696201432" TEXT="Restructurer le ant"/>
        </node>
        <node CREATED="1450696201432" ID="6lohsep5crh5g3ngj8ld8loei6" MODIFIED="1450696201432" POSITION="right" TEXT="#2 MindMapping to Opale">
            <node CREATED="1450696201432" ID="5pl3210hq6if3nv7r2chtjstmo" MODIFIED="1450696201432" TEXT="Faire la connexion avec le transformateur Hdoc to Opale"/>
            <node CREATED="1450696201432" ID="3haa2fgohe9j4n04ilddco84uh" MODIFIED="1450696201432" TEXT="Permettre la personnalisation de l'organisation du module"><node CREATED="1450696201432" ID="4ie264076qt3tnlu0pqpp1dh1p" MODIFIED="1450696201432" TEXT="#ex A l'aide de hashtag">
                <node CREATED="1450696201432" ID="6qegjh2snbh0s21cn7sdukun6v" MODIFIED="1450696201432" TEXT="#p L'idée est d'utiliser les hashtag pour identifier le type de contenu"/>
            </node>
                <node CREATED="1450696201432" ID="23mafrh60fmdmkjsp1jdg6c58g" MODIFIED="1450696201432" TEXT="#ex A l'aide d'attributs">
                    <node CREATED="1450696201432" ID="3cofh24e4l95fo0pgcqiefc3gv" MODIFIED="1450696201432" TEXT="#p L'idée est d'utiliser les attributs pour identifier le type de contenu"/>
                </node>
            </node>
            <node CREATED="1450696201432" ID="11k2hbkcu415e1mgk5c5ucr48k" MODIFIED="1450696201432" TEXT="Permettre la personnalisation dy type de noeud (Intro, conclusion etc.)">
                <node CREATED="1450696201432" ID="5pf0idbv9nln725d14rs1c1p0a" MODIFIED="1450696201432" TEXT="#ex A l'aide d'attributs">
                    <node CREATED="1450696201432" ID="6pa7h6p941mljiimpb18tv628g" MODIFIED="1450696201432" TEXT="#p L'idée est d'utiliser les hashtag pour identifier le type de contenu"/>
                </node>
                <node CREATED="1450696201432" ID="48799q1i5c6rns3smpa8vnekfb" MODIFIED="1450696201432" TEXT="#ex A l'aide de hashtag">
                    <node CREATED="1450696201432" ID="1j3m1erstm0171a1i29hg3dgj6" MODIFIED="1450696201432" TEXT="#p L'idée est d'utiliser les attributs pour identifier le type de contenu"/>
                </node>
            </node>
            <node CREATED="1450696201432" ID="4npf8jpvak3lffn9c1rsogi0sq" MODIFIED="1450696201432" TEXT="Permettre la personnalisation grâce aux couleurs/icônes"/>
        </node>
        <node CREATED="1450696201432" ID="3n8v43p8qb005puq3qiv7aiubc" MODIFIED="1450696201432" POSITION="right" TEXT="#3 MindMapping to Optim">
            <node CREATED="1450696201432" ID="2bn1la3apnjf1fi8qqfqde226t" MODIFIED="1450696201432" TEXT="#def Faire la connexion avec le transformateur Hdoc to Optim">
                <node CREATED="1450696201432" ID="2juvq1lehuivco8c1h0scqh1lr" MODIFIED="1450696201432" TEXT="#p Le connecteur hdoc_to_optim n'est pas encore opérationnel"/>
            </node>
        </node>
        <node CREATED="1450696201432" ID="3s00si3hquol4pldg7lo9opjd6" MODIFIED="1450696201432" POSITION="right" TEXT="#4 MindMapping to Lexique">
            <node CREATED="1450696201432" ID="37jlfl25iuaoe6f2cpiv30qksd" MODIFIED="1450696201432" TEXT="#p Faire la connexion avec le transformateur Hdoc to Lexique"/>
        </node>
        <node CREATED="1450696201432" ID="27gnl6pgottjfiukpa3ulfdjrj" MODIFIED="1450696201432" POSITION="left" TEXT="#5 Prendre d'autres outils de carte mentales en entrée">
            <node CREATED="1450696201432" ID="6ci8ukhfg8fd96vsvgn9sb30cr" MODIFIED="1450696201432" TEXT="#a http://hdoc.crzt.fr/www/co/hdocConverter.html"/>
        </node>
        <node CREATED="1450696201432" ID="24d41nnnnvvu7fv5m8m4vegu0g" MODIFIED="1450696201432" POSITION="left" TEXT="#intro This converter aims at convert a given MindMapping file into a Hdoc file which then will be imported in Opale (advanced only)."/>
        <node CREATED="1450696201432" ID="6a7pr2b5vvmq6na5964dhf9e13" MODIFIED="1450696201432" POSITION="left" TEXT="#conclu DONE : MindMapping to Hdoc, MindMapping to Opale TODO : MindMapping to Optim, MindMapping to Lexique"/>
    </node>
</map>