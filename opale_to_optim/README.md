﻿Converter opale_to_optim
-----------------------

The purpose of this converter is to obtain an Optim document (paper, slideshow, website or webpage) from a Opale document.

The Hdoc pivot format is used to convert one format to another, losing the least amount of information.  It is based on HTML.


License GPL3.0
--------------

http://www.gnu.org/licenses/gpl-3.0.txt


Credits
-------

* Thibault DRAIN
* Rihab HACHEM
* Perrine RAVAUD (2014)
* Christophe Virot (2014)
* Pierre Lemaire (2014)


Dependance
----------

This converter is based on two converters :
* Opale to Hdoc
* Hdoc to Optim


User documentation
------------------

There are two different ways to use the converter opale_to_optim: by running a script run.bat/run.sh or by using a terminal (allows the user to specify some parameters like the document type). 


#### Running the script run.bat/run.sh:

Use this method if you do not want to use a terminal. 

1. Download hdoc_converter.zip and unzip it.

2. Add your source file to the Input folder. It has to be a .scar file. 

	Extract a module file from your Opale workspace. You can't export directly a web, paper or slide show support. You must extract a scar module. Moreover, when you extract a module, keep the default configuration :
	
	* "Include descendant network" input : checked
	* "keep workspaces" : unchecked

3. Place only one file in that folder !!

4. If you are using Linux, run the script run.sh. If you are using Windows, run the script run.bat. 

5. Your file has been converted, the result is in the Output folder. 

6. You can now open it with OptimOffice.

The default output document type used by this script is an Optim paper. 


#### Terminal: 

By using the terminal you can specify some parameters to the conversion : the source file, or the output type (either a paper, a slideshow, a website or a webpage).

1. Download hdoc_converter.zip and unzip it.
2. Open your terminal and go into the folder opale_to_optim.
3. Run the following command:

        "ant -buildfile opale_to_optim.ant"
    
    You can specify the source file, and the output document type by adding parameters.
    Use -DSource to specify the source file. 
    Use -DDocType to specify if it's a "paper", a "slideshow", a "website" or a "webpage".
    Exemples:
        
        "ant -buildfile opale_to_optim.ant -DDocType slideshow -DSource sample.scar"

	"ant -buildfile opale_to_optim.ant -DDocType website"


Both parameters are optional. Your file has been converted, you can open your document with OptimOffice.


Unsupported 
-----------

Refer to the unsupported elements in Opale to Hdoc and in Hdoc to Optim. 


Known bugs
----------

Refer to the known bugs in Opale to Hdoc and in Hdoc to Optim. 


Todo
---- 

Refer to the Todos in Opale to Hdoc and in Hdoc to Optim. 

* Offer the possibility of placing multiple input files.


Technical notes
---------------

The converter contains 1 file:

* opale_to_optim.ant
        
    It checks wether or not the user specified the parameters and performs the following : 
    * Copy the input file in the opale_to_hdoc directory.
    * Perform the opale_to_hdoc transformation.
    * Copy the result in the hdoc_to_optim directory. 
    * Perform the hdoc_to_optim transformation. 
    * Copy the result in the opale_to_optim directory. 


Capitalisation
--------------

N/A
