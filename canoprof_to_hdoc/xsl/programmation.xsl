<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns="http://www.utc.fr/ics/hdoc/xhtml"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0"
    xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
    xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
    xmlns:cp="canope.fr:canoprof">
    <xsl:output method="xhtml"/>
    <xsl:strip-space elements="*" /> 
    <!-- Il faut uniformiser les sc:para -->
    <xsl:template match="sc:item">
        <xsl:apply-templates/>
    </xsl:template>
    <!--<xsl:template match="*"></xsl:template>-->
    <xsl:template match="cp:program">
        <xsl:processing-instruction name="oxygen">
         RNGSchema="../../schemas/xhtml/hdoc1-xhtml.rng" type="xml"</xsl:processing-instruction>
        <html>
            <head>
                <title>
                    <xsl:choose>
                        <xsl:when test="string-length(cp:programM/sp:title) > 0">
                            <xsl:value-of select="cp:programM/sp:title"/>
                        </xsl:when>
                        <xsl:otherwise>
                            PROGRAMMATION 
                            <xsl:if test="./cp:programM/sp:class">
                                <xsl:value-of select="./cp:programM/sp:class"/><xsl:text> </xsl:text>
                            </xsl:if>
                            <xsl:if test="./cp:programM/sp:period">
                                [<xsl:value-of select="./cp:programM/sp:period"/>]
                            </xsl:if>                            
                        </xsl:otherwise>
                    </xsl:choose>
                </title>
                <meta charset="utf-8"/>
                <meta content="HdocConverter/Opale3.4" name="generator"/>
                <xsl:apply-templates select="cp:programM/child::*[name() != 'sp:abstract' and name()!= 'sp:title']"/>
            </head>
            <body>
                <xsl:apply-templates select="cp:programM/sp:abstract"/>
                <xsl:apply-templates select="child::*[name() != 'cp:programM']"/>
                <xsl:apply-templates select="descendant::*[sp:question and ../name() != 'sp:question']/sp:question | descendant::*[sp:exercice]/sp:exercice"/>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="sp:class">
        <meta name="description" content="{.}" />
    </xsl:template>
    <xsl:template match="cp:programM/sp:period">
        <meta name="date" content="{.}" />
    </xsl:template>
    <xsl:template match="sp:abstract">
        <section data-hdoc-type="introduction">
            <header>
                <h1>Introduction</h1>
            </header>
            <!-- a introduction has only a bloc -->
            <div>
                <xsl:apply-templates select="./cp:txtDesc/*"/>
            </div>          
        </section>
    </xsl:template>
    <xsl:template match="sp:sequence/cp:sequence">
        <section>
            <header>
                <h1><xsl:value-of select="./cp:sequenceM/sp:title"/></h1>
                <xsl:apply-templates select=".//cp:competence"/>
            </header>
            <xsl:apply-templates select=".//sp:ojectives"/>
            <xsl:apply-templates select="child::*[name()!= 'cp:sequenceM']"/>
        </section>
    </xsl:template>
    <xsl:template match="sp:ojectives">
        <section>
            <header>
                <h1>Objectifs</h1>
            </header>
            <div>
                <xsl:apply-templates select="./cp:txtDesc/*"/>
            </div>
        </section>
    </xsl:template>
    <xsl:template match="sp:session">
        <section>
            <header>
                <h1><xsl:value-of select="cp:session/cp:sessionM/sp:title"/></h1>
                <!-- gérer les sessionM -->
                
            </header>
            <xsl:apply-templates select="cp:session/*[name() != 'cp:sessionM']"/>
        </section>
    </xsl:template>
    
    <xsl:template match="sp:profInstruction">
        <div><xsl:apply-templates select="cp:txt/sc:para"/></div>
    </xsl:template>
    
    <!-- ***** TEXT START ***** -->
    
    <!-- Simple text 
    <xsl:template match="cp:res/sp:txt">
        <xsl:apply-templates select="./op:txt/*" />
    </xsl:template>-->
    
    <!-- Paragraph -->
    <xsl:template match="sc:para"> 
        <p>
            <xsl:apply-templates select="./* | ./text()" />
        </p>
    </xsl:template>
    
    <!-- ***** PARAGRAPH ITEMS START ***** -->
    
    <!-- citation -->
    <xsl:template match="sc:para/sc:inlineStyle[@role='quote']" priority="2">
        <q>
            <xsl:value-of select="." />
        </q>
    </xsl:template>
    <!-- important -->
    <xsl:template match="sc:para/sc:inlineStyle[@role='emphasis']" priority="2">
        <em>
            <xsl:value-of select="." />
        </em>
    </xsl:template>
    <!-- foreign term -->
    <xsl:template match="sc:para/sc:inlineStyle[@role='specific']" priority="2">
        <i>
            <xsl:value-of select="." />
        </i>
    </xsl:template>
    <!-- syntax -->
    <xsl:template match="sc:para/sc:inlineStyle[@role='code']" priority="2">
        <span data-hdoc-type="syntax">
            <xsl:value-of select="." />
        </span>
    </xsl:template>
    
    <xsl:template match="sc:para/sc:uLink" priority="2">
        <xsl:value-of select="." />
    </xsl:template>
    <!-- exponent -->
    <xsl:template match="sc:para/sc:textLeaf[@role='exp']" priority="2">
        <sup>
            <xsl:value-of select="." />
        </sup>
    </xsl:template>
    <!-- subscript -->
    <xsl:template match="sc:para/sc:textLeaf[@role='ind']" priority="2">
        <sub>
            <xsl:value-of select="." />
        </sub>
    </xsl:template>
    <!-- latex -->
    <xsl:template match="sc:para/sc:textLeaf[@role='mathtex']" priority="2">
        <span data-hdoc-type="latex">
            <xsl:value-of select="." />
        </span>
    </xsl:template>
    
    <xsl:template match="sc:para/sc:textLeaf[@role='gap']" priority = "2">
        <span data-hdoc-type="blank">
            <xsl:value-of select="."/>
        </span>    
    </xsl:template>
    
    <!-- default behaviour for other balises -->
    <xsl:template match="sc:para/sc:inlineStyle|sc:para/sc:textLeaf" priority="1">
        <xsl:value-of select="." />
    </xsl:template>
    
    <xsl:template match="sc:para/sc:phrase[@role='url']">
        <xsl:apply-templates select="cp:link"/>
    </xsl:template>
    
    <xsl:template match="cp:link">
        <a href="{sp:url/text()}"><xsl:value-of select="sp:title/text()"></xsl:value-of></a>
    </xsl:template>
    
    <!-- ***** PARAGRAPH ITEMS END ***** -->
    
    <!-- List-->
    <xsl:template match="sc:itemizedList">
        <ul>
            <xsl:apply-templates select="./sc:listItem" />
        </ul>
    </xsl:template>
    
    <!-- Ordered list -->
    <xsl:template match="sc:orderedList">
        <ol>
            <xsl:apply-templates select="./sc:listItem" />
        </ol>
    </xsl:template>
    
    <!-- ***** LIST ITEMS START ***** -->
    
    <!-- List item -->
    <xsl:template match="sc:listItem">
        <li>
            <xsl:apply-templates select="./*" />
        </li>
    </xsl:template>
    
    <!-- ***** LIST ITEMS END ***** -->
    
    <!-- Table -->
    <xsl:template match="sc:table">
        <table>
            <xsl:apply-templates select="./*" />
        </table>
    </xsl:template>
    
    <!-- ***** TABLE ITEMS START ***** -->
    
    <!-- caption -->
    <xsl:template match="sc:table/sc:caption">
        <caption>
            <xsl:value-of select="."></xsl:value-of>
        </caption>
    </xsl:template>
    <!-- table row -->
    <xsl:template match="sc:table/sc:row">
        <tr>
            <xsl:apply-templates select="./*" />
        </tr>
    </xsl:template>
    <!-- table cell -->
    <xsl:template match="sc:table/sc:row/sc:cell">
        <td>
            <xsl:apply-templates select="./*" />
        </td>
    </xsl:template>
    
    <!-- ***** TABLE ITEMS END ***** -->
    
    <!-- ***** TEXT END ***** -->
    
    
    
    <!-- ******** ACTIVITE TETM ********** -->
    <xsl:template match="sp:textActivity/cp:textActivity | sc:item/cp:textActivity | sp:section/cp:textActivity">
        <section>
            <header>
                <h1><xsl:value-of select="cp:activityM/sp:title"/></h1>
            </header>
           
            <xsl:apply-templates select="sp:body/cp:blocks/*[name() != 'sp:question' and name() != 'sp:exercice'] | sp:section/cp:textActivity"/>
        </section>
    </xsl:template>
    
    <!-- Gestion transclusion d'activite -->
    <xsl:template match="sp:textActivity[@sc:refUri]">
        <xsl:apply-templates select="document(@sc:refUri)"/>
    </xsl:template>
    
    <!-- Gestion information -->
    <xsl:template match="sp:info">
        <div>
            <h6><xsl:value-of select="cp:block/cp:blockM/sp:title"/></h6>
            <!-- TODO gerer plusieurs types de paragraphe -->
            <xsl:apply-templates select="cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para"/>
        </div>
    </xsl:template>
    
    <!-- Gestion warning -->
    <xsl:template match="sp:warning">
        <div data-hdoc-type='warning'>
            <h6><xsl:value-of select="cp:block/cp:blockM/sp:title"/></h6>
            <!-- TODO gerer plusieurs types de paragraphe -->
            <xsl:apply-templates select="cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para"/>
        </div>
    </xsl:template>
    
    <!-- Gestion complement -->
    <xsl:template match="sp:extra">
        <div data-hdoc-type='complement'>
            <h6><xsl:value-of select="cp:block/cp:blockM/sp:title"/></h6>
            <!-- TODO gerer plusieurs types de paragraphe -->
            <xsl:apply-templates select="cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para"/>
        </div>
    </xsl:template>
    
    <!-- Gestion conseil -->
    <xsl:template match="sp:advice">
        <div data-hdoc-type='advice'>
            <h6><xsl:value-of select="cp:block/cp:blockM/sp:title"/></h6>
            <!-- TODO gerer plusieurs types de paragraphe -->
            <xsl:apply-templates select="cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para"/>
        </div>
    </xsl:template>
    
    <!-- Gestion definition -->
    <xsl:template match="sp:definition">
        <div data-hdoc-type='definition'>
            <h6><xsl:value-of select="cp:block/cp:blockM/sp:title"/></h6>
            <!-- TODO gerer plusieurs types de paragraphe -->
            <xsl:apply-templates select="cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para"/>
        </div>
    </xsl:template>
    
    <!-- Gestion exemple -->
    <xsl:template match="sp:example">
        <div data-hdoc-type='example'>
            <h6><xsl:value-of select="cp:block/cp:blockM/sp:title"/></h6>
            <!-- TODO gerer plusieurs types de paragraphe -->
            <xsl:apply-templates select="cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para"/>
        </div>
    </xsl:template>
    
    <!-- Gestion hypothese -->
    <xsl:template match="sp:hypothesis">
        <div data-hdoc-type='remark'>
            <h6><xsl:value-of select="cp:block/cp:blockM/sp:title"/></h6>
            <!-- TODO gerer plusieurs types de paragraphe -->
            <xsl:apply-templates select="cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para"/>
        </div>
    </xsl:template>

    <!-- Gestion methode -->
    <xsl:template match="sp:method">
        <div data-hdoc-type='method'>
            <h6><xsl:value-of select="cp:block/cp:blockM/sp:title"/></h6>
            <xsl:apply-templates select="cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para"/>
        </div>
    </xsl:template>
    
    <!-- Gestion rappel -->
    <xsl:template match="sp:reminder">
        <div data-hdoc-type='remind'>
            <h6><xsl:value-of select="cp:block/cp:blockM/sp:title"/></h6>
            <xsl:apply-templates select="cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para"/>
        </div>
    </xsl:template>
    
    <!-- ******** FIN ACTIVITE TETM ********** -->
    
    
    <!-- ******** EXERCICE AVEC ET SANS CORRIGE ******** -->
    
    <xsl:template match="sc:item/cp:match">
        <xsl:param name="withoutExplanation"/>
            <section data-hdoc-type='match'>
                <header>
                    <h1><xsl:value-of select='cp:questionM/sp:title'/></h1>
                </header>
                <div data-hdoc-type = 'question'>
                    <xsl:apply-templates select='sc:question/cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para'/>
                </div>
                <xsl:apply-templates select='//sc:group'/>
                
                <xsl:if test="$withoutExplanation = 'sp:question'">
                    <div data-hdoc-type='explanation'>
                        <xsl:apply-templates select="sc:globalExplanation/cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para"/>
                    </div>
                </xsl:if>
            </section>
    </xsl:template>
    
    <!-- Categorisation -->
    <xsl:template match='sp:question/cp:match | sp:exercice/cp:match'>
        <section data-hdoc-type='match'>
            <header>
                <h1><xsl:value-of select='cp:questionM/sp:title'/></h1>
            </header>
            <div data-hdoc-type = 'question'>
                <xsl:apply-templates select='sc:question/cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para'/>
            </div>
            <xsl:apply-templates select='//sc:group'/>
            
            <xsl:if test="../name() = 'sp:question'">
                <div data-hdoc-type='explanation'>
                    <xsl:apply-templates select="sc:globalExplanation/cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para"/>
                </div>
            </xsl:if>
        </section>
    </xsl:template>
    
    <xsl:template match='sc:group'>
        <xsl:if test="sc:target/cp:label/sp:txt/cp:txtLabel/sc:para">
            <section data-hdoc-type = 'group'>
                <header>
                    <h1/>
                </header>
                <div data-hdoc-type='target'><xsl:apply-templates select='sc:target/cp:label/sp:txt/cp:txtLabel/sc:para'/></div>
                <xsl:apply-templates select='sc:label'/>
            </section>            
        </xsl:if>
    </xsl:template>
    
    <!-- Ordonnancement -->
    
    <xsl:template match='sp:question/cp:order | sp:exercice/cp:order'>								
        <section data-hdoc-type='order'>
            <header>
                <h1><xsl:value-of select='cp:questionM/sp:title'/></h1>
            </header>
            <div data-hdoc-type='question'>
                <xsl:apply-templates select='sc:question/cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para'/>
            </div>
            <xsl:apply-templates select="//sc:label"/>
            <xsl:if test="../name() = 'sp:question'">
                <div data-hdoc-type='explanation'>
                    <xsl:apply-templates select="sc:globalExplanation/cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para"/>
                </div>
            </xsl:if>
            
        </section>
    </xsl:template>
    
    
    <xsl:template match='sc:item/cp:order'>	        
        <xsl:param name="withoutExplanation"/>
        <section data-hdoc-type='order'>
            <header>
                <h1><xsl:value-of select='cp:questionM/sp:title'/></h1>
            </header>
            <div data-hdoc-type='question'>
                <xsl:apply-templates select='sc:question/cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para'/>
            </div>
            <xsl:apply-templates select="//sc:label"/>
            <xsl:if test="$withoutExplanation = 'sp:question'">
                <div data-hdoc-type='explanation'>
                    <xsl:apply-templates select="sc:globalExplanation/cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para"/>
                </div>
            </xsl:if>
            
        </section>
    </xsl:template>
    
    <xsl:template match="sc:label">
        <div data-hdoc-type = 'label'>
            <xsl:apply-templates select='cp:label/sp:txt/cp:txtLabel/sc:para'/>
        </div>
    </xsl:template>
    
    <!-- Texte à trou -->
    <xsl:template match="sp:question/cp:cloze | sp:exercice/cp:cloze">
        <section data-hdoc-type='fill-in-the-blank'>
            <header>
                <h1><xsl:value-of select="cp:questionM/sp:title"/></h1>
            </header>
            <div data-hdoc-type='question'>
                <xsl:apply-templates select="sc:question/cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para"/>
            </div>
            <div data-hdoc-type='gapText'>
                <xsl:apply-templates select="sc:gapText/cp:txtCloze/sc:para"/>
            </div>
            <xsl:if test="../name() = 'sp:question'">
                <div data-hdoc-type='explanation'>
                    <xsl:apply-templates select="sc:globalExplanation/cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para"/>
                </div>
            </xsl:if>
        </section>
    </xsl:template>
    
    <xsl:template match="sc:item/cp:cloze">
        <xsl:param name="withoutExplanation"/>
        <section data-hdoc-type='fill-in-the-blank'>
            <header>
                <h1><xsl:value-of select="cp:questionM/sp:title"/></h1>
            </header>
            <div data-hdoc-type='question'>
                <xsl:apply-templates select="sc:question/cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para"/>
            </div>
            <div data-hdoc-type='gapText'>
                <xsl:apply-templates select="sc:gapText/cp:txtCloze/sc:para"/>
            </div>
            <xsl:if test="$withoutExplanation = 'sp:question'">
                <div data-hdoc-type='explanation'>
                    <xsl:apply-templates select="sc:globalExplanation/cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para"/>
                </div>
            </xsl:if>
        </section>
    </xsl:template>
    
    
    <!-- Questionnement -->
    
    <xsl:template match="sp:question/cp:openQuestion | sp:exercice/cp:openQuestion">
        <section data-hdoc-type='exercise'>
            <header>
                <h1><xsl:value-of select="cp:questionM/sp:title"/></h1>
            </header>
            <section>
                <header><h1/></header>
                <xsl:if test="sp:question/cp:flow/*[name() != 'sp:profInstruction']">
                    <div data-hdoc-type='openQuestion'>
                        <xsl:apply-templates select="sp:question/cp:flow/sp:txt/cp:txt//sc:para"/>
                    </div>
                    <div data-hdoc-type='hint'>
                        <xsl:apply-templates select="sp:question/cp:flow/sp:profInstruction/cp:txt//sc:para"/>
                    </div>
                </xsl:if>
                <xsl:if test="../name() = 'sp:question' and sp:solution">
                    <div data-hdoc-type='solution'>
                        <xsl:apply-templates select="sp:solution/cp:flow/sp:txt/cp:txt//sc:para"/>
                    </div>
                </xsl:if>
            </section>
        </section>
    </xsl:template>
    
    <xsl:template match="sc:item/cp:openQuestion">
        <xsl:param name="withoutExplanation"/>
        <section data-hdoc-type='exercise'>
            <header>
                <h1><xsl:value-of select="cp:questionM/sp:title"/></h1>
            </header>
            <section>
                <header><h1/></header>
                <xsl:if test="sp:question/cp:flow/*">
                    <div data-hdoc-type='openQuestion'>
                        <xsl:apply-templates select="sp:question/cp:flow/sp:txt/cp:txt//sc:para"/>
                    </div>
                    <div data-hdoc-type='hint'>
                        <xsl:apply-templates select="sp:question/cp:flow/sp:profInstruction/cp:txt//sc:para"/>
                    </div>
                </xsl:if>
                <xsl:if test="$withoutExplanation = 'sp:question' and sp:solution">
                    <div data-hdoc-type='solution'>
                        <xsl:apply-templates select="sp:solution/cp:flow/sp:txt/cp:txt//sc:para"/>
                    </div>
                </xsl:if>
            </section>
        </section>
    </xsl:template>
    
    <!-- QCM and QCU-->
    <!-- TODO Gérer les explications par choix -->
    <xsl:template match="sc:item/cp:qcm">
        <xsl:param name="withoutExplanation"/>
        <section>
            <header>
                <h1><xsl:value-of select="cp:questionM/sp:title"/></h1>
            </header>
            <div data-hdoc-type = 'question'>
                <xsl:apply-templates select='sc:question/cp:block/sp:body'/>
            </div>
            <xsl:apply-templates select="sc:choices/sc:choice"/>
            <xsl:if test="$withoutExplanation = 'sp:question' and sc:globalExplanation/cp:block/sp:body/cp:flow/sp:txt">
                <div data-hdoc-type='explanation'>
                    <xsl:apply-templates select="sc:globalExplanation/cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para"/>
                </div>            
            </xsl:if>
        </section>
    </xsl:template>
    
    <xsl:template match="sp:question/cp:qcm | sp:exercice/cp:qcm">
        <section>
            <header>
                <h1><xsl:value-of select="cp:questionM/sp:title"/></h1>
            </header>
            <div data-hdoc-type = 'question'>
                <xsl:apply-templates select='sc:question/cp:block/sp:body'/>
            </div>
            <xsl:apply-templates select="sc:choices/sc:choice"/>
            <xsl:if test="../name() = 'sp:question' and sc:globalExplanation/cp:block/sp:body/cp:flow/sp:txt">
                <div data-hdoc-type='explanation'>
                    <xsl:apply-templates select="sc:globalExplanation/cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para"/>
                </div>            
            </xsl:if>
        </section>
    </xsl:template>
    
    <xsl:template match="sc:item/cp:qcu">
        <xsl:param name="withoutExplanation"/>
        <section>
            <header>
                <h1><xsl:value-of select="cp:questionM/sp:title"/></h1>
            </header>
            <div data-hdoc-type='singlechoicequestion'>
                <xsl:apply-templates select="sc:question/cp:block/sp:body"/>
            </div>
            <xsl:apply-templates select="sc:choices//sc:choice"/>
            <div data-hdoc-type='solution'><p><xsl:value-of select="sc:solution/@choice"/></p></div>
            <xsl:if test="$withoutExplanation = 'sp:question' and sc:globalExplanation/cp:block/sp:body/cp:flow/sp:txt">
                <div data-hdoc-type='explanation'>
                    <xsl:apply-templates select="sc:globalExplanation/cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para"/>
                </div>            
            </xsl:if>
            
        </section>
    </xsl:template>
    
    <xsl:template match="sp:question/cp:qcu | sp:exercice/cp:qcu">
        <section>
            <header>
                <h1><xsl:value-of select="cp:questionM/sp:title"/></h1>
            </header>
            <div data-hdoc-type='singlechoicequestion'>
                <xsl:apply-templates select="sc:question/cp:block/sp:body"/>
            </div>
            <xsl:apply-templates select="sc:choices//sc:choice"/>
            <div data-hdoc-type='solution'><p><xsl:value-of select="sc:solution/@choice"/></p></div>
            <xsl:if test="../name() = 'sp:question' and sc:globalExplanation/cp:block/sp:body/cp:flow/sp:txt">
                <div data-hdoc-type='explanation'>
                    <xsl:apply-templates select="sc:globalExplanation/cp:block/sp:body/cp:flow/sp:txt/cp:txt//sc:para"/>
                </div>            
            </xsl:if>
    
        </section>
    </xsl:template>
    
    <xsl:template match="sc:choice">
        <div data-hdoc-type='choice'>
            <xsl:apply-templates select="sc:choiceLabel/cp:txt/sc:para"/>
            
            <xsl:if test="sc:choiceExplanation">
                <div data-hdoc-type='explanation-choice-correct'>
                    <xsl:apply-templates select="sc:choiceExplanation/cp:txt/sc:para"/>
                </div>    
            </xsl:if>
        </div>
        
    </xsl:template>
    
    <xsl:template match="sc:choice[@solution]">
        <xsl:choose>
            <xsl:when test="@solution = 'checked'">
                <div data-hdoc-type='choice-correct'>
                    <xsl:apply-templates select="sc:choiceLabel/cp:txt/sc:para"/>
                    <xsl:if test="sc:choiceExplanation">
                        <div data-hdoc-type='explanation-choice-correct'>
                            <xsl:apply-templates select="sc:choiceExplanation/cp:txt/sc:para"/>
                        </div>    
                    </xsl:if>
                </div>
            </xsl:when>
            <xsl:otherwise>
                <div data-hdoc-type='choice-incorrect'>
                    <xsl:apply-templates select="sc:choiceLabel/cp:txt/sc:para"/>
                    <xsl:if test="sc:choiceExplanation">
                        <div data-hdoc-type='explanation-choice-incorrect'>
                            <xsl:apply-templates select="sc:choiceExplanation/cp:txt/sc:para"/>
                        </div>    
                    </xsl:if>
                </div>
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:template>
    
    <xsl:template match="sp:question[@sc:refUri] | sp:exercice[@sc:refUri]">
       <xsl:apply-templates select="document(@sc:refUri)/sc:item/*">
           <xsl:with-param name="withoutExplanation" select="name()"/>
       </xsl:apply-templates>
    </xsl:template>
    
    
    <xsl:template match="sp:question | sp:exercice">
        <xsl:apply-templates select="./*"/>
    </xsl:template>
</xsl:stylesheet>