# Convertisseur Canoprof vers HDOC

##Credits

### Autumn 2016

* NEVEUX Ana�s

* BELLIER Pierre-Ulysse

## User story

Un utilisateur a cr�� un cours de math�matiques � l'aide de l'outil Canoprof. Il souhaite, pour une raison quelconque, convertir son projet (et donc ses activit�s et programmations) dans un autre format. Pour cela il utilise notre convertisseur qui permet pr�alablement une conversion vers un format interm�diare (hdoc). Il fait donc un clic droit sur l'�l�ment principal de son projet sous Canoprof et clique sur "exporter l'archive". Il laisse les options (Inclure le r�seau descendant complet des items s�lectionn�s, et Pr�server les espaces de l'atelier) par d�faut. Il d�place ensuite le fichier .scar obtenu dans le dossier input du convertisseur puis ex�cute le fichier run.bat (ou run.sh sous un syst�me UNIX). Une fois l'ex�cution termin�e, il r�cup�re son hdoc dans le dossier output.

## Getting started
- Put the Canoprof .scar in the input folder. The scar should be the only one in this folder. The .programme should be into the root of your .scar and should be the only .programme file.
- Use the run.bat file to launch conversion.
- You will find your converted .hdoc in the output folder.

## What the converter handle
- Sequence
- Session
- Activity created in the .programme, transclusion available.
- Information/Warning/Advice/ect...
- Method and Remind fully functional.
- Add QCU and QCM. Transclusion of them available too. Need to fix a schema validation error when putting a title to a question, without filling the question content.
- Add "Exercice redactionnel" and transclusion.
- Fill-in-the blank (and transclusion)
- Order exercise (and transclusion)
- Match exercise (and transclusion)

## Known bugs
- You can have an exercice with no question but a solution. We should add a if statement to handle this.
- Something create a "keywrds" node inside the opale output, that make the schema invalid. Editing the title inside myScenari and just pressing enter is sufficient to fix that. But it will be better to fix that inside one of the xsl files.

## TODO List
- Handle multiple .programme file
- Copy resources (images, ect...)
- Handle arrays
- Handle other type of paragraph (facing texts, ect...)
- Handle remark inside different blocks
- Handle metadata
- Handle other activity types
- Handle sequence recursivity
- Handle comments
- Handle "Question � r�ponse num�rique"
- Handle "Liste de questions"
- Handle .personne, .liste, ... files
- Handle "Contenu filtrable"
- Handle transclusion in sub-level. For the moment, only transclusion in the main .programme works.
- Handle "Texte � s�lection simple" and "Texte � s�lection multiple" exercices
- Check how sequence -> division conversion is done to fix weird conversion
