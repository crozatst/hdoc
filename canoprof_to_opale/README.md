﻿# Canoprof2Opale

##Credits

### Autumn 2016

* NEVEUX Anaïs

* BELLIER Pierre-Ulysse

## User story

Un utilisateur a créé un cours de mathématiques à l'aide de l'outil Canoprof. Il souhaite, pour une raison quelconque, convertir son projet (et donc ses activités et programmations) au format Opale. Pour cela il utilise notre convertisseur qui va agir en deux étapes : une conversion dans un format intermédiaire (le format hdoc), puis conversion de ce hdoc dans le format Opale. Une fois son cours créé, il effectue un clic-droit sur l'élément racine de son projet, et choisit "exporter l'archive". Il laisse les options (Inclure le réseau descendant complet des items sélectionnés, et Préserver les espaces de l'atelier) par défaut. Il déplace ensuite le fichier .scar obtenu dans le dossier input du convertisseur puis exécute le fichier run.bat (ou run.sh sous un système UNIX). Il récupère ensuite un nouveau fichier .scar dans le dossier output, qu'il pourra importer dans Scenari.

## Getting started
- Put the Canoprof .scar in the input folder. The scar must be the only one in this folder. The .programme file must be into the root of your .scar, and must be the only .programme.
You can find several examples in the sample folder.
- Check if there's no .scar file in the input folder of canoprof_to_hdoc folder. If files exist, please remove them. Otherwise, the converter can take one of the .scar in this folder instead of the one you want to convert.
- Use the run.bat file to launch conversion.
- You will find your converted .scar in the output folder. Two .scar will be available, choose one of them.

## What the converter handle
- Sequence
- Session
- Activity created in the .programme, transclusion available.
- Information/Warning/Advice/ect...
- Method and Remind fully functional
- QCM and QCU working, even with transclusion. 
- Add "Exercice redactionnel" and transclusion.
- Fill-in-the-blank with transclusion.
- Order exercise (and transclusion)
- Match exercise (and transclusion)

## Known bugs
- You can have an exercice with no question but a solution. We should add a if statement to handle this.
- Something create a "keywrds" node inside the opale output, that make the schema invalid. Editing the title inside myScenari and just pressing enter is sufficient to fix that. But it will be better to fix that inside one of the xsl files.

## TODO List
- Handle multiple .programme file
- Copy resources (images, ect...)
- Handle arrays
- Handle other type of paragraph (facing texts, ect...)
- Handle remark inside different blocks
- Handle metadata
- Handle other activity types
- Handle sequence recursivity
- Handle comments
- Handle "Question à réponse numérique"
- Handle "Liste de questions"
- Handle .personne, .liste, ... files
- Handle "Contenu filtrable"
- Handle transclusion in sub-level. For the moment, only transclusion in the main .programme works.
- Handle "Texte à sélection simple" and "Texte à sélection multiple" exercices
- Check how sequence -> division conversion is done to fix weird conversion

