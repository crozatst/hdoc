<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    xpath-default-namespace="http://www.utc.fr/ics/hdoc/xhtml"
    xmlns="http://www.w3.org/1999/xhtml"
    version="2.0">

    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

    <xsl:template match="/html" priority="10">
        <xsl:result-document method="xml" href="../../../retour/oebps/chapitres/titlepage.xhtml">
            <html xml:lang="fr" lang="fr" xmlns:epub="http://www.idpf.org/2007/ops">
                <head>
                    <title><xsl:value-of select="head/title"/></title>
                    <meta content="http://www.w3.org/1999/xhtml; charset=utf-8" http-equiv="Content-Type"/>
                    <link href="../styles/stylesheet.css" type="text/css" rel="stylesheet"/>
                </head>
                <body>
                    <div class="titlePage"><xsl:value-of select="head/title"/></div>
                    <div class="subTitlePage"><xsl:value-of select="head/meta[@name='author']/@content"/></div>
                </body>
            </html>
        </xsl:result-document>
        <xsl:for-each select="/html/body/section">
            <xsl:variable name="path">../../../retour/oebps/chapitres/chapitre<xsl:number level="multiple" count="section" format="1"/>.xhtml</xsl:variable>
            <xsl:result-document method="xml" href="{$path}">
                <html xml:lang="fr" lang="fr" xmlns:epub="http://www.idpf.org/2007/ops">
                    <head>
                        <title><xsl:value-of select="header/h1"/></title>
                        <meta content="http://www.w3.org/1999/xhtml; charset=utf-8" http-equiv="Content-Type"/>
                        <link href="../styles/stylesheet.css" type="text/css" rel="stylesheet"/>
                    </head>
                    <body>
                        <h1><xsl:value-of select="header/h1"/></h1>
                        <xsl:apply-templates select="div|section"/>
                    </body>
                </html>
            </xsl:result-document>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="//section[count(ancestor::section) = 1 and position() &gt; 1]" priority="9">
        <xsl:variable name="path">../../../retour/oebps/chapitres/chapitre<xsl:number level="multiple" count="section" format="1_1"/>.xhtml</xsl:variable>
        <xsl:result-document method="xml" href="{$path}">
            <html xml:lang="fr" lang="fr" xmlns:epub="http://www.idpf.org/2007/ops">
                <head>
                    <title><xsl:value-of select="header/h1"/></title>
                    <meta content="http://www.w3.org/1999/xhtml; charset=utf-8" http-equiv="Content-Type"/>
                    <link href="../styles/stylesheet.css" type="text/css" rel="stylesheet"/>
                </head>
                <body>
                    <h2><xsl:value-of select="header/h1"/></h2>
                    <xsl:apply-templates select="div|section"/>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="section" priority="3">
        <xsl:element name="h{count(ancestor::section)+1}">
            <xsl:if test="section[@data-hdoc-type='exercise']">
                Exercice : 
            </xsl:if>
            <xsl:value-of select="header/h1"/>
        </xsl:element>
        <xsl:apply-templates select="div|section"/>
    </xsl:template>

    <xsl:template match="h6" priority="3"/>
    
    <xsl:template match="//div[@data-hdoc-type='emphasis']" priority="4">
        <h3>
            Fondamental : 
            <xsl:if test="h6">
                 <xsl:value-of select="h6"/>
            </xsl:if>
        </h3>
        <div class="border-top-bot">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="//div[@data-hdoc-type='definition']" priority="4">
        <h3>
            Définition : 
            <xsl:if test="h6">
                <xsl:value-of select="h6"/>
            </xsl:if>
        </h3>
        <div class="border-top-bot">
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="//div[@data-hdoc-type='remark']" priority="4">
        <h3>
            Remarque : 
            <xsl:if test="h6">
                <xsl:value-of select="h6"/>
            </xsl:if>
        </h3>
        <div>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="//div[@data-hdoc-type='complement']" priority="4">
        <h3>
            Complément : 
            <xsl:if test="h6">
                <xsl:value-of select="h6"/>
            </xsl:if>
        </h3>
        <div>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="//div[@data-hdoc-type='example']" priority="4">
        <h3>
            Exemple : 
            <xsl:if test="h6">
                <xsl:value-of select="h6"/>
            </xsl:if>
        </h3>
        <div>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="//div[@data-hdoc-type='syntax']" priority="4">
        <h3>
            Syntaxe : 
            <xsl:if test="h6">
                <xsl:value-of select="h6"/>
            </xsl:if>
        </h3>
        <div>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="//div[@data-hdoc-type='warning']" priority="4">
        <h3>
            Attention : 
            <xsl:if test="h6">
                <xsl:value-of select="h6"/>
            </xsl:if>
        </h3>
        <div>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="//div[@data-hdoc-type='advice']" priority="4">
        <h3>
            Conseil : 
            <xsl:if test="h6">
                <xsl:value-of select="h6"/>
            </xsl:if>
        </h3>
        <div>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="//div[@data-hdoc-type='description']" priority="4">
        <h4>
            Description :
        </h4>
        <div>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="//div[@data-hdoc-type='question']" priority="4">
        <h4>
            Question :
        </h4>
        <div>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="//div[@data-hdoc-type='solution']" priority="4">
        <h4>
            Solution :
        </h4>
        <div>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="//span[@data-hdoc-type='syntax']" priority="4">
        <span class="bold"><xsl:value-of select="."/></span>
    </xsl:template>
    
    <xsl:template match="//img" priority="4"> 
        <img width="300px">
            <xsl:attribute name="src">../images/<xsl:value-of select="./@alt"/></xsl:attribute>
            <xsl:attribute name="alt"><xsl:value-of select="./@alt"/></xsl:attribute>
        </img>
    </xsl:template>
    
    <xsl:template match="//a[not(starts-with(@href,'http'))]" priority="4">
        <xsl:element name="a">
            <xsl:attribute name="href">http://<xsl:value-of select="@href"/></xsl:attribute>
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="//object[@type='application/vnd.oasis.opendocument.graphics']" priority="4">
        Élément non supporté en EPUB : <xsl:value-of select="@data"/>
    </xsl:template>

    <!-- Identity transformation -->
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>

    <!-- Namespace substitution for hdoc elements -->
    <xsl:template match="*" priority="1">
        <xsl:element name="{local-name()}">
            <xsl:apply-templates select="node()|@*"/>
        </xsl:element>
    </xsl:template>


</xsl:stylesheet>
