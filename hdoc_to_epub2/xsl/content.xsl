<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xpath-default-namespace="http://www.utc.fr/ics/hdoc/xhtml"
    exclude-result-prefixes="xs"
    xmlns="http://www.idpf.org/2007/opf"
    version="2.0">
    
    <xsl:import href="content/metadata.xsl"/>
    <xsl:import href="content/manifest.xsl"/>
    <xsl:import href="content/spine.xsl"/>
    
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

    <xsl:template match="html">
      <package  unique-identifier="BookID" version="2.0">
          <xsl:call-template name="metadata"/>
          <xsl:call-template name="manifest"/>
          <xsl:call-template name="spine"/>
      </package>
    </xsl:template>

    <xsl:template match="text()"/>

</xsl:stylesheet>
