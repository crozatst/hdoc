<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xpath-default-namespace="http://www.utc.fr/ics/hdoc/xhtml"
    exclude-result-prefixes="xs"
    xmlns="http://www.idpf.org/2007/opf"
    version="2.0">
    <xsl:template name="metadata">
        <metadata xmlns:dc="http://purl.org/dc/elements/1.1/">
            <dc:title><xsl:value-of select="head/title"/></dc:title>
            <xsl:for-each select="head/meta[@name='keywords']">
                <dc:subject><xsl:value-of select="."/></dc:subject>
            </xsl:for-each>
            <dc:creator><xsl:value-of select="head/meta[@name='author']/@content"/></dc:creator>
            <dc:language>fr</dc:language>
            <dc:identifier id="BookID">V2.0</dc:identifier>
        </metadata>
    </xsl:template>

</xsl:stylesheet>
