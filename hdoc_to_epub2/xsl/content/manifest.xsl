<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xpath-default-namespace="http://www.utc.fr/ics/hdoc/xhtml"
    exclude-result-prefixes="xs"
    xmlns="http://www.idpf.org/2007/opf"
    version="2.0">
    <xsl:param name="images"/>
    <xsl:template name="manifest">
        <manifest>
            <!-- Table des matières -->
            <item id="tDMncx" href="tableDesMatieres.ncx" media-type="application/x-dtbncx+xml"/>
            <!-- Chapitre -->
            <xsl:for-each select="//section[(count(ancestor::section) = 1 and position() &gt; 1) or (count(ancestor::section) = 0)]">
                <item>
                    <xsl:attribute name="href">chapitres/chapitre<xsl:number level="multiple" count="section" format="1_1"/>.xhtml</xsl:attribute>
                    <xsl:attribute name="id">ch_<xsl:number level="multiple" count="section" format="1_1"/></xsl:attribute>
                    <xsl:attribute name="media-type">application/xhtml+xml</xsl:attribute>
                </item>
            </xsl:for-each>
            <!-- CSS -->
            <item id="CSS" href="styles/stylesheet.css" media-type="text/css"/>  
            <!-- Première page -->
            <item href="chapitres/titlepage.xhtml" id="titlepage" media-type="application/xhtml+xml"/>
            <!-- Images -->
            <xsl:call-template name="parse-comma-separated">
                <xsl:with-param name="text" select="$images"/>
                <xsl:with-param name="i" select="1"/>
            </xsl:call-template>
            
        </manifest>
    </xsl:template>
    
    <xsl:template name="parse-comma-separated">
        <xsl:param name="text"/>
        <xsl:param name="i"/>
        
        <xsl:choose>
            <xsl:when test="contains($text, ',')">
                <!-- Récupération du nom du fichier et de son extension -->
                <xsl:variable name="filename"><xsl:value-of select="normalize-space(substring-before($text, ','))"/></xsl:variable>
                <xsl:variable name="extension">
                    <xsl:call-template name="get-file-extension">
                        <xsl:with-param name="path" select="$filename" />
                    </xsl:call-template>
                </xsl:variable>
                <!-- Création de l'item -->
                <item>
                    <!-- Test sur son extension -->
                    <xsl:choose>
                        <xsl:when test="$extension = 'jpg' or $extension = 'jpeg'">
                            <xsl:attribute name="media-type">image/jpeg</xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="media-type">image/<xsl:value-of select="$extension"/></xsl:attribute>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:attribute name="href">images/<xsl:value-of select="$filename"/></xsl:attribute>
                    <xsl:attribute name="id">img<xsl:value-of select="$i"/></xsl:attribute>
                </item>
                
                <!-- Boucle tant qu'il y a des ',' -->
                <xsl:call-template name="parse-comma-separated">
                    <xsl:with-param name="text" select="substring-after($text, ',')"/>
                    <xsl:with-param name="i" select="$i + 1"/>
                </xsl:call-template>
                
            </xsl:when>
            <xsl:otherwise>
                <!-- Récupération du nom du fichier et de son extension -->
                <xsl:variable name="filename"><xsl:value-of select="normalize-space($text)"/></xsl:variable>
                <xsl:variable name="extension">
                    <xsl:call-template name="get-file-extension">
                        <xsl:with-param name="path" select="$filename" />
                    </xsl:call-template>
                </xsl:variable>
                <!-- Création de l'item -->
                <item>
                    <!-- Test sur son extension -->
                    <xsl:choose>
                        <xsl:when test="$extension = 'jpg' or $extension = 'jpeg'">
                            <xsl:attribute name="media-type">image/jpeg</xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="media-type">image/<xsl:value-of select="$extension"/></xsl:attribute>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:attribute name="href">images/<xsl:value-of select="$filename"/></xsl:attribute>
                    <xsl:attribute name="id">img<xsl:value-of select="$i"/></xsl:attribute>
                </item>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- Récupère l'extension du fichier -->
    <xsl:template name="get-file-extension">
        <xsl:param name="path"/>
        <xsl:choose>
            <xsl:when test="contains($path, '/')">
                <xsl:call-template name="get-file-extension">
                    <xsl:with-param name="path" select="substring-after($path, '/')"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="contains($path, '.')">
                <xsl:call-template name="get-file-extension">
                    <xsl:with-param name="path" select="substring-after($path, '.')"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$path"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
