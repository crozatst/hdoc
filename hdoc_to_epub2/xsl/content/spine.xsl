<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xpath-default-namespace="http://www.utc.fr/ics/hdoc/xhtml"
    exclude-result-prefixes="xs"
    xmlns="http://www.idpf.org/2007/opf"
    version="2.0">
    <xsl:template name="spine">
        <spine toc="tDMncx">
            <itemref idref="titlepage"/>
            <xsl:for-each select="//section[(count(ancestor::section) = 1 and position() &gt; 1) or (count(ancestor::section) = 0)]">
                <itemref>
                    <xsl:attribute name="idref">ch_<xsl:number level="multiple" count="section" format="1_1"/></xsl:attribute>
                </itemref>
            </xsl:for-each>
        </spine>
    </xsl:template>
</xsl:stylesheet>
