<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    xpath-default-namespace="http://www.utc.fr/ics/hdoc/xhtml"
    xmlns="http://www.daisy.org/z3986/2005/ncx/"
    version="2.0">
    
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    
    <xsl:template match="html">
        <ncx version="2005-1" xml:lang="fra">
            <head>
                <meta content="org-example-5059463624137734586" name="dtb:uid"/>
            </head>
            <docTitle>
                <text><xsl:value-of select="head/title"/></text>
            </docTitle>
            <navMap>
                <xsl:apply-templates/>
            </navMap>
        </ncx>
    </xsl:template>
    
    <xsl:template match="html/body/section">
        <navPoint>
            <xsl:attribute name="class">h<xsl:value-of select="count(ancestor::section)+1"/></xsl:attribute>
            <xsl:attribute name="id">ch_<xsl:number level="multiple" count="section" format="1_1"/></xsl:attribute>
            <navLabel>
                <text>
                    <xsl:value-of select="header/h1"/>
                </text>
            </navLabel>
            <content>
                <xsl:attribute name="src">chapitres/chapitre<xsl:number level="multiple" count="section" format="1_1"/>.xhtml</xsl:attribute>
            </content>
            <xsl:apply-templates/>
        </navPoint>
    </xsl:template>
    
    <xsl:template match="//section[count(ancestor::section) = 1 and position() &gt; 1]">
        <navPoint>
            <xsl:attribute name="class">h<xsl:value-of select="count(ancestor::section)+1"/></xsl:attribute>
            <xsl:attribute name="id">ch_<xsl:number level="multiple" count="section" format="1_1"/></xsl:attribute>
            <navLabel>
                <text>
                    <xsl:value-of select="header/h1"/>
                </text>
            </navLabel>
            <content>
                <xsl:attribute name="src">chapitres/chapitre<xsl:number level="multiple" count="section" format="1_1"/>.xhtml</xsl:attribute>
            </content>
            <xsl:apply-templates/>
        </navPoint>
    </xsl:template>
    
    <xsl:template match="text()"/>
    
</xsl:stylesheet>