﻿Converter hdoc_to_neo4j
-----------------------

The purpose of this converter is to obtain a file of Neo4j requests from .scar format. 


License GPL3.0
--------------

http://www.gnu.org/licenses/gpl-3.0.txt


Credits
-------

* Alié Félix

* Routier Clément


Dependance
----------


This project can be used alone if you only want to convert a .scar into a Neo4j requests file or directly insert data in neo4j local database.

WARNINGS: 

1) If you want to insert data automatically in a database, you need to have nodeJS installed! 
	Windows: https://nodejs.org/en/ and download + install the last stable version
	Linux: install with your distribution's package manager

2) You have to download neo4j, according to your OS, and have it running during the execution of the script.
	Windows: https://neo4j.com/download/community-edition
	Linux: https://neo4j.com/docs/operations-manual/current/installation/linux

3) You need to configure the ../hdoc_to_neo4j/node/config.json file properly to fit with the parameters of the dabase you want to insert in. (host, db and password).



User documentation
------------------

You have to respect the following steps :

1 - Place one or several archives (.scar) in the "input" folder

2 - Run the "runWithNode" file (choose the extension according to your OS)

3 - Admire the graph created with neo4j in your browser (on localhost:7474)


Unsupported 
-----------


Known bugs
----------


Todo
---- 
Create a new type of link for references (a first version of the "voir aussi" section is done in opale_to_hdoc)
Create and import a .grass file for nodes and links style
Use metadata (as a new attribute to the node) to search all nodes related to a topic
Create a public database



Technical notes
---------------

Here are some requests which could be useful :

==> Display all data
MATCH (n) return n

==> Delete all data
MATCH (n) detach delete n

==> Display all exercises
MATCH (n:exercise) return n

==> Display all nodes linked with the searched one
MATCH (n) WHERE n.title =~ '(?i).*Algèbre.*' MATCH(n)-[:LIEN*1..10]->(d) RETURN distinct d;

==> Delete a single node
MATCH (n) WHERE n.title =~ '(?i).*Gestion du personnel.*' detach n delete n 



Capitalisation
--------------