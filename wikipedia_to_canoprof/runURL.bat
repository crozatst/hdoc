@echo off
set lib=lib
set ant=ant/wiki_to_canoprof_fetcher.ant
set url=%1
set filename=%2

set scJarList=%lib%\*

java.exe -classpath "%scJarList%" -Xmx150m org.apache.tools.ant.Main -buildfile %ant% -DwikipediaUrl %url% -Dfilename %filename%
pause

REM start /MIN java.exe -classpath "%scJarList%" -Xmx150m org.apache.tools.ant.Main -buildfile %ant% %antparam%
