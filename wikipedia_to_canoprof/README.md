---------------------------------
Converter : Wikipedia to Canoprof
---------------------------------


##### Credits #####

LAINE Anna
LAMROUS Aghiles



##### Presentation #####

Wikipedia to Canoprof is a converter that transforms a Wikipedia page into a Canoprof programme.



##### Dependencies #####

- Wikipedia to Hdoc converter
- Hdoc to Canoprof converter



##### User documentation #####

Generating a Canoprof archive from the URL of the Wikipedia page:
-----------------------------------------------------------------
	1. Run the command corresponding to your OS:
		
		On Windows:
			runURL yourWikipediaUrl yourFileName
				yourWikipediaUrl is the URL of the Wikipedia page you would like to convert
				yourFileName is the name of the file which will be created
				
			For instance: runURL https://fr.wikipedia.org/wiki/Jean_Jaur%C3%A8s jean_jaures
			
		On Linux:
			sh runURL.sh yourWikipediaUrl yourFileName
				yourWikipediaUrl is the URL of the Wikipedia page you would like to convert
				yourFileName is the name of the file which will be created
				
			For instance: sh runURL.sh https://fr.wikipedia.org/wiki/Jean_Jaur%C3%A8s jean_jaures
			
	2. Get the .scar in the output folder.



Generating a Canoprof archive from a local file containing the Wikipedia page code
----------------------------------------------------------------------------------
	1. Copy the source code of the Wikipedia page you would like to convert in a file called "source.xml" and put this file in the "input" directory.
	
	2. Run the command corresponding to your OS:
	
		On Windows:
			runFile
			
		On Linux:
			sh runFile.sh



##### Supported functionalities #####

This converter can get from a Wikipedia page images, simple tables, links. It will get the important sections, excluding the "See also", "References", "Further reading", "External links", etc...



##### Known bugs #####

- The display of the complex tables : actually we get an external object that we have to download, which is not that practical



##### To do #####

- Add the table on the right of each Wikipedia page
