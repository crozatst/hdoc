<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes"/>
    <xsl:key name="bits" match="p/node()[not(self::br)]" use="generate-id((..|preceding-sibling::br[1])[last()])"/>
    
    <xsl:template match="p">
        <p>
            <xsl:apply-templates select="key('bits', generate-id())"/>
        </p>
        <xsl:apply-templates select="br"/>
    </xsl:template>
    
    <xsl:template match="p/br">
        <p>
            <xsl:apply-templates select="key('bits', generate-id())"/>
        </p>
    </xsl:template>
    <xsl:template match="li/br">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="strong">
        <em>
            <xsl:apply-templates select="@*|node()"/>
        </em>
    </xsl:template>
    <xsl:template match="s">
        <em>
            <xsl:apply-templates select="@*|node()"/>
        </em>
    </xsl:template>
    <xsl:template match="u">
        <em>
            <xsl:apply-templates select="@*|node()"/>
        </em>
    </xsl:template>
    <xsl:template match="ul">
        <ul>
            <xsl:apply-templates select="*"/>
        </ul>
    </xsl:template>
    <xsl:template match="ol">
        <ol>
            <xsl:apply-templates select="*"/>
        </ol>
    </xsl:template>
    
    
    <xsl:template match="page">
        <xsl:apply-templates select="@*|node()"/>
    </xsl:template>
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>